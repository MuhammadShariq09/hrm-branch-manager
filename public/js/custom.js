$( ".confirm-icon" ).click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $(".confirm-input");
  if (input.attr("type") === "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

// show password on eye icon click
$( ".enter-icon" ).click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $(".enter-input");
  if (input.attr("type") === "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

// show password on eye icon click
$( ".current-icon" ).click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $(".current-input");
  if (input.attr("type") === "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

var sidebarToggle = document.getElementsByClassName("sidebar-toggle");
var sidebar = document.getElementsByClassName("sidebar")[0];

sidebarToggle[0].addEventListener("click", sidebarCollapsed);

function sidebarCollapsed() {
  sidebar.classList.toggle("collapsed");
}


$(document).ready(function () {

    setTimeout(function () {
        $('.preloader').fadeOut(100);
    }, 1000);

});

$('.banner-slider').owlCarousel({
  loop:true,
  margin:10,
  nav:false,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
})

var $frame = $('#centered');
var $wrap  = $frame.parent();

// Call Sly on frame
$frame.sly({
  horizontal: 1,
  itemNav: 'centered',
  smart: 1,
  activateOn: 'click',
  mouseDragging: 1,
  touchDragging: 1,
  releaseSwing: 1,
  startAt: 4,
  scrollBar: $wrap.find('.scrollbar'),
  scrollBy: 1,
  speed: 300,
  elasticBounds: 1,
  easing: 'easeOutExpo',
  dragHandle: 1,
  dynamicHandle: 1,
  clickBar: 1,

  // Buttons
  prev: $wrap.find('.prev'),
  next: $wrap.find('.next')
});



//-------

// init Isotope
var $grid = $('.grid').isotope({
  itemSelector: '.element-item',
  layoutMode: 'fitRows',
  percentPosition: true,
  masonry: {
    columnWidth: '.element-item'
  }
});

function onAnimationFinished(){
  $frame.sly('reload');
}


$('.filters-button-group').on( 'click', 'a', function() {
  var filterValue = $( this ).attr('data-filter');
  $grid.isotope({ filter: filterValue }, onAnimationFinished);
});


$grid.on( 'layoutComplete', onAnimationFinished );

$('.button-group').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'a', function() {
    $buttonGroup.find('.active').removeClass('active');
    $( this ).closest('li').addClass('active');
    
  });
});

var $frame2 = $('#centered2');
var $wrap2  = $frame2.parent();

// Call Sly on frame
$frame2.sly({
  horizontal: 1,
  itemNav: 'centered',
  smart: 1,
  activateOn: 'click',
  mouseDragging: 1,
  touchDragging: 1,
  releaseSwing: 1,
  startAt: 4,
  scrollBar: $wrap2.find('.scrollbar'),
  scrollBy: 1,
  speed: 300,
  elasticBounds: 1,
  easing: 'easeOutExpo',
  dragHandle: 1,
  dynamicHandle: 1,
  clickBar: 1,

  // Buttons
  prev: $wrap2.find('.prev'),
  next: $wrap2.find('.next')
});

//-------

// init Isotope
var $grid = $('.grid').isotope({
  itemSelector: '.element-item',
  layoutMode: 'fitRows',
  percentPosition: true,
  masonry: {
    columnWidth: '.element-item'
  }
});

function onAnimationFinished(){
  $frame2.sly('reload');
}


$('.filters-button-group').on( 'click', 'a', function() {
  var filterValue = $( this ).attr('data-filter');
  $grid.isotope({ filter: filterValue }, onAnimationFinished);
});


$grid.on( 'layoutComplete', onAnimationFinished );

$('.button-group').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'a', function() {
    $buttonGroup.find('.active').removeClass('active');
    $( this ).closest('li').addClass('active');
  });
});
//  $(window).resize(function(e) {
//         $frame.sly('reload');
//   });

// const SimpleScrollbar = require('simple-scrollbar');
// require('simple-scrollbar/simple-scrollbar.css');



//   $breadcrumb-divider: quote(">");
