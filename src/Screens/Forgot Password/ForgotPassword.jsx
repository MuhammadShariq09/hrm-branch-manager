import React, { useState } from "react";
import { useMutation } from "react-query";
import { Link } from "react-router-dom";
import { recoverPassword, resetPassword, verifyCode } from "../../Apis";
import Button from "../../Components/Elements/Form/Button";
import Input from "../../Components/Elements/Form/Input";
import InputNumber from "../../Components/Elements/Form/InputNumber";
import InputPassword from "../../Components/Elements/Form/InputPassword";
import Error from "../../Components/Elements/Modals/Modal.Error";
import Success from "../../Components/Elements/Modals/Modal.Success";

export default function ForgotPassword({ history }) {
  const [email, setEmail] = useState();
  const [code, setCode] = useState("");
  const [password, setPassword] = useState("");
  const [confirm_password, setConfirmPassword] = useState("");
  const [step, setStep] = useState(1);

  // FORGOT PASSWORD JOURNEY APIs
  const { mutate: recoverPasswordMuate, isLoading: recoverLoading } =
    useMutation((data) => recoverPassword(data), {
      retry: false,
      onSuccess: (res) => {
        Success(res?.data?.message);
        setStep(2);
      },
      onError: (err) => Error(err?.response?.data?.message),
    });
  const { mutate: verifyCodeMutate, isLoading: codeLoading } = useMutation(
    (data) => verifyCode(data),
    {
      retry: false,
      onSuccess: (res) => {
        Success(res?.data?.message);
        setStep(3);
      },
      onError: (err) => Error(err?.response?.data?.message),
    }
  );
  const { mutate: resetPasswordMuate, isLoading: resetLoading } = useMutation(
    (data) => resetPassword(data),
    {
      retry: false,
      onSuccess: (res) => {
        Success(res?.data?.message);
        setStep(1);
        setCode("");
        setPassword("");
        setConfirmPassword("");
        history.replace("/");
      },
      onError: (err) => Error(err?.response?.data?.message),
    }
  );

  return (
    <div className="main">
      <section className="login">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-6 login-left px-md-0">
              <img src="images/logo-bg.png" alt="" className="img-fluid" />
              <p className="mt-3">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum.{" "}
              </p>
              <a href="m-index.php" className="d-blue-text ubuntu">
                Back to website
              </a>
            </div>
            <div className="col-md-6 login-right px-md-0">
              <h1>Password Recovery</h1>
              <form action="a-forget-password-2.php" className="w-100">
                {step === 1 && (
                  <div className="form-group mt-3">
                    <Input
                      placeholder="Email Address"
                      value={email}
                      onChange={(email) => setEmail(email)}
                      className="site-input login-input px-3 py-2 w-100"
                    />
                  </div>
                )}
                {step === 2 && (
                  <div className="form-group mt-3">
                    <InputNumber
                      max={4}
                      value={code}
                      className="form-control"
                      onChange={(email) => setCode(email)}
                      placeholder="Enter Code Sent To Your Email"
                      className="site-input login-input px-3 py-2 w-100"
                    />
                  </div>
                )}
                {step === 3 && (
                  <>
                    <div className="form-group mt-3">
                      <InputPassword
                        placeholder="Enter New Password"
                        value={password}
                        onChange={(password) => setPassword(password)}
                        className="site-input login-input px-3 py-2 w-100"
                      />
                    </div>
                    <div className="form-group mt-3">
                      <InputPassword
                        placeholder="Enter Password Again"
                        value={confirm_password}
                        onChange={(password) => setConfirmPassword(password)}
                        className="site-input login-input px-3 py-2 w-100"
                      />
                    </div>
                  </>
                )}
                <div className="form-group text-lef mt-4">
                  <Button
                    loading={recoverLoading || codeLoading || resetLoading}
                    className="site-btn grey-btn py-2 w-100 mt-lg-5 mt-4"
                    onClick={() => {
                      if (step === 1) recoverPasswordMuate({ email });
                      if (step === 2) verifyCodeMutate({ code, email });
                      if (step === 3)
                        resetPasswordMuate({
                          password,
                          confirm_password,
                          code,
                          email,
                        });
                    }}
                  >
                    {step === 3 ? "CONITNUE" : "SEND"}
                  </Button>
                </div>
                <Link
                  to="/login"
                  className="site-btn d-block text-center yellow-border py-2 w-100 mt-3"
                >
                  Back To Login
                </Link>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
