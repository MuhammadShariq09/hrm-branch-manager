import React, { useState } from "react";
import { useQuery } from "react-query";
import { getDashboardData } from "../../Apis";
import Loading from "../../Components/Elements/Icons/Loading";
import Graph from "./Graph";

import EmployeeLogs from "../Employees/Components/EmployeeLogs";

export default function Dashboard({ history }) {
  const [year, setYear] = useState(new Date().getFullYear());

  const { data, isLoading } = useQuery(["dashboard", year], () =>
    getDashboardData(year)
  );

  return (
    <div className="main-body">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 px-0 px-lg-2">
            <div className="white-card mt-3 px-2 py-3 px-sm-3">
              <div className="d-md-flex align-items-center justify-content-between">
                <h5 className="mb-0">Dashboard</h5>
                <button
                  className="site-btn mt-md-0 mt-2 d-flex align-items-center px-3 py-2 grey-btn"
                  onClick={() => history.push("/messages")}
                >
                  <svg
                    className="mr-2"
                    xmlns="http://www.w3.org/2000/svg"
                    width="25.781"
                    height="24.492"
                    viewBox="0 0 25.781 24.492"
                  >
                    <path
                      id="chat"
                      d="M12.89,1.5C5.783,1.5,0,6.7,0,13.1a10.725,10.725,0,0,0,2.049,6.275c-.254,2.809-.935,4.894-1.923,5.882a.43.43,0,0,0,.3.733.4.4,0,0,0,.06,0,22.954,22.954,0,0,0,7.14-2.294A13.986,13.986,0,0,0,12.89,24.7c7.108,0,12.89-5.2,12.89-11.6S20,1.5,12.89,1.5ZM6.875,14.82A1.719,1.719,0,1,1,8.594,13.1,1.72,1.72,0,0,1,6.875,14.82Zm6.016,0A1.719,1.719,0,1,1,14.609,13.1,1.72,1.72,0,0,1,12.89,14.82Zm6.016,0A1.719,1.719,0,1,1,20.625,13.1,1.72,1.72,0,0,1,18.906,14.82Z"
                      transform="translate(0 -1.5)"
                      fill="#fff"
                    />
                  </svg>
                  Message
                </button>
              </div>
              <div className="row">
                <div className="col-xl-4 col-sm-6 mt-3">
                  <div className="shadow-card p-3 d-flex align-items-center justify-content-between">
                    <div>
                      <p className="p-xl mb-2 bold ubuntu">Total Employees</p>
                      <p className="p-xl ubuntu mb-0 orange-text">
                        {isLoading ? <Loading /> : data?.data?.total_employees}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-xl-4 col-sm-6 mt-3">
                  <div className="shadow-card p-3 d-flex align-items-center justify-content-between">
                    <div>
                      <p className="p-xl mb-2 bold ubuntu">
                        Total Leave Requests
                      </p>
                      <p className="p-xl ubuntu mb-0 orange-text">
                        {isLoading ? (
                          <Loading />
                        ) : (
                          data?.data?.total_leave_requests
                        )}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="d-sm-flex mt-3 align-items-center justify-content-between">
                <h5 className="mt-3 mb-0">Employee Analytics</h5>
                <select
                  className="site-input mt-3 w-150 px-3 py-2"
                  value={year}
                  onChange={(e) => setYear(e.target.value)}
                >
                  {[21, 22, 23, 24, 25, 26, 27, 28, 29, 30].map((year) => (
                    <option value={`20${year}`}>20{year}</option>
                  ))}
                </select>
              </div>
              {isLoading ? (
                <Loading />
              ) : (
                <Graph graph_data={data?.data?.employee_analytics} />
              )}
              <div className="mt-5" />
              <EmployeeLogs />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
