import React, { useState, useEffect } from "react";
import { useQuery, useMutation } from "react-query";
import { useRecoilValue } from "recoil";
import Pagination from "../../Components/Elements/Table/Pagination";
import { queryClient } from "../..";
import { userState } from "../../Recoil";
import { getNotifications, postNotificationRead } from "../../Apis";
import LoadingView from "../../Components/LoadingView";
import moment from "moment";

export default function Notifications() {
  const user = useRecoilValue(userState);
  const [page, setPage] = useState(1);

  const { isFetching, isLoading, data } = useQuery(
    ["notifications", page],
    () => getNotifications(page, user?.branch),
    { keepPreviousData: true }
  );

  // Prefetch the next page!
  useEffect(() => {
    if (data?.data?.notifications?.hasNextPage) {
      queryClient.prefetchQuery(["notifications", page + 1], () =>
        getNotifications(page, user?.branch)
      );
    }
  }, [data, page, queryClient]);

  const { mutate, isLoading: postNotificationReadLoading } = useMutation(
    (id, branchId) => postNotificationRead(id, branchId),
    {
      onSuccess: (res) => {},
      onError: (err) => Error(err?.response?.data?.message),
    }
  );

  if (isLoading || isFetching) return <LoadingView />;

  return (
    <div className="main">
      <div className="main-body">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 px-0 px-lg-2">
              <div className="white-card mt-3 px-2 py-3 px-sm-3">
                <h4 className="my-0">Notifications</h4>
                <div className="orange-bg p-3">
                  {data?.data?.notifications?.docs?.map((el, i) => (
                    <div key={i} className={i === 0 ? "media" : "media mt-4"}>
                      <i
                        className={`fas fa-bell p-lg mt-1 ${
                          Boolean(el?.read) ? "d-blue-text" : "orange-text"
                        }`}
                      />
                      <div
                        className="media-body ml-2"
                        onClick={
                          !postNotificationReadLoading && !Boolean(el?.read)
                            ? () => mutate(el?.id, user?.branch)
                            : null
                        }
                      >
                        <p className="mb-0">{el?.message}</p>
                        <p
                          className={`p-sm ${
                            Boolean(el?.read) ? "d-grey-text" : "orange-text"
                          } mb-0`}
                        >
                          {moment(el?.createdAt).format(
                            "DD/MM/YYYY hh:mm:ss A"
                          )}
                        </p>
                      </div>
                    </div>
                  ))}
                </div>
                <div className="justify-content-between">
                  <p className="p-sm mt-3 mb-0">
                    Showing 1 to 20 of 54 entries
                  </p>
                  <Pagination totalPages={3} setPage={setPage} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
