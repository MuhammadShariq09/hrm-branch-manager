import React, { useState, useEffect, useRef } from "react";
import { useQuery } from "react-query";
import { useRecoilValue } from "recoil";
// import InfiniteScroll from "react-infinite-scroller";
import useInfiniteScroll from "react-infinite-scroll-hook";

import { queryClient } from "../..";
import { getMessages } from "../../Apis";
import LoadingView from "../../Components/LoadingView";
import { userState } from "../../Recoil";
import { image_url } from "../../Utils/connection_strings";
import { format_date_time } from "../../Utils/helpers";
import Loading from "../../Components/Elements/Icons/Loading";

export default function ViewAdminMessages() {
  const [page, setPage] = useState(1);
  const [perPage] = useState(5);
  const [messages, setMessages] = useState([]);
  const [total, setTotal] = useState(0);
  const [pagination_info, setPaginationInfo] = useState(0);
  const [loading, setLoading] = useState(true);
  const user = useRecoilValue(userState);

  const scrollableRootRef = useRef(null);
  const lastScrollDistanceToBottomRef = useRef(null);

  const [infiniteRef, { rootRef }] = useInfiniteScroll({
    loading,
    hasNextPage: pagination_info?.hasNextPage,
    onLoadMore: () => {
      setPage(page + 1);
    },
    rootMargin: "0px 0px 400px 0px",
  });

  useEffect(() => {
    if (user?.message_room_id) handleGetMessages(page);
  }, [page, user?.message_room_id]);

  useEffect(() => {
    if (total) setLoading(false);
  }, [total]);

  useEffect(() => {
    const scrollableRoot = scrollableRootRef.current;
    const lastScrollDistanceToBottom = lastScrollDistanceToBottomRef.current ?? 0;
    if (scrollableRoot) {
      scrollableRoot.scrollTop = scrollableRoot.scrollHeight - lastScrollDistanceToBottom;
    }
  }, [messages, rootRef]);

  const rootRefSetter = React.useCallback(
    (node) => {
      rootRef(node);
      scrollableRootRef.current = node;
    },
    [rootRef]
  );

  const handleRootScroll = React.useCallback(() => {
    const rootNode = scrollableRootRef.current;
    if (rootNode) {
      const scrollDistanceToBottom = rootNode.scrollHeight - rootNode.scrollTop;
      lastScrollDistanceToBottomRef.current = scrollDistanceToBottom;
    }
  }, []);

  const handleGetMessages = async (page) => {
    try {
      setLoading(true);
      const { data } = await getMessages(page, perPage, user?.message_room_id);

      if (!total) {
        setTotal(data?.messages?.totalDocs);
      }
      // DONT PUT IN IF CONDITION
      setPaginationInfo(data?.messages);

      setMessages([...data?.messages?.docs, ...messages]);
      setLoading(false);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="main">
      <div className="main-body">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 px-0 px-lg-2">
              <div className="white-card mt-3 px-2 pb-5 py-3 px-sm-3">
                <h4>View Messages - Admin</h4>
                <div>
                  <div
                    style={{
                      height: 300,
                      overflow: "auto",
                    }}
                    ref={rootRefSetter}
                    onScroll={handleRootScroll}
                  >
                    {(loading || pagination_info?.hasNextPage) && (
                      <div ref={infiniteRef}>
                        <Loading />
                      </div>
                    )}
                    {messages.map((el, i) => (
                      <div key={i} className="media mt-3 d-sm-flex d-block">
                        <div className="message-img">
                          <img
                            src={
                              el?.branch_manager_from?.user_image
                                ? `${image_url}${el?.branch_manager_from?.user_image}`
                                : "images/message-img.png"
                            }
                            alt=""
                            className="img-fluid"
                          />
                          <span className="online" />
                        </div>
                        <div className="media-body ml-sm-3">
                          <h5>Admin</h5>
                          <div className="message-card p-lg-4 p-2">
                            <p className="mb-0">{el?.message}</p>
                          </div>
                          <p className="p-sm mb-0 mt-2 d-grey-text">{format_date_time(el?.createdAt)}</p>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
