import React, { useState, useEffect } from "react";
import debounce from "debounce-promise";
import { useMutation } from "react-query";
import AsyncSelect from "react-select/async";

import { getMinEmployees } from "../../Apis";
import { sendMessage, sendMessageToAll } from "../../Apis";
import Error from "../../Components/Elements/Modals/Modal.Error";
import Button from "../../Components/Elements/Form/Button";

export default function SendMessage() {
  const [messageTo, setMessageTo] = useState("all");
  const [employees, setEmployees] = useState([]);
  const [selectedEmployees, setSelectedEmployees] = useState([]);
  const [message, setMessage] = useState("");

  const { mutate: send, isLoading: sendSingleLoading } = useMutation((data) => sendMessage(data), {
    onSuccess: (res) => {
      setSelectedEmployees([]);
      setMessage("");
      window.$(".sendMessage").modal("hide");
      window.$(".messageSent").modal("show");
    },
    onError: (err) => {
      Error(err?.response?.data?.message);
    },
  });

  const { mutate: sendToAll, isLoading: sendToAllLoading } = useMutation((data) => sendMessageToAll(data), {
    onSuccess: (res) => {
      setSelectedEmployees([]);
      setMessage("");
      window.$(".sendMessage").modal("hide");
      window.$(".messageSent").modal("show");
    },
    onError: (err) => {
      Error(err?.response?.data?.message);
    },
  });

  useEffect(() => {
    handleGetEmployees();
  }, []);

  const handleGetEmployees = async (inputValue, callback) => {
    const { data } = await getMinEmployees(inputValue);
    const formatted = [];
    data?.employees?.forEach((emp) => {
      formatted.push({
        label: emp?.auth?.email,
        value: emp?._id,
      });
    });

    if (callback) await callback(formatted);
    else setEmployees(formatted);
  };

  const debounceLoadEmployees = (inputValue, callBack) =>
    debounce(handleGetEmployees(inputValue, callBack), 500, {
      leading: true,
    });

  const handleSendMessage = () => {
    if (messageTo === "all") {
      sendToAll({ message });
    } else {
      if (selectedEmployees.length > 0) {
        send({ employeeIds: selectedEmployees, message });
      }
    }
  };

  // console.log("Send Message", selectedEmployees);

  return (
    <div>
      <div
        className="modal fade sendMessage"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content site-modal">
            <h3 className="medium text-center">Send Message</h3>
            <div className="text-left">
              <form action>
                <label htmlFor className="p-lg ubuntu">
                  Select List
                </label>
                <div className="d-flex align-items-center">
                  <p className="mb-0" onClick={() => setMessageTo("all")}>
                    <input type="checkbox" id="c10" name="send" value="all" checked={messageTo === "all"} />
                    <label htmlFor="c10">All</label>
                  </p>
                  <p className="mb-0 ml-4" onClick={() => setMessageTo("custom")}>
                    <input type="checkbox" id="c11" name="send" value="custom" checked={messageTo === "custom"} />
                    <label htmlFor="c11">Custom</label>
                  </p>
                </div>
                {messageTo === "custom" && (
                  <>
                    <label htmlFor className="p-lg mt-3 ubuntu">
                      Select Employee
                    </label>
                    <AsyncSelect
                      placeholder="Select Employees"
                      isMulti
                      cacheOptions
                      defaultOptions={employees}
                      value={selectedEmployees}
                      loadOptions={(inputValue, callBack) => debounceLoadEmployees(inputValue, callBack)}
                      onChange={(opt) => {
                        setSelectedEmployees(opt);
                      }}
                      styles={{
                        control: (styles) => ({
                          ...styles,
                          cursor: "pointer",
                        }),
                      }}
                    />
                  </>
                )}
                {/* <select name className="site-input py-2 px-3 w-100" id>
                  <option value>Multiselect Dropdown</option>
                </select> */}
                <label htmlFor className="p-lg mt-3 ubuntu">
                  Message
                </label>
                <textarea
                  name
                  placeholder="Enter Message"
                  id
                  cols={30}
                  rows={5}
                  className="site-input py-2 px-3 w-100"
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                />
                <div className="text-center">
                  <Button
                    className="site-btn grey-btn w-100 py-2 px-4 mt-4"
                    loading={sendSingleLoading || sendToAllLoading}
                    onClick={handleSendMessage}
                  >
                    Send Message
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade messageSent"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content site-modal">
            <i className="close modal-close" data-dismiss="modal" aria-label="Close">
              <svg xmlns="http://www.w3.org/2000/svg" width={21} height={21} viewBox="0 0 21 21">
                <path
                  id="Icon_material-close"
                  data-name="Icon material-close"
                  d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
                  transform="translate(-7.5 -7.5)"
                  fill="#545454"
                />
              </svg>
            </i>
            <div className="text-center">
              <img src="images/check.png" alt="" className="img-fluid" />
              <p className="d-blue-text mt-3 p-lg">Message Successfully Sent</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
