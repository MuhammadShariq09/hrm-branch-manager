import React from "react";
import SendMessage from "./SendMessage";

export default function Messages({ history }) {
  return (
    <div>
      <div className="main">
        <div className="main-body">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12 px-0 px-lg-2">
                <div className="white-card mt-3 px-2 py-3 px-sm-3">
                  <h4>Messages</h4>
                  <div className="grey-bg p-2 mt-3 pl-lg-4 d-sm-flex align-items-center justify-content-between">
                    <p className="ubuntu p-lg mb-0">Admin</p>
                    <div className="d-flex align-items-center">
                      {/* <p className="mb-0 mt-sm-0 mt-2 mr-2">1 New Message</p> */}
                      <button className="site-btn px-4 py-2 mt-sm-0 mt-2" onClick={() => history.push("/messages/admin/view")}>
                        View Message
                      </button>
                    </div>
                  </div>
                  <div className="grey-bg p-2 mt-3 pl-lg-4 d-sm-flex align-items-center justify-content-between">
                    <p className="ubuntu p-lg mb-0">Employees</p>
                    <div className="d-sm-flex align-items-center">
                      <button
                        className="site-btn white-border px-3 mr-sm-2 py-2 mt-sm-0 mt-2"
                        onClick={() => history.push("/messages/employees/view")}
                      >
                        View Message
                      </button>
                      <button className="site-btn px-4 py-2 mt-sm-0 mt-2" data-target=".sendMessage" data-toggle="modal">
                        Send Message
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <SendMessage />
    </div>
  );
}
