import React, { useState } from "react";
import { useMutation } from "react-query";

import InputPassword from "../../../Components/Elements/Form/InputPassword";
import Button from "../../../Components/Elements/Form/Button";
import { changePassword } from "../../../Apis";
import Error from "../../../Components/Elements/Modals/Modal.Error";
import Success from "../../../Components/Elements/Modals/Modal.Success";

export default function ChangePassword({ history }) {
  const [state, setState] = useState({
    currentPassword: "",
    newPassword: "",
    confirmPassword: "",
  });

  const { mutate, isLoading } = useMutation((data) => changePassword(data), {
    retry: false,
    onSuccess: (res) => {
      Success(res?.data?.message);
      history.replace("/profile");
    },
    onError: (err) => Error(err?.response?.data?.message),
  });

  const handleSubmit = () => {
    if (state?.newPassword !== state?.currentPassword) {
      if (state?.newPassword === state?.confirmPassword) {
        mutate(state);
      } else Error("Password does not match");
    } else
      Error("Your new password must be different from your previous password.");
  };

  return (
    <div className="main">
      <div className="main-body">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 px-0 px-lg-2">
              <div className="white-card mt-3 px-2 pb-5 py-3 px-sm-3">
                <h4>Update Password</h4>
                <div className="orange-bg mt-4 py-xl-5">
                  <div className="row">
                    <div className="col-xl-7 mx-auto py-5 col-lg-8">
                      <form action="#_" className="my-lg-5">
                        <div className="password-card mt-2 py-4 px-3">
                          <label htmlFor className="mt-0 ubuntu p-lg">
                            Current Password<span className="red-text">*</span>
                          </label>
                          <div className="position-relative">
                            <InputPassword
                              type="password"
                              placeholder="Enter Current Password"
                              className="site-input transparent-bg px-3 py-2 w-100"
                              value={state?.currentPassword}
                              onChange={(currentPassword) =>
                                setState({ ...state, currentPassword })
                              }
                            />
                          </div>
                          <label htmlFor className="mt-3 ubuntu p-lg">
                            New Password<span className="red-text">*</span>
                          </label>
                          <div className="position-relative">
                            <InputPassword
                              type="password"
                              placeholder="Enter New Password"
                              className="site-input transparent-bg px-3 py-2 w-100"
                              value={state?.newPassword}
                              onChange={(newPassword) =>
                                setState({ ...state, newPassword })
                              }
                            />
                          </div>
                          <label htmlFor className="mt-3 ubuntu p-lg">
                            Confirm New Password
                            <span className="red-text">*</span>
                          </label>
                          <div className="position-relative">
                            <InputPassword
                              type="password"
                              placeholder="Confirm New Password"
                              className="site-input transparent-bg px-3 py-2 w-100"
                              value={state?.confirmPassword}
                              onChange={(confirmPassword) =>
                                setState({ ...state, confirmPassword })
                              }
                            />
                          </div>
                          <div className="text-center">
                            <Button
                              className="site-btn grey-btn py-2 w-100 mt-lg-5 mt-4"
                              loading={isLoading}
                              onClick={handleSubmit}
                            >
                              Update
                            </Button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
