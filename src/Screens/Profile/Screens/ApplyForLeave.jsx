import React, { useState } from "react";

import Dialog from "../../../Components/Elements/Modals/Dialog";
import Button from "../../../Components/Elements/Form/Button";
import Calender from "./../../../Components/Elements/Form/Calender";
import { useMutation } from "react-query";
import { applyForLeave } from "../../../Apis/Leave";
import Confirmation from "../../../Components/Elements/Modals/Modal.Confirmation";
import Success from "../../../Components/Elements/Modals/Modal.Success";
import Error from "../../../Components/Elements/Modals/Modal.Error";

export default function ApplyForLeave() {
  const [to, setTo] = useState("");
  const [from, setFrom] = useState("");
  const [open1, setOpen1] = useState(false);
  const [open2, setOpen2] = useState(false);
  const [leave_type, setLeaveType] = useState("");
  const [reason, setReason] = useState("");

  const { mutate, isLoading } = useMutation((data) => applyForLeave(data), {
    onSuccess: (res) => {
      window.$(".leaveForm").modal("hide");
      window.$(".leaveSubmitted").modal("show");
    },
    onError: (err) => {
      Error(err?.response?.data?.message);
    },
  });

  const handleSubmit = () => {
    mutate({
      from,
      to,
      leave_type,
      reason,
    });
  };

  return (
    <Dialog target="leaveForm" isClose={true}>
      <h3 className="medium text-center">Leave Form</h3>
      <div className="text-left">
        <form action>
          <label htmlFor className="p-lg ubuntu">
            From
          </label>
          <Calender
            className="site-input py-2 px-3 w-100"
            value={from}
            maxDate={to}
            open={open1}
            onChange={(date) => {
              setFrom(date);
            }}
            onInputClick={() => {
              setOpen1(!open1);
            }}
            onClickOutside={() => setOpen1(false)}
          />
          <label htmlFor className="p-lg mt-3 ubuntu">
            To
          </label>
          <Calender
            className="site-input py-2 px-3 w-100"
            value={to}
            open={open2}
            // maxDate={from || new Date()}
            minDate={from}
            onChange={(to) => {
              setTo(to);
            }}
            onInputClick={() => {
              setOpen2(!open2);
            }}
            onClickOutside={() => setOpen2(false)}
          />
          <label htmlFor className="p-lg mt-3 ubuntu">
            Select Leave Type:
          </label>
          <select
            className="site-input px-3 py-2 w-100"
            value={leave_type}
            onChange={(e) => setLeaveType(e.target.value)}
          >
            <option value>Select Leave Type</option>
            {["Casual", "Medical", "Annual"].map((el, i) => (
              <option key={i} value={el}>
                {el}
              </option>
            ))}
          </select>
          <label htmlFor className="p-lg mt-3 ubuntu">
            Reason
          </label>
          <textarea
            name
            placeholder="Enter Reason"
            id
            cols={30}
            rows={5}
            className="site-input py-2 px-3 w-100"
            defaultValue={""}
            onChange={(e) => {
              setReason(e.target.value);
            }}
          />
          <Button
            loading={isLoading}
            onClick={handleSubmit}
            className="site-btn grey-btn py-2 w-100 mt-lg-5 mt-4"
          >
            Apply
          </Button>
        </form>
      </div>
    </Dialog>
  );
}
