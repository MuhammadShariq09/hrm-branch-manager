import React from "react";

import AttendanceReport from "../../../Components/AttendanceReport";

export default function MyAttendanceReport(props) {
  return (
    <>
      <AttendanceReport {...props} />
    </>
  );
}
