import React from "react";
import MonthlyReport from "../../../Components/MonthlyReport";

export default function MonthlyReports(props) {
  return <MonthlyReport {...props} />;
}
