import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import { useMutation } from "react-query";
import { useRecoilState } from "recoil";

import Input from "../../../Components/Elements/Form/Input";
import Button from "../../../Components/Elements/Form/Button";
import Success from "../../../Components/Elements/Modals/Modal.Success";
import Error from "../../../Components/Elements/Modals/Modal.Error";
import ImageSelector from "../Components/ImageSelector";
import { userState } from "../../../Recoil";
import { updateProfile } from "../../../Apis";
import ApplyForLeave from "./ApplyForLeave";

export default function Profile({ history }) {
  const location = useLocation();
  const [user, setUser] = useRecoilState(userState);
  const [is_edit, setIsEdit] = useState(false);
  const [userInfo, setUserInfo] = useState({
    first_name: user?.first_name,
    last_name: user?.last_name,
    user_image: user?.user_image,
  });

  useEffect(() => {
    if (location.pathname === "/profile/edit") {
      setIsEdit(true);
    } else {
      setIsEdit(false);
    }
  }, [location.pathname]);

  const { mutate: _updateProfile, isLoading } = useMutation(
    (data) => updateProfile(data),
    {
      onSuccess: (res) => {
        Success(res?.data?.message);
        // console.log("debug",res?.)
        setUser(res?.data?.branch_manager);
      },
      onError: (err) => {
        Error(err?.response?.data?.message);
      },
    }
  );

  const handleUpdateProfile = () => {
    const form_data = new FormData();
    form_data.append("first_name", userInfo.first_name);
    form_data.append("last_name", userInfo.last_name);
    form_data.append("user_image", userInfo.user_image);
    _updateProfile(form_data);
  };

  return (
    <div>
      <div className="main">
        <div className="main-body">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12 px-0 px-lg-2">
                <div className="white-card mt-3 px-2 py-3 px-sm-3">
                  <div className="d-flex align-items-center justify-content-between">
                    <h4 className="mb-0">Profile</h4>
                    <button
                      className="site-btn grey-btn px-4 py-2"
                      data-target=".leaveForm"
                      data-toggle="modal"
                      // onClick={() => history.push("/apply-for-leave")}
                    >
                      Apply For Leave
                    </button>
                  </div>
                  <div className="orange-bg mt-3 py-lg-5 p-sm-4 p-3">
                    <div className="row">
                      <div className="col-xl-7 ml-auto col-lg-8 col-12">
                        <div className="d-md-flex justify-content-between">
                          <div>
                            {is_edit ? (
                              <ImageSelector
                                value={userInfo?.user_image}
                                onChange={(user_image) =>
                                  setUserInfo({
                                    ...userInfo,
                                    user_image,
                                  })
                                }
                              />
                            ) : (
                              <div className="profile-img mx-auto mx-md-0">
                                <img
                                  src={
                                    userInfo?.user_image
                                      ? userInfo.user_image
                                      : "images/profile-img.png"
                                  }
                                  alt=""
                                  className="img-fluid"
                                />
                              </div>
                            )}
                            <div className="text-md-left text-center">
                              <Link
                                to="/profile/change-password"
                                className="orange-text medium p-sm"
                              >
                                Change Password
                              </Link>
                            </div>
                          </div>
                          <div className="mt-2 text-right text-md-left mt-md-0">
                            <Link
                              to="/profile/monthly-reports"
                              className="orange-text d-block medium p-sm"
                            >
                              View Monthly Report
                            </Link>
                            <Link
                              to="/profile/attendance-report"
                              className="orange-text d-block medium p-sm"
                            >
                              View Attendance Report
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row mt-2">
                      <div className="col-xl-5 col-lg-7 mx-auto">
                        <div className="row">
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu ml-sm-4 medium">
                              First Name:
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            {is_edit ? (
                              <Input
                                value={userInfo?.first_name}
                                onChange={(first_name) =>
                                  setUserInfo({ ...userInfo, first_name })
                                }
                                className="site-input yellow-input px-3 py-2 w-100"
                              />
                            ) : (
                              <p className="mb-0 d-grey-text">
                                {user?.first_name}
                              </p>
                            )}
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu ml-sm-4 medium">
                              Last Name:
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            {is_edit ? (
                              <Input
                                value={userInfo?.last_name}
                                onChange={(last_name) =>
                                  setUserInfo({ ...userInfo, last_name })
                                }
                                className="site-input yellow-input px-3 py-2 w-100"
                              />
                            ) : (
                              <p className="mb-0 d-grey-text">
                                {user?.last_name}
                              </p>
                            )}
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu ml-sm-4 medium">
                              Email Address:
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {user?.auth?.email}
                            </p>
                          </div>
                          <div className="col-12 text-center mt-4">
                            <Button
                              className="site-btn grey-btn px-5 py-2"
                              style={{
                                width: 150,
                              }}
                              loading={isLoading}
                              onClick={
                                is_edit
                                  ? handleUpdateProfile
                                  : () => history.push("/profile/edit")
                              }
                            >
                              {is_edit ? "Update" : "Edit"}
                            </Button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ApplyForLeave />
    </div>
  );
}
