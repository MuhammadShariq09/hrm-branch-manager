import React, { useState } from "react";

import Contact from "../../Components/Contact";
import Footer from "../../Components/Footer";
import PublicHeader from "../../Components/PublicHeader";

export default function LandingPage() {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(!showModal);
  };
  return (
    <div className="main">
      <PublicHeader showContactModal={handleShowModal} />

      <section className="banner">
        <div className="container">
          <div className="banner-inner">
            <div className="row">
              <div className="col-xl-9 col-lg-10 mx-auto">
                <h1>The Ultimate Human &amp; Resource Management Solution</h1>
                <p className="px-lg-5 mt-2">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam.
                </p>
                <button className="site-btn px-sm-5 mt-3 px-4 py-2 pur-shadow">
                  Get Started Now
                </button>
              </div>
              <div className="col-lg-11 col-xl-10 mx-auto">
                <img
                  src="images/banner-img.png"
                  alt=""
                  className="img-fluid banner-img w-100"
                />
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="integrating-functions pb-lg-5 pb-4">
        <div className="container">
          <div className="row">
            <div className="col-lg-8 mx-auto">
              <h3 className="px-xl-4">
                Integrating many functions suitable for your company's
                management
              </h3>
              <p className="grey-text px-xl-5 mt-2">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam.
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4 col-md-6 mr-lg-0 mt-3">
              <div className="integrate-card">
                <img
                  src="images/integrate-1.png"
                  alt=""
                  className="img-fluid integrate-img"
                />
              </div>
              <p className="p-lg ubuntu">Monthly Attendance Report</p>
            </div>
            <div className="col-lg-4 col-md-6 mr-lg-0 mt-3">
              <div className="integrate-card">
                <img
                  src="images/integrate-2.png"
                  alt=""
                  className="img-fluid integrate-img"
                />
              </div>
              <p className="p-lg ubuntu">Payroll Management</p>
            </div>
            <div className="col-lg-4 col-md-6 mr-lg-0 mt-3">
              <div className="integrate-card">
                <img
                  src="images/integrate-3.png"
                  alt=""
                  className="img-fluid integrate-img"
                />
              </div>
              <p className="p-lg ubuntu">Advance Leave Management</p>
            </div>
            <div className="col-lg-4 col-md-6 mr-lg-0 mt-3">
              <div className="integrate-card">
                <img
                  src="images/integrate-4.png"
                  alt=""
                  className="img-fluid integrate-img"
                />
              </div>
              <p className="p-lg ubuntu">Learning &amp; Development</p>
            </div>
            <div className="col-lg-4 col-md-6 mr-lg-0 mt-3">
              <div className="integrate-card">
                <img
                  src="images/integrate-5.png"
                  alt=""
                  className="img-fluid integrate-img"
                />
              </div>
              <p className="p-lg ubuntu">Employee On-boarding</p>
            </div>
            <div className="col-lg-4 col-md-6 mr-lg-0 mt-3">
              <div className="integrate-card">
                <img
                  src="images/integrate-6.png"
                  alt=""
                  className="img-fluid integrate-img"
                />
              </div>
              <p className="p-lg ubuntu">Employee Asset Management</p>
            </div>
          </div>
        </div>
      </section>

      <section className="manage-everything py-lg-4 py-3">
        <div className="container">
          <div className="row">
            <div className="col-lg-5 mt-3 my-lg-auto">
              <h3>
                Manage everything <br /> in one workspace
              </h3>
              <p className="grey-text mt-2">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum. Stet clita kasd gubergren
              </p>
            </div>
            <div className="col-lg-7 mt-3">
              <img
                src="images/maange-everything.png"
                alt=""
                className="img-fluid"
              />
            </div>
          </div>
        </div>
      </section>

      <section className="outstanding-features py-lg-4 py-3">
        <div className="container">
          <div className="row">
            <div className="col-lg-7 order-lg-1 order-2 mt-3">
              <img
                src="images/outstanding-features.png"
                alt=""
                className="img-fluid"
              />
            </div>
            <div className="col-lg-5 order-lg-2 order-1 mt-3 my-lg-auto">
              <h3>
                Outstanding Features <br /> &amp; Automations
              </h3>
              <p className="grey-text mt-2">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
                takimata
              </p>
              <button className="site-btn grey-btn px-md-5 px-4 mt-4 py-2">
                Get Started
              </button>
            </div>
          </div>
        </div>
      </section>

      <section className="testimonials">
        <div className="container">
          <div className="row">
            <div className="col-lg-7 mt-4 mx-auto">
              <h3>Testimonials from our customers</h3>
              <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy{" "}
              </p>
              <div className="owl-carousel testimonial-carousel owl-theme">
                <div className="item">
                  <img
                    src="images/testimonial-img.png"
                    alt=""
                    className="testimonial-img img-fluid"
                  />
                  <p className="mt-3 grey-text">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat, sed diam voluptua. At vero eos et accusam et
                    justo duo dolores et ea rebum.
                  </p>
                  <p className="p-lg ubuntu">Brooklyn Simmons</p>
                </div>
                <div className="item">
                  <img
                    src="images/testimonial-img.png"
                    alt=""
                    className="testimonial-img img-fluid"
                  />
                  <p className="mt-3 grey-text">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat, sed diam voluptua. At vero eos et accusam et
                    justo duo dolores et ea rebum.
                  </p>
                  <p className="p-lg ubuntu">Brooklyn Simmons</p>
                </div>
                <div className="item">
                  <img
                    src="images/testimonial-img.png"
                    alt=""
                    className="testimonial-img img-fluid"
                  />
                  <p className="mt-3 grey-text">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat, sed diam voluptua. At vero eos et accusam et
                    justo duo dolores et ea rebum.
                  </p>
                  <p className="p-lg ubuntu">Brooklyn Simmons</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="better-work pb-lg-5 pb-4 pt-lg-4 pt-3">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="better-work-card p-lg-5 p-sm-4 p-3">
                <div className="row">
                  <div className="col-lg-10 mx-auto">
                    <h2>Get better work done</h2>
                    <p className="mt-3 px-xl-4">
                      Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                      sed diam nonumy eirmod tempor invidunt ut labore et dolore
                      magna aliquyam erat, sed diam voluptua. At vero eos et
                      accusam et justo duo dolores et ea rebum. Stet clita kasd
                      gubergren, no sea takimata
                    </p>
                    <button className="site-btn px-5 py-2 mt-3">
                      Get Started
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
      <Contact hideModal={handleShowModal} />
    </div>
  );
}
