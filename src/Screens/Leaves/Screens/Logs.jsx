import React, { useState, useEffect } from "react";
import { useQuery } from "react-query";
import { useMutation } from "react-query";

import { queryClient } from "../../..";
import { changeLeaveStatus, getLeaveLogs } from "../../../Apis/Leave";
import Table from "../../../Components/Table";
import Filters from "../../../Components/Filters";
import { format_date } from "../../../Utils/helpers";
import { Link } from "react-router-dom";
import Dialog from "../../../Components/Elements/Modals/Dialog";
import ConfirmationDialog from "../../../Components/Elements/Modals/Dialog.Confirm";
import SuccessDialog from "../../../Components/Elements/Modals/Dialog.Success";
import Error from "../../../Components/Elements/Modals/Modal.Error";

export default function Logs() {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [search_string, setSearchString] = useState("");
  const [status, setStatus] = useState("");
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [leave_id, setLeaveId] = useState();
  const [reject_reason, setRejectResaon] = useState("");
  const statusOptions = [
    { label: "Pending", value: "Pending" },
    { label: "Approved", value: "Approved" },
    { label: "Rejected", value: "Rejected" },
  ];

  const { isFetching, isLoading, data, refetch } = useQuery(
    ["leave_logs", page, perPage, search_string, status, from, to],
    () => getLeaveLogs(page, perPage, search_string, status, from, to),
    { keepPreviousData: true }
  );

  // Prefetch the next page!
  useEffect(() => {
    if (data?.data?.logs?.hasNextPage) {
      queryClient.prefetchQuery(
        ["leave_logs", page + 1, perPage, search_string, status, from, to],
        () => getLeaveLogs(page + 1, perPage, search_string, from, to, status)
      );
    }
  }, [data, page, queryClient]);

  const { mutate: _changeLeaveStatus, isLoading: loadingLeaveStatus } =
    useMutation((data) => changeLeaveStatus(data), {
      onSuccess: (res) => {
        refetch();
        if (res?.data?.message === "Leave Rejected") {
          window.$(".rejectReason").modal("hide");
          window.$(".leaveRejected").modal("show");
        } else {
          window.$(".approveLeave").modal("hide");
          window.$(".leaveApproved").modal("show");
        }
      },
      onError: (err) => {
        console.log("debug", err);
        Error(err?.response?.data?.message);
      },
    });

  return (
    <div className="main">
      <div className="main-body">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 px-0 px-lg-2">
              <div className="white-card mt-3 px-2 py-3 px-sm-3">
                <h5 className="mt-0 mb-0">Leaves</h5>
                <div className="orange-bg d-md-flex flex-wrap align-items-center mt-3 p-3">
                  <Filters
                    perPage={perPage}
                    setPerPage={setPerPage}
                    statusOptions={statusOptions}
                    status={status}
                    setStatus={setStatus}
                    dateFilters={true}
                    dateTo={to}
                    setDateTo={setTo}
                    dateFrom={from}
                    setDateFrom={setFrom}
                    setSearchString={setSearchString}
                    isFetching={isFetching}
                    // isLoading={isLoading || loadingLeaveStatus}
                  />
                </div>
              </div>
              <div className="maain-tabble manager-table table-responsive">
                <Table
                  headings={[
                    "S. No",
                    "Emp.First Name",
                    "	Emp.Last Name",
                    "From",
                    "To",
                    "Status",
                    "Action",
                  ]}
                  perPage={perPage}
                  setPerPage={setPerPage}
                  totalPages={data?.data?.logs?.totalPages}
                  setPage={setPage}
                  data={data?.data?.logs?.docs}
                  isFetching={isFetching}
                  isLoading={isLoading || loadingLeaveStatus}
                >
                  <tbody className="orange-bg p-3">
                    {data?.data?.logs?.docs?.map((el, i) => (
                      <tr>
                        <td>{i + 1}</td>
                        <td>{el?.employee?.first_name}</td>
                        <td>{el?.employee?.last_name}</td>
                        <td>{format_date(el?.from)}</td>
                        <td>{format_date(el?.to)}</td>
                        <td>
                          <p className="pending d-inline-block w-108 d-flex align-items-center justify-content-center mb-0">
                            {el?.status}
                          </p>
                        </td>
                        <td className="text-center">
                          <div className="btn-group ml-2 mb-1">
                            <button
                              type="button"
                              className="transparent-btn"
                              data-toggle="dropdown"
                              aria-haspopup="true"
                              aria-expanded="false"
                            >
                              {" "}
                              <i className="fa fa-ellipsis-v" />
                            </button>
                            <div className="dropdown-menu custom-dropdown">
                              <Link
                                to={`/leave/details/${el?._id}`}
                                className="dropdown-item"
                              >
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="13.465"
                                  height="10.324"
                                  viewBox="0 0 13.465 10.324"
                                >
                                  <g
                                    id="stack-of-square-papers"
                                    transform="translate(0 -35.441)"
                                  >
                                    <g
                                      id="Group_1057"
                                      data-name="Group 1057"
                                      transform="translate(0 35.441)"
                                    >
                                      <path
                                        id="Path_4860"
                                        data-name="Path 4860"
                                        d="M.218,38.554l6.373,2.728a.352.352,0,0,0,.141.029.361.361,0,0,0,.141-.029l6.373-2.728a.359.359,0,0,0-.014-.665L6.86,35.465a.357.357,0,0,0-.255,0L.232,37.889a.359.359,0,0,0-.013.665Zm6.514-2.37,5.415,2.06L6.733,40.562,1.317,38.244Z"
                                        transform="translate(0 -35.441)"
                                        fill="#545454"
                                      />
                                      <path
                                        id="Path_4861"
                                        data-name="Path 4861"
                                        d="M.219,125.879l6.514,2.788,6.514-2.788a.359.359,0,0,0-.282-.66l-6.232,2.668L.5,125.22a.359.359,0,1,0-.282.66Z"
                                        transform="translate(-0.001 -121.214)"
                                        fill="#545454"
                                      />
                                      <path
                                        id="Path_4862"
                                        data-name="Path 4862"
                                        d="M.219,158.278l6.514,2.788,6.514-2.788a.359.359,0,1,0-.282-.66l-6.232,2.668L.5,157.618a.359.359,0,1,0-.282.66Z"
                                        transform="translate(-0.001 -152.178)"
                                        fill="#545454"
                                      />
                                      <path
                                        id="Path_4863"
                                        data-name="Path 4863"
                                        d="M.219,190.682l6.514,2.788,6.514-2.788a.359.359,0,0,0-.282-.66L6.733,192.69.5,190.022a.359.359,0,1,0-.282.66Z"
                                        transform="translate(-0.001 -183.146)"
                                        fill="#545454"
                                      />
                                    </g>
                                  </g>
                                </svg>
                                Details
                              </Link>
                              {el.status === "Pending" && (
                                <a
                                  className="dropdown-item"
                                  data-toggle="modal"
                                  data-target=".rejectLeave"
                                  onClick={() => setLeaveId(el?._id)}
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="14.42"
                                    height="14.215"
                                    viewBox="0 0 14.42 14.215"
                                  >
                                    <g
                                      id="Group_1083"
                                      data-name="Group 1083"
                                      transform="translate(-9385.145 -8660.973)"
                                    >
                                      <path
                                        id="Path_3560"
                                        data-name="Path 3560"
                                        d="M8.346,236.459a5.034,5.034,0,0,0-3.238-1.16H5.1a5.019,5.019,0,0,0-3.61,1.512A5.38,5.38,0,0,0,0,240.4l8.255-.017a3.568,3.568,0,0,1,.09-3.922Zm0,0"
                                        transform="translate(9385.145 8433.584)"
                                        fill="#666"
                                      />
                                      <path
                                        id="Path_3561"
                                        data-name="Path 3561"
                                        d="M247.316,237.4a3.091,3.091,0,1,0,2.212.905A3.117,3.117,0,0,0,247.316,237.4Z"
                                        transform="translate(9149.131 8431.555)"
                                        fill="#d50c0c"
                                      />
                                      <path
                                        id="Icon_ionic-ios-close"
                                        data-name="Icon ionic-ios-close"
                                        d="M13.02,12.691l1-1a.235.235,0,1,0-.332-.332l-1,1-1-1a.235.235,0,1,0-.332.332l1,1-1,1a.235.235,0,0,0,.332.332l1-1,1,1a.235.235,0,0,0,.332-.332Z"
                                        transform="translate(9383.76 8659.379)"
                                        fill="#f2f2f2"
                                      />
                                      <path
                                        id="Path_4857"
                                        data-name="Path 4857"
                                        d="M69.446,3.075A2.973,2.973,0,1,1,66.473.1,2.973,2.973,0,0,1,69.446,3.075Zm0,0"
                                        transform="translate(9323.773 8660.871)"
                                        fill="#666"
                                      />
                                    </g>
                                  </svg>
                                  Reject
                                </a>
                              )}
                              {el.status === "Pending" && (
                                <a
                                  className="dropdown-item"
                                  data-toggle="modal"
                                  data-target=".approveLeave"
                                  onClick={() => setLeaveId(el?._id)}
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="14.42"
                                    height="14.188"
                                    viewBox="0 0 14.42 14.188"
                                  >
                                    <g
                                      id="user_3_"
                                      data-name="user (3)"
                                      transform="translate(0 -0.102)"
                                    >
                                      <path
                                        id="Path_3559"
                                        data-name="Path 3559"
                                        d="M69.446,3.075A2.973,2.973,0,1,1,66.473.1,2.973,2.973,0,0,1,69.446,3.075Zm0,0"
                                        transform="translate(-61.371 0)"
                                        fill="#666"
                                      />
                                      <path
                                        id="Path_3560"
                                        data-name="Path 3560"
                                        d="M8.346,236.459a5.034,5.034,0,0,0-3.238-1.16H5.1a5.019,5.019,0,0,0-3.61,1.512A5.38,5.38,0,0,0,0,240.4l8.255-.017a3.568,3.568,0,0,1,.09-3.922Zm0,0"
                                        transform="translate(0 -227.314)"
                                        fill="#666"
                                      />
                                      <path
                                        id="Path_3561"
                                        data-name="Path 3561"
                                        d="M247.316,237.4a3.091,3.091,0,1,0,2.212.905A3.117,3.117,0,0,0,247.316,237.4Zm1.488,2.537-1.874,1.525a.229.229,0,0,1-.147.054.226.226,0,0,1-.178-.084l-.808-.942a.235.235,0,0,1,.359-.305l.657.768,1.7-1.381a.234.234,0,0,1,.329.034A.238.238,0,0,1,248.8,239.936Zm0,0"
                                        transform="translate(-236.014 -229.344)"
                                        fill="#06a22f"
                                      />
                                    </g>
                                  </svg>
                                  Approved
                                </a>
                              )}
                            </div>
                          </div>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ConfirmationDialog
        target="approveLeave"
        isClose={true}
        onSuccess={() =>
          _changeLeaveStatus({
            id: leave_id,
            status: "Approved",
          })
        }
      >
        Are you sure you want to approve this leave?
      </ConfirmationDialog>
      <SuccessDialog target="leaveApproved">
        Leave Approved Successfully!
      </SuccessDialog>
      <ConfirmationDialog
        target="rejectLeave"
        isClose={true}
        onSuccessTarget="rejectReason"
      >
        Are you sure you want to reject this leave?
      </ConfirmationDialog>
      <Dialog target="rejectReason">
        <div className="text-left">
          <h4 className="text-center medium">Rejection Reason</h4>
          <label htmlFor className="ubuntu p-lg mt-2">
            Reason:
          </label>
          <textarea
            name
            id
            cols={30}
            rows={7}
            placeholder="Enter Rejection Resaon"
            className="site-input p-3 w-100"
            defaultValue={""}
            onChange={(e) => setRejectResaon(e.target.value)}
          />
          <button
            className="site-btn mt-4 grey-btn w-100 py-2"
            onClick={() => {
              _changeLeaveStatus({
                id: leave_id,
                status: "Rejected",
                rejectReason: reject_reason,
              });
            }}
            data-dismiss="modal"
            aria-label="Close"
          >
            Submit
          </button>
        </div>
      </Dialog>
      <SuccessDialog target="leaveRejected">
        Leave Rejected Successfully!
      </SuccessDialog>
    </div>
  );
}
