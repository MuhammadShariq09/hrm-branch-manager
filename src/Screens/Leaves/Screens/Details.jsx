import React, { useEffect, useState } from "react";
import { useMutation } from "react-query";
import { changeLeaveStatus, getLeaveDetails } from "../../../Apis/Leave";
import { format_date } from "../../../Utils/helpers";
import LoadingView from "../../../Components/LoadingView";
import Dialog from "../../../Components/Elements/Modals/Dialog";
import ConfirmationDialog from "../../../Components/Elements/Modals/Dialog.Confirm";
import SuccessDialog from "../../../Components/Elements/Modals/Dialog.Success";
import Error from "../../../Components/Elements/Modals/Modal.Error";

export default function Details({ match, history }) {
  const id = match.params.id;
  const [reject_reason, setRejectResaon] = useState("");
  const [details, setDetails] = useState();
  const { mutate, isLoading } = useMutation((id) => getLeaveDetails(id), {
    onSuccess: (res) => {
      setDetails(res?.data?.leave);
    },
    onError: (err) => Error(err?.response?.data?.message),
  });

  useEffect(() => {
    mutate(id);
  }, [id]);

  const { mutate: _changeLeaveStatus, isLoading: changeLeaveStatusLoading } =
    useMutation((data) => changeLeaveStatus(data), {
      onSuccess: (res) => {
        if (res?.data?.message === "Leave Rejected") {
          window.$(".rejectReason").modal("hide");
          window.$(".leaveRejected").modal("show");
          mutate(id);
        } else {
          window.$(".approveLeave").modal("hide");
          window.$(".leaveApproved").modal("show");
          mutate(id);
        }
      },
      onError: (err) => {
        console.log("debug", err);
        Error(err?.response?.data?.message);
      },
    });

  return (
    <div className="main">
      {isLoading ? (
        <LoadingView />
      ) : (
        <div className="main-body">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12 px-0 px-lg-2">
                <div className="white-card mt-3 px-2 py-3 px-sm-3">
                  <div className="d-sm-flex align-items-center justify-content-between">
                    <h4 className="mb-0">Leave Details</h4>
                    <button
                      onClick={() =>
                        history.push(`/employee/details/${details?.user?._id}`)
                      }
                      className="site-btn mt-sm-0 mt-2 yellow-border px-3 py-2"
                    >
                      Employee Details
                    </button>
                  </div>
                  <div className="orange-bg mt-3 py-lg-5 p-sm-4 p-3">
                    <div className="row">
                      <div className="col-12 text-right">
                        <p className="mb-0 d-grey-text">
                          {format_date(details?.createdAt)}
                        </p>
                      </div>
                      <div className="col-12 text-center">
                        <div className="profile-img mx-auto">
                          <img
                            src="images/profile-img.png"
                            alt=""
                            className="img-fluid"
                          />
                        </div>
                        <h4 className="mt-3">
                          Status:{" "}
                          <span
                            className={
                              details?.status === "Pending"
                                ? "orange-text"
                                : details?.status === "Rejected"
                                ? "red-text"
                                : "d-green-text"
                            }
                          >
                            {" "}
                            {details?.status}
                          </span>
                        </h4>
                      </div>
                    </div>
                    <div className="row mt-2">
                      <div className="col-xl-6 col-lg-7 mx-auto">
                        <div className="row">
                          <div className="col-sm-7 mt-3">
                            <p className="mb-0 ubuntu ml-sm-4 medium">
                              Personnel First Name:
                            </p>
                          </div>
                          <div className="col-sm-5 mt-3">
                            <p className="mb-0 d-grey-text">
                              {details?.user?.first_name}
                            </p>
                          </div>
                          <div className="col-sm-7 mt-3">
                            <p className="mb-0 ubuntu ml-sm-4 medium">
                              Personnel Last Name:
                            </p>
                          </div>
                          <div className="col-sm-5 mt-3">
                            <p className="mb-0 d-grey-text">
                              {details?.user?.last_name}
                            </p>
                          </div>
                          <div className="col-sm-7 mt-3">
                            <p className="mb-0 ubuntu ml-sm-4 medium">From:</p>
                          </div>
                          <div className="col-sm-5 mt-3">
                            <p className="mb-0 d-grey-text">
                              {format_date(details?.from)}
                            </p>
                          </div>
                          <div className="col-sm-7 mt-3">
                            <p className="mb-0 ubuntu ml-sm-4 medium">To:</p>
                          </div>
                          <div className="col-sm-5 mt-3">
                            <p className="mb-0 d-grey-text">
                              {format_date(details?.to)}
                            </p>
                          </div>
                          <div className="col-sm-7 mt-3">
                            <p className="mb-0 ubuntu ml-sm-4 medium">
                              Employment status:
                            </p>
                          </div>
                          <div className="col-sm-5 mt-3">
                            <p className="mb-0 d-grey-text">
                              {details?.user?.employement_status}
                            </p>
                          </div>
                          <div className="col-sm-7 mt-3">
                            <p className="mb-0 ubuntu ml-sm-4 medium">Shift:</p>
                          </div>
                          <div className="col-sm-5 mt-3">
                            <p className="mb-0 d-grey-text">
                              {details?.user?.shift?.title}
                            </p>
                          </div>
                          <div className="col-sm-7 mt-3">
                            <p className="mb-0 ubuntu ml-sm-4 medium">
                              Branch:
                            </p>
                          </div>
                          <div className="col-sm-5 mt-3">
                            <p className="mb-0 d-grey-text">
                              {details?.user?.branch?.title}
                            </p>
                          </div>
                          <div className="col-sm-7 mt-3">
                            <p className="mb-0 ubuntu ml-sm-4 medium">
                              Leave Type:
                            </p>
                          </div>
                          <div className="col-sm-5 mt-3">
                            <p className="mb-0 d-grey-text">
                              {details?.leave_type}
                            </p>
                          </div>
                          <div className="col-sm-7 mt-3">
                            <p className="mb-0 ubuntu ml-sm-4 medium">
                              Leave Reason:
                            </p>
                          </div>
                          <div className="col-sm-5 mt-3">
                            <p className="mb-0 d-grey-text">
                              {details?.reason}
                            </p>
                          </div>
                          {details?.status === "Rejected" && (
                            <>
                              <div class="col-sm-7 mt-3">
                                <p class="mb-0 ubuntu ml-sm-4 medium">
                                  Rejection Reason:
                                </p>
                              </div>
                              <div class="col-sm-5 mt-3">
                                <p class="mb-0 d-grey-text">
                                  {details?.rejection_reason}
                                </p>
                              </div>
                            </>
                          )}
                          {details?.status === "Pending" && (
                            <div className="col-12 mt-3">
                              <div className="d-sm-flex align-items-center justify-content-center">
                                <button
                                  className="site-btn grey-btn px-5 py-2 mt-md-3 mr-sm-3"
                                  data-target=".approveLeave"
                                  data-toggle="modal"
                                >
                                  Approve
                                </button>
                                <button
                                  className="site-btn yellow-border px-5 py-2 mt-md-3"
                                  data-target=".rejectLeave"
                                  data-toggle="modal"
                                >
                                  Reject
                                </button>
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
      <ConfirmationDialog
        target="approveLeave"
        isClose={true}
        onSuccess={() =>
          _changeLeaveStatus({
            id,
            status: "Approved",
          })
        }
      >
        Are you sure you want to approve this leave?
      </ConfirmationDialog>
      <SuccessDialog target="leaveApproved">
        Leave Approved Successfully!
      </SuccessDialog>
      <ConfirmationDialog
        target="rejectLeave"
        isClose={true}
        onSuccessTarget="rejectReason"
      >
        Are you sure you want to reject this leave?
      </ConfirmationDialog>
      <Dialog target="rejectReason">
        <div className="text-left">
          <h4 className="text-center medium">Rejection Reason</h4>
          <label htmlFor className="ubuntu p-lg mt-2">
            Reason:
          </label>
          <textarea
            name
            id
            cols={30}
            rows={7}
            placeholder="Enter Rejection Resaon"
            className="site-input p-3 w-100"
            defaultValue={""}
            onChange={(e) => setRejectResaon(e.target.value)}
          />
          <button
            className="site-btn mt-4 grey-btn w-100 py-2"
            onClick={() => {
              _changeLeaveStatus({
                id,
                status: "Rejected",
                rejectReason: reject_reason,
              });
            }}
            data-dismiss="modal"
            aria-label="Close"
          >
            Submit
          </button>
        </div>
      </Dialog>
      <SuccessDialog target="leaveRejected">
        Leave Rejected Successfully!
      </SuccessDialog>
    </div>
  );
}
