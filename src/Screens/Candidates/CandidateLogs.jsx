import React, { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { Link } from "react-router-dom";
import { queryClient } from "../..";
import { getCandidatesLogs } from "../../Apis";
import Table from "../../Components/Table";
import Filters from "../../Components/Filters";
import { format_date } from "../../Utils/helpers";

export default function CandidateLogs() {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [search_string, setSearchString] = useState("");
  const [status, setStatus] = useState("");
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const statusOptions = [
    { label: "Pending", value: "Pending" },
    { label: "Under Review", value: "Under Review" },
    { label: "Accepted", value: "Accepted" },
    { label: "Rejected", value: "Rejected" },
  ];

  const { isFetching, isLoading, data } = useQuery(
    ["candidates_logs", page, perPage, search_string, status, from, to],
    () => getCandidatesLogs(page, perPage, search_string, status, from, to),
    { keepPreviousData: true }
  );

  // Prefetch the next page!
  useEffect(() => {
    if (data?.data?.logs?.hasNextPage) {
      queryClient.prefetchQuery(
        ["candidates_logs", page + 1, perPage, search_string, status, from, to],
        () =>
          getCandidatesLogs(page + 1, perPage, search_string, status, from, to)
      );
    }
  }, [data, page, queryClient]);

  return (
    <div className="main-body">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 px-0 px-lg-2">
            <div className="white-card mt-3 px-2 py-3 px-sm-3">
              <div className="d-sm-flex align-items-center justify-content-between">
                <h5 className="mt-0 mb-0">Potential Candidate</h5>
                <Link
                  to="/candidate/add"
                  className="site-btn px-4 py-2 grey-btn mt-3 mt-sm-0"
                >
                  Add Candidate
                </Link>
              </div>
              <div className="orange-bg d-md-flex flex-wrap align-items-center mt-3 p-3">
                <Filters
                  perPage={perPage}
                  setPerPage={setPerPage}
                  statusOptions={statusOptions}
                  status={status}
                  setStatus={setStatus}
                  dateFilters={true}
                  dateTo={to}
                  setDateTo={setTo}
                  dateFrom={from}
                  setDateFrom={setFrom}
                  setSearchString={setSearchString}
                  isFetching={isFetching}
                />
              </div>
              <div className="maain-tabble orange-bg mt-4 px-3 manager-table table-responsive">
                <Table
                  headings={[
                    "First Name",
                    "Last Name",
                    "Date",
                    "Email Address",
                    "Status",
                    "Action",
                  ]}
                  perPage={perPage}
                  setPerPage={setPerPage}
                  totalPages={data?.data?.logs?.totalPages}
                  setPage={setPage}
                  data={data?.data?.logs?.docs}
                  isFetching={isFetching}
                  isLoading={isLoading}
                >
                  <tbody>
                    {data?.data?.logs?.docs?.map((el, i) => (
                      <tr key={i}>
                        <td>{el?.first_name}</td>
                        <td>{el?.last_name}</td>
                        <td>{format_date(el?.createdAt)}</td>
                        <td>{el?.auth?.email}</td>
                        <td>{el?.status}</td>
                        <td>
                          <Link
                            to={`/candidate/details/${el?._id}`}
                            className="orange-text"
                          >
                            View
                          </Link>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
