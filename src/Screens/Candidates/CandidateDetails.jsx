import React, { useState } from "react";
import { useMutation, useQuery } from "react-query";
import {
  acceptCandidate,
  candidateDetails,
  rejectCandidate,
  underReviewCandidate,
} from "../../Apis";
import Button from "../../Components/Elements/Form/Button";
import Error from "../../Components/Elements/Modals/Modal.Error";
import ConfirmationDialog from "../../Components/Elements/Modals/Dialog.Confirm";
import SuccessDialog from "../../Components/Elements/Modals/Dialog.Success";
import Dialog from "../../Components/Elements/Modals/Dialog";
import LoadingView from "../../Components/LoadingView";
import { image_url } from "../../Utils/connection_strings";
import { changeRoute, closeModals } from "../../Utils/helpers";

export default function CandidateDetails({ match, history }) {
  const [rejectionReason, setRejectionReason] = useState("");
  const { data, isLoading, refetch } = useQuery(
    ["candidate_details", match.params.id],
    () => candidateDetails(match.params.id)
  );

  const { mutate: rejectMutate, isLoading: loadingReject } = useMutation(
    (id, rejectionReason) => rejectCandidate(id, rejectionReason),
    {
      onSuccess: (res) => {
        window.$(".rejectReason").modal("hide");
        window.$(".candidateRejected").modal("show");
        refetch();
      },
      onError: (err) => Error(err?.response?.data?.message),
    }
  );

  const { mutate: acceptMutate, isLoading: loadingAccept } = useMutation(
    (data) => acceptCandidate(data),
    {
      onSuccess: (res) => {
        window.$(".hireCandidate").modal("hide");
        window.$(".candidateHired").modal("show");
        changeRoute(history, `/employee/edit/${res?.data?.id}`);
      },
      onError: (err) => Error(err?.response?.data?.message),
    }
  );

  const { mutate: underReviewMutate, isLoading: loadingUnderReview } =
    useMutation((data) => underReviewCandidate(data), {
      onSuccess: (res) => {
        window.$(".underReview").modal("hide");
        window.$(".custom").modal("show");
        refetch();
      },
      onError: (err) => Error(err?.response?.data?.message),
    });

  if (isLoading) return <LoadingView />;

  return (
    <div className="main-body">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 px-0 px-lg-2">
            <div className="white-card mt-3 px-2 py-3 px-sm-3">
              <h4>View Candidate</h4>
              <div className="orange-bg p-2">
                <div className="grey-bg p-2 d-sm-flex align-items-center justify-content-between">
                  <div className="media align-items-center">
                    <div className="media-body ml-2">
                      <p className="ubuntu mb-0">
                        {data?.data?.user?.first_name}{" "}
                        {data?.data?.user?.last_name}
                      </p>
                      <p className="p-sm mb-0">
                        <span className="ubuntu">Employment Status: </span>{" "}
                        <span className="p-xs">
                          {" "}
                          {data?.data?.user?.status}
                        </span>
                      </p>
                    </div>
                  </div>
                  <a
                    target="_blank"
                    className="site-btn px-4 py-2 mt-sm-0 mt-2"
                    href={`${image_url}${data?.data?.user?.resume}`}
                  >
                    View Resume
                  </a>
                </div>
                <div className="px-2">
                  <p className="p-lg bold mt-3 ubuntu">User Details</p>
                  <div className="row">
                    <div className="col-xl-5 col-md-7">
                      <div className="row">
                        <div className="col-sm-6">
                          <p className="ubuntu bold p-md mb-2">First Name:</p>
                        </div>
                        <div className="col-sm-6">
                          <p className="p-md mb-2">
                            {data?.data?.user?.first_name}
                          </p>
                        </div>
                        <div className="col-sm-6">
                          <p className="ubuntu bold p-md mb-2">Last Name:</p>
                        </div>
                        <div className="col-sm-6">
                          <p className="p-md mb-2">
                            {data?.data?.user?.last_name}
                          </p>
                        </div>
                        <div className="col-sm-6">
                          <p className="ubuntu bold p-md mb-2">Branch:</p>
                        </div>
                        <div className="col-sm-6">
                          <p className="p-md mb-2">
                            {data?.data?.user?.branch?.title}
                          </p>
                        </div>
                        <div className="col-sm-6">
                          <p className="ubuntu bold p-md mb-2">Shift:</p>
                        </div>
                        <div className="col-sm-6">
                          <p className="p-md mb-2">
                            {data?.data?.user?.shift?.title} |{" "}
                            {data?.data?.user?.shift?.from} -{" "}
                            {data?.data?.user?.shift?.to}
                          </p>
                        </div>
                        <div className="col-sm-6">
                          <p className="ubuntu bold p-md mb-2">
                            Application Status:
                          </p>
                        </div>
                        <div className="col-sm-6">
                          <p
                            className={`p-md mb-2 ${
                              data?.data?.user.status === "Pending" &&
                              "orange-text"
                            }`}
                          >
                            {data?.data?.user?.status}
                          </p>
                          {data?.data?.user?.status === "Pending" && (
                            <a
                              href="#"
                              class="ml-4"
                              data-target=".underReview"
                              data-toggle="modal"
                            >
                              Mark as Under Review
                            </a>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                  {data?.data?.user?.status === "Under Review" && (
                    <div className="d-sm-flex align-items-center">
                      <button
                        className="site-btn px-4 py-2 grey-btn mr-sm-4"
                        data-toggle="modal"
                        data-target=".hireCandidate"
                      >
                        Hire Candidate
                      </button>
                      <button
                        className="site-btn px-4 py-2 red-btn"
                        data-toggle="modal"
                        data-target=".rejectCandidate"
                      >
                        Reject Candidate
                      </button>
                      {/* <Button
                        className="site-btn grey-btn px-3 py-2 mt-3 mr-sm-3"
                        loading={isLoading || loadingAccept || loadingReject}
                        onClick={() => {
                          Confirmation(
                            "Are You Sure You Want To Hire This Candidate?",
                            "Yes",
                            () => acceptMutate(data?.data?.user?._id)
                          );
                        }}
                      >
                        Hire Candidate
                      </Button>
                      <Button
                        className="site-btn red-btn px-3 py-2 mt-3 mr-sm-3"
                        loading={isLoading || loadingAccept || loadingReject}
                        onClick={() => {
                          Confirmation(
                            "Are You Sure You Want To Reject This Candidate?",
                            "Yes",
                            () => rejectMutate(data?.data?.user?._id)
                          );
                        }}
                      >
                        Reject Candidate
                      </Button> */}
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <ConfirmationDialog
        target="underReview"
        isClose={true}
        loading={
          isLoading || loadingAccept || loadingReject || loadingUnderReview
        }
        onSuccess={() => underReviewMutate(data?.data?.user?._id)}
      >
        Are you sure you want to mark the application status as Under Review?
      </ConfirmationDialog>
      <SuccessDialog target="custom">
        Application marked as Under Review Successfully
      </SuccessDialog>

      <ConfirmationDialog
        target="hireCandidate"
        isClose={true}
        loading={
          isLoading || loadingAccept || loadingReject || loadingUnderReview
        }
        onSuccess={() => acceptMutate(data?.data?.user?._id)}
      >
        Are you sure you want to hire the candidate?
      </ConfirmationDialog>
      <SuccessDialog target="candidateHired">Candidate Hired</SuccessDialog>

      <ConfirmationDialog
        target="rejectCandidate"
        isClose={true}
        onSuccessTarget="rejectReason"
      >
        Are you sure you want to reject the candidate?
      </ConfirmationDialog>
      <Dialog target="rejectReason">
        <div className="text-left">
          <h4 className="text-center medium">Candidate Rejection Reason</h4>
          <label htmlFor className="ubuntu p-lg mt-2">
            Reason:
          </label>
          <textarea
            name=""
            id=""
            cols="30"
            rows="6"
            placeholder="Enter Reason"
            class="site-input w-100 p-3"
            onChange={(e) => setRejectionReason(e.target.value)}
          ></textarea>
          <Button
            className="site-btn mt-4 grey-btn w-100 py-2"
            loading={isLoading || loadingAccept || loadingReject}
            onClick={() => rejectMutate(data?.data?.user?._id, rejectionReason)}
          >
            Reject Candidate
          </Button>
        </div>
      </Dialog>
      <SuccessDialog target="candidateRejected">
        Candidate Rejected
      </SuccessDialog>
    </div>
  );
}
