import React, { useEffect, useState } from "react";
import debounce from "debounce-promise";
import AsyncSelect from "react-select/async";
import ImageSelector from "../../Components/Elements/Form/ImageSelector";
import Input from "../../Components/Elements/Form/Input";
import InputPassword from "../../Components/Elements/Form/InputPassword";

import { getMinBranches, getShiftDropdown } from "../../Apis";
import { Link } from "react-router-dom";
import { useMutation, useQuery } from "react-query";
import { registerCandidate } from "../../Apis";
import Success from "../../Components/Elements/Modals/Modal.Success";
import Error from "../../Components/Elements/Modals/Modal.Error";
import Loading from "../../Components/Elements/Icons/Loading";
import Button from "../../Components/Elements/Form/Button";

export default function AddCandidate({ history }) {
  const [data, setData] = useState({
    first_name: "",
    last_name: "",
    password: "",
    confirm_password: "",
    email: "",
    branch: "",
    shift: "",
    personnel_type: "Employee",
    resume: "",
  });
  const [branches, setBranches] = useState([]);

  const { data: all_shift, isLoading: shiftLoading } = useQuery(
    ["shifts_dropdown"],
    () => getShiftDropdown()
  );

  const { mutate, isLoading } = useMutation((data) => registerCandidate(data), {
    onSuccess: (res) => {
      Success(res?.data?.message);
      history.push("/candidate/logs");
    },
    onError: (err) => Error(err?.response?.data?.message),
  });

  useEffect(() => {
    handleGetBranches();
  }, []);

  const handleRegsiterCandidate = () => {
    const {
      first_name,
      last_name,
      password,
      confirm_password,
      email,
      branch,
      shift,
      personnel_type,
      resume,
    } = data;
    const form_data = new FormData();
    form_data.append("first_name", first_name);
    form_data.append("last_name", last_name);
    form_data.append("password", password);
    form_data.append("confirm_password", confirm_password);
    form_data.append("email", email);
    form_data.append("branch", branch?.value);
    form_data.append("shift", shift);
    form_data.append("personnel_type", personnel_type);
    form_data.append("resume", resume);
    mutate(form_data);
  };

  const handleGetBranches = async (inputValue, callback) => {
    const { data } = await getMinBranches(inputValue, true);
    const formatted = [];
    data?.branches?.forEach((branch) => {
      formatted.push({
        label: branch?.title,
        value: branch?._id,
      });
    });

    if (callback) await callback(formatted);
    else setBranches(formatted);
  };

  const debounceLoadBranches = (inputValue, callBack) =>
    debounce(handleGetBranches(inputValue, callBack), 500, {
      leading: true,
    });

  return (
    <div className="main-body">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 px-0 px-lg-2">
            <div className="white-card mt-3 px-2 py-3 px-sm-3">
              <div className="d-sm-flex align-items-center justify-content-between">
                <h4 className="mb-0">Add Candidate</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="profile-card mt-3 py-lg-5 p-sm-4 p-3">
                    <form action="a-potential-candidates.php">
                      <div className="row">
                        <div className="col-lg-6 mt-3">
                          <label htmlFor className="medium ubuntu p-lg">
                            First Name:
                          </label>
                          <Input
                            type="text"
                            placeholder="Enter First Name"
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.first_name}
                            onChange={(first_name) =>
                              setData({ ...data, first_name })
                            }
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Last Name:
                          </label>
                          <Input
                            type="text"
                            placeholder="Enter Last Name"
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.last_name}
                            onChange={(last_name) =>
                              setData({ ...data, last_name })
                            }
                          />
                          <label htmlFor className="medium ubuntu mt-3 p-lg">
                            Email Address:
                          </label>
                          <Input
                            type="email"
                            placeholder="Enter Email Address"
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.email}
                            onChange={(email) => setData({ ...data, email })}
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Shift:
                          </label>
                          {shiftLoading ? (
                            <Loading />
                          ) : (
                            <select
                              className="site-input w-100 grey-input px-3 py-2"
                              value={data?.shift}
                              onChange={(e) =>
                                setData({ ...data, shift: e.target.value })
                              }
                            >
                              <option value>Select Shift</option>
                              {all_shift?.data?.shifts?.map((shift) => (
                                <option value={shift?._id}>
                                  {shift?.title} || {shift.from} - {shift.to}
                                </option>
                              ))}
                            </select>
                          )}
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Upload Resume:
                          </label>
                          <ImageSelector
                            is_edit={true}
                            value={data?.resume}
                            accept={"*"}
                            onChange={(resume) => setData({ ...data, resume })}
                          />
                          <p>* Accepted Formats doc, docx, & pdf</p>
                        </div>
                        <div className="col-lg-6 mt-3">
                          <label htmlFor className="medium ubuntu p-lg">
                            Password:
                          </label>
                          <InputPassword
                            type="password"
                            placeholder="Enter Password"
                            className="site-input login-input px-3 py-2 w-100"
                            value={data?.password}
                            onChange={(password) =>
                              setData({ ...data, password })
                            }
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Confirm Password:
                          </label>
                          <InputPassword
                            type="password"
                            placeholder="Enter Password"
                            className="site-input login-input px-3 py-2 w-100"
                            value={data?.confirm_password}
                            onChange={(confirm_password) =>
                              setData({ ...data, confirm_password })
                            }
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Branch:
                          </label>
                          <AsyncSelect
                            loadOptions={(inputValue, callBack) =>
                              debounceLoadBranches(inputValue, callBack)
                            }
                            onChange={(opt) => {
                              setData({ ...data, branch: opt });
                            }}
                            defaultOptions={branches}
                            value={data?.branch}
                            styles={{
                              control: (styles) => ({
                                ...styles,
                                cursor: "pointer",
                              }),
                            }}
                            placeholder=""
                          />
                        </div>
                      </div>
                      <div className="text-center mt-lg-3">
                        <Button
                          loading={isLoading}
                          onClick={handleRegsiterCandidate}
                          className="site-btn grey-btn mt-3 px-3 py-2"
                        >
                          Add Candidate
                        </Button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
