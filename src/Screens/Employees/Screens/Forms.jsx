import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { image_url } from "../../../Utils/connection_strings";
import { getEmployeeForms } from "../../../Apis/Employee";
import LoadingView from "../../../Components/LoadingView";
import Error from "../../../Components/Elements/Modals/Modal.Error";

export default function Forms({ history }) {
  const { id } = useParams();
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState(null);

  useEffect(() => {
    handleGetForms();
  }, []);

  const handleGetForms = async () => {
    try {
      const { data } = await getEmployeeForms(id);
      // console.log("GET FORMS", data);
      setData(data?.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      Error(err?.response?.data?.message);
    }
  };

  if (loading) return <LoadingView />;

  return (
    <div className="main">
      <div className="main-body">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 px-0 px-lg-2">
              <div className="white-card mt-3 px-2 py-3 px-sm-3">
                <div className="d-md-flex align-items-center justify-content-between">
                  <h4 className="mb-0">Important Forms</h4>
                  <div className="d-sm-flex align-items-center">
                    {data?.id_file?.status === "Submitted" && (
                      <p className="green-text mb-0 mt-md-0 mt-2">
                        <i className="fas fa-check" /> Uploaded
                      </p>
                    )}
                    <button
                      title={data?.id_file?.status === "Pending" && "Not Submitted Yet"}
                      disabled={!data?.id_file?.submitted_file_path}
                      className="site-btn yellow-border px-5 py-2 ml-sm-3"
                      onClick={() => {
                        const url = `${image_url}${data?.id_file?.submitted_file_path}`;
                        window.open(url);
                      }}
                    >
                      View ID
                    </button>
                  </div>
                </div>
                {data?.forms?.map((el, index) => (
                  <div key={index} className="orange-bg py-2 px-3 mt-3 d-md-flex align-items-center justify-content-between">
                    <p className="p-lg ubuntu mb-0">Form Name: {el?.form_name}</p>
                    <div className="d-sm-flex align-items-center">
                      {el?.status === "Submitted" && (
                        <p className="green-text mb-0 mt-md-0 mt-2">
                          <i className="fas fa-check" /> Submitted
                        </p>
                      )}
                      <button
                        title={el?.status === "Pending" && "Not Submitted Yet"}
                        disabled={!el?.submitted_file_path}
                        className="site-btn grey-btn px-4 py-2 ml-sm-3"
                        onClick={() => {
                          const url = `${image_url}${el?.submitted_file_path}`;
                          window.open(url);
                        }}
                      >
                        View Form
                      </button>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
