import React from "react";
import { useQuery } from "react-query";

import { getCertificate } from "../../../Apis";
import LoadingView from "../../../Components/LoadingView";
import Certificate from "../Components/Certificate";

export default function CertificateDetails({ match }) {
  const employeeId = match.params.employeeId;
  const id = match.params.id;
  const { data, isLoading } = useQuery(
    ["certificate_details", id, employeeId],
    () => getCertificate(id, employeeId)
  );

  if (isLoading) return <LoadingView />;

  return (
    <div>
      <div className="main">
        <div className="main-body">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12 px-0 px-lg-2">
                <div className="white-card mt-3 px-2 py-3 px-sm-3">
                  <div className="d-md-flex align-items-center justify-content-between">
                    <h4 className="mb-0">Employee's Individual Certificate</h4>
                    <button className="site-btn grey-btn px-3 py-2 mt-md-0 mt-3">
                      Download PDF
                    </button>
                  </div>
                  <div className="row">
                    <div className="col-xl-4 col-lg-6 col-md-8 col-12">
                      <div className="row">
                        <div className="col-6 mt-3">
                          <p className="p-lg mb-0 medium ubuntu">Month:</p>
                        </div>
                        <div className="col-6 mt-3">
                          <p className="p-md mb-0">
                            {data?.data?.certificate?.month}
                          </p>
                        </div>
                        <div className="col-6 mt-1">
                          <p className="p-lg mb-0 medium ubuntu">Awarded By:</p>
                        </div>
                        <div className="col-6 mt-1">
                          <p className="p-md mb-0">
                            {data?.data?.certificate?.awarded_by}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-xl-9 col-lg-10 mx-auto">
                      <Certificate
                        employeeName={data?.data?.employee_name}
                        date={data?.data?.certificate?.createdAt}
                      />
                      {/* <img
                        src="images/certificate.png"
                        alt=""
                        className="img-fluid mt-3 w-100"
                      /> */}
                    </div>
                  </div>
                  <div className="d-sm-flex pb-lg-5 pb-3 align-items-center justify-content-center">
                    <button
                      className="site-btn grey-btn px-3 py-2"
                      data-toggle="modal"
                      data-target=".sendCertificate"
                    >
                      Send Certificate
                    </button>
                    <button className="site-btn yellow-border px-3 mt-sm-0 mt-3 py-2 ml-sm-3">
                      Download PDF
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade sendCertificate"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content site-modal">
            <i
              className="close modal-close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={21}
                height={21}
                viewBox="0 0 21 21"
              >
                <path
                  id="Icon_material-close"
                  data-name="Icon material-close"
                  d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
                  transform="translate(-7.5 -7.5)"
                  fill="#545454"
                />
              </svg>
            </i>
            <div className="text-center">
              <img src="images/question.png" alt="" className="img-fluid" />
              <p className="d-grey-text mt-3 p-lg">
                Are you sure you want to send the certificate?
              </p>
              <div className="d-sm-flex align-items-center justify-content-center">
                <button
                  className="site-btn grey-border mr-sm-3 py-2 px-5 mt-1"
                  data-dismiss="modal"
                  aria-label="Close"
                  data-toggle="modal"
                  data-target=".certificateGiven"
                >
                  Yes
                </button>
                <button
                  className="site-btn grey-btn py-2 px-5 mt-1"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  No
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade certificateGiven"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content site-modal">
            <i
              className="close modal-close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={21}
                height={21}
                viewBox="0 0 21 21"
              >
                <path
                  id="Icon_material-close"
                  data-name="Icon material-close"
                  d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
                  transform="translate(-7.5 -7.5)"
                  fill="#545454"
                />
              </svg>
            </i>
            <div className="text-center">
              <img src="images/check.png" alt="" className="img-fluid" />
              <p className="d-grey-text mt-3 p-lg">Certificate Given</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
