import React from "react";
import AttendanceReport from "../../../Components/AttendanceReport";

export default function EmployeeAttendanceReport(props) {
  return (
    <>
      <AttendanceReport {...props} />
    </>
  );
}
