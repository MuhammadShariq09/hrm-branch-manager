import React from "react";

export default function ViewId() {
  return (
    <div className="main">
      <div className="main-body">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 px-0 px-lg-2">
              <div className="white-card mt-3 px-2 py-3 px-sm-3">
                <h4 className="mb-0 d-flex align-items-center">
                  <a href="m-forms.php">
                    <i className="fas mr-2 p-lg fa-arrow-left" />
                  </a>{" "}
                  View ID
                </h4>
                <div className="row">
                  <div className="col-xl-9 col-lg-10 col-12 mx-auto">
                    <div className="orange-bg my-5 text-center p-lg-5 p-sm-4 p-3">
                      <img
                        src="images/id-img.png"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
