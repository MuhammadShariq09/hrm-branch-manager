import React, { useState, useEffect } from "react";
import { useQuery } from "react-query";
import { image_url } from "../../../Utils/connection_strings";
import { queryClient } from "../../..";
import { getMessages } from "../../../Apis";
import { format_date_time } from "../../../Utils/helpers";
import { getEmployeeDetails } from "../../../Apis";
import LoadingView from "../../../Components/LoadingView";

export default function ViewEmployeeMessages({ match }) {
  const id = match.params.id;
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(20);

  const { data: employeeDetails, isLoading: loadingEmployeeDetails } = useQuery(["employee_details", id], () =>
    getEmployeeDetails(id, false)
  );

  const { isFetching, isLoading, data, refetch } = useQuery(
    ["employee_messages", page, perPage, employeeDetails],
    () => getMessages(page, perPage, employeeDetails?.data?.employee?.messages_rooms?.branch_manger_room_id),
    { keepPreviousData: true }
  );

  // Prefetch the next page!
  useEffect(() => {
    if (data?.data?.logs?.hasNextPage) {
      queryClient.prefetchQuery(["employee_messages", page + 1, perPage, employeeDetails], () =>
        getMessages(page + 1, perPage, employeeDetails?.data?.employee?.messages_rooms?.branch_manger_room_id)
      );
    }
  }, [data, page, queryClient, employeeDetails]);

  if (loadingEmployeeDetails || isLoading) return <LoadingView />;

  return (
    <div>
      <div className="main">
        <div className="main-body">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12 px-0 px-lg-2">
                <div className="white-card mt-3 px-2 pb-5 py-3 px-sm-3">
                  <h4>
                    Employee Messages -{" "}
                    {`${employeeDetails?.data?.employee?.first_name} ${employeeDetails?.data?.employee?.last_name}`}
                  </h4>
                  {data?.data?.messages?.docs.map((el, i) => (
                    <div key={i} className="media mt-3 d-sm-flex d-block">
                      <div className="message-img">
                        <img
                          src={
                            el?.branch_manager_from?.user_image
                              ? `${image_url}${el?.branch_manager_from?.user_image}`
                              : "images/message-img.png"
                          }
                          alt=""
                          className="img-fluid"
                        />
                        <span className="online" />
                      </div>
                      <div className="media-body ml-sm-3">
                        <div className="d-flex align-items-center">
                          <div>
                            <h5>{el?.branch_manager_from?.first_name}</h5>
                            <div className="message-card p-lg-4 p-2">
                              <p className="mb-0">{el?.message}</p>
                            </div>
                          </div>
                          <button
                            className="site-input ml-sm-5 mt-4 px-2 pb-1 pt-2"
                            data-target=".deleteMessage"
                            data-toggle="modal"
                          >
                            <svg xmlns="http://www.w3.org/2000/svg" width="23.003" height="29.569" viewBox="0 0 23.003 29.569">
                              <g id="delete_3_" data-name="delete (3)" transform="translate(0)">
                                <path
                                  id="Path_4852"
                                  data-name="Path 4852"
                                  d="M127.25,1.785h4.942v.832h1.785V1.668A1.67,1.67,0,0,0,132.309,0h-5.176a1.67,1.67,0,0,0-1.668,1.668v.948h1.785Zm0,0"
                                  transform="translate(-118.219 0)"
                                  fill="#f7941e"
                                />
                                <path
                                  id="Path_4853"
                                  data-name="Path 4853"
                                  d="M58.031,167.75H41.277a.786.786,0,0,0-.783.849l1.4,17.32a1.86,1.86,0,0,0,1.853,1.712H55.56a1.86,1.86,0,0,0,1.853-1.712l1.4-17.32a.786.786,0,0,0-.783-.849Zm-12.7,18.034-.056,0a.892.892,0,0,1-.89-.838l-.878-14.219a.892.892,0,0,1,1.781-.11l.878,14.219a.892.892,0,0,1-.836.946Zm5.223-.891a.892.892,0,0,1-1.785,0V170.674a.892.892,0,1,1,1.785,0ZM55.8,170.726l-.838,14.219a.892.892,0,0,1-.89.84h-.053a.892.892,0,0,1-.838-.943l.838-14.219a.892.892,0,0,1,1.782.105Zm0,0"
                                  transform="translate(-38.152 -158.062)"
                                  fill="#f7941e"
                                />
                                <path
                                  id="Path_4854"
                                  data-name="Path 4854"
                                  d="M22.965,78.744l-.586-1.757a1.135,1.135,0,0,0-1.077-.776H1.7a1.135,1.135,0,0,0-1.076.776L.039,78.744a.737.737,0,0,0,.7.969h21.53a.727.727,0,0,0,.39-.113.735.735,0,0,0,.309-.857Zm0,0"
                                  transform="translate(0 -71.81)"
                                  fill="#f7941e"
                                />
                              </g>
                            </svg>
                          </button>
                        </div>
                        <p className="p-sm mb-0 mt-2 d-grey-text">{format_date_time(el?.createdAt)}</p>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade deleteMessage"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content site-modal">
            <i className="close modal-close" data-dismiss="modal" aria-label="Close">
              <svg xmlns="http://www.w3.org/2000/svg" width={21} height={21} viewBox="0 0 21 21">
                <path
                  id="Icon_material-close"
                  data-name="Icon material-close"
                  d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
                  transform="translate(-7.5 -7.5)"
                  fill="#545454"
                />
              </svg>
            </i>
            <div className="text-center">
              <img src="images/question.png" alt="" className="img-fluid" />
              <p className="d-grey-text mt-3 p-lg">Are you sure you want to delete this message?</p>
              <div className="d-sm-flex align-items-center justify-content-center">
                <button className="site-btn grey-border mr-sm-3 py-2 px-5 mt-1" data-dismiss="modal" aria-label="Close">
                  No
                </button>
                <button
                  className="site-btn grey-btn py-2 px-5 mt-1"
                  data-dismiss="modal"
                  aria-label="Close"
                  data-toggle="modal"
                  data-target=".messageDeleted"
                >
                  Yes
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade messageDeleted"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content site-modal">
            <i className="close modal-close" data-dismiss="modal" aria-label="Close">
              <svg xmlns="http://www.w3.org/2000/svg" width={21} height={21} viewBox="0 0 21 21">
                <path
                  id="Icon_material-close"
                  data-name="Icon material-close"
                  d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
                  transform="translate(-7.5 -7.5)"
                  fill="#545454"
                />
              </svg>
            </i>
            <div className="text-center">
              <img src="images/check.png" alt="" className="img-fluid" />
              <p className="d-grey-text mt-3 p-lg">Message Deleted Successfully!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
