import React from "react";
import MonthlyReport from "../../../Components/MonthlyReport";

export default function EmployeeMonthlyReport(props) {
  return <MonthlyReport {...props} />;
}
