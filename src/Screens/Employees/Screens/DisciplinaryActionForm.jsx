import moment from "moment";
import React, { useState } from "react";
import { useQuery } from "react-query";

import { getDisciplinaryActionForm } from "../../../Apis";
import LoadingView from "../../../Components/LoadingView";

export default function DisciplinaryActionForm({ match }) {
  const id = match.params.id;
  const employeeId = match.params.employeeId;
  const { data, isLoading } = useQuery(["disciplinary_action_form", id], () =>
    getDisciplinaryActionForm(id, employeeId)
  );

  if (isLoading) return <LoadingView />;

  return (
    <div>
      <div className="main">
        <div className="main-body">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12 px-0 px-lg-2">
                <div className="white-card mt-3 px-2 py-3 px-sm-3">
                  <h4 className="mb-0">Employee's Disciplinary Action Form</h4>
                  <div className="orange-bg mt-4 p-sm-4 pb-lg-5 p-3">
                    <div className="d-flex align-items-center">
                      <h5 className="mb-0">Date Of Warning:</h5>
                      <p className="p-lg orange-text bold mb-0 ml-4">
                        {moment(
                          data?.data?.disciplinaryAction?.violationDate
                        ).format("MMMM DD, YYYY")}
                      </p>
                    </div>
                    <div className="row">
                      <div className="col-xl-6 col-lg-8">
                        <div className="row align-items-center">
                          <div className="col-md-4 mt-2">
                            <p className="p-lg mb-0 ubuntu light">
                              Employee Name:
                            </p>
                          </div>
                          <div className="col-md-8 mt-2">
                            <p className="mb-0 p-md">
                              {`${data?.data?.disciplinaryAction?.employee?.first_name} ${data?.data?.disciplinaryAction?.employee?.last_name}`}
                            </p>
                          </div>
                        </div>
                        <h5 className="mt-4">Type of Violation</h5>
                        <div className="row align-items-center">
                          <div className="col-md-4 mt-2">
                            <p className="p-lg mb-0 ubuntu light">
                              Option Selected:
                            </p>
                          </div>
                          <div className="col-md-8 mt-2">
                            <p className="mb-0 p-md">
                              {data?.data?.disciplinaryAction?.violationType}
                            </p>
                          </div>
                        </div>
                        <h5 className="mt-4">Warning Details</h5>
                        <div className="row align-items-center">
                          <div className="col-md-4 mt-2">
                            <p className="p-lg mb-0 ubuntu light">
                              Violation Date:
                            </p>
                          </div>
                          <div className="col-md-8 mt-2">
                            <p className="mb-0 p-md">
                              {moment(
                                data?.data?.disciplinaryAction?.violationDate
                              ).format("MMMM DD, YYYY")}
                            </p>
                          </div>
                          <div className="col-md-4 mt-2">
                            <p className="p-lg mb-0 ubuntu light">
                              Violation Time:
                            </p>
                          </div>
                          <div className="col-md-8 mt-2">
                            <p className="mb-0 p-md">
                              {moment(
                                data?.data?.disciplinaryAction?.violationTime,
                                "hh:mm"
                              ).format("hh:mm A")}
                            </p>
                          </div>
                          <div className="col-md-4 mt-2">
                            <p className="p-lg mb-0 ubuntu light">
                              Violation Details:
                            </p>
                          </div>
                          <div className="col-md-8 mt-2">
                            <p className="mb-0 p-md">
                              {data?.data?.disciplinaryAction?.violationNotes}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade disciplinarySubmitted"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content site-modal">
            <i
              className="close modal-close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={21}
                height={21}
                viewBox="0 0 21 21"
              >
                <path
                  id="Icon_material-close"
                  data-name="Icon material-close"
                  d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
                  transform="translate(-7.5 -7.5)"
                  fill="#545454"
                />
              </svg>
            </i>
            <div className="text-center">
              <img src="images/check.png" alt="" className="img-fluid" />
              <p className="d-grey-text mt-3 p-lg">
                Form Submitted Successfully
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
