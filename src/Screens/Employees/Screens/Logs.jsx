import React from "react";
import EmployeeLogs from "../Components/EmployeeLogs";

export default function Logs(props) {
  return (
    <div className="main">
      <div className="main-body">
        <EmployeeLogs origin="Employee" {...props} />
      </div>
    </div>
  );
}
