import React from "react";

export default function EditLeaves() {
  return (
    <div>
      <div className="main">
        {/*?php include('m-mobile-navigation-login.php') ?*/}
        <div className="main-body">
          <nav className="navbar navbar-expand-lg navbar-light navbar-lg  bg-me1">
            <a href="#" className="sidebar-toggle">
              <i className="fas fa-bars" />
            </a>
            <a href="m-index-login.php" className="p-0 mt-0">
              <img
                src="images/logo-black.png"
                alt=""
                className="img-fluid d-lg-none"
              />
            </a>
            <button
              className="navbar-toggler border-0 sidebar-toggle"
              type="button"
              data-toggle="collapse"
              data-target="#navbarNav1"
              aria-controls="navbarNav1"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <i className="fas fa-ellipsis-v" />
            </button>
            <div className="collapse navbar-collapse" id="navbarNav1">
              <ul className="navbar-nav align-items-lg-center ml-auto">
                <li className="nav-item active">
                  <a
                    className="nav-link site-btn grey-border mt-3 mt-lg-0 d-inline-block px-3 py-2"
                    href="m-index.php"
                  >
                    Back to Web View
                  </a>
                </li>
                <li className="dropdown dropdown-notification nav-item two-bell-icons">
                  <a
                    className="nav-link nav-link-label"
                    href="#"
                    data-toggle="dropdown"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="34.667"
                      height="40.909"
                      viewBox="0 0 34.667 40.909"
                    >
                      <g
                        id="Group_218"
                        data-name="Group 218"
                        transform="translate(-1556.583 -56)"
                      >
                        <g
                          id="notification"
                          transform="translate(1556.584 59.091)"
                        >
                          <path
                            id="Path_3517"
                            data-name="Path 3517"
                            d="M140.754,452.727A5.918,5.918,0,0,0,146.544,448H134.965a5.919,5.919,0,0,0,5.789,4.727Zm0,0"
                            transform="translate(-124.997 -414.909)"
                            fill="#29313f"
                          />
                          <path
                            id="Path_3518"
                            data-name="Path 3518"
                            d="M26.806,58.423h-.019a11.043,11.043,0,0,1-11.03-11.03,10.932,10.932,0,0,1,1.054-4.674c-.347-.033-.7-.053-1.054-.053A11.03,11.03,0,0,0,4.727,53.7v4.393A10.566,10.566,0,0,1,.964,66.179,2.757,2.757,0,0,0,.1,69.009a2.891,2.891,0,0,0,2.833,2.022H28.576a2.889,2.889,0,0,0,2.88-2.186,2.762,2.762,0,0,0-.941-2.7,10.485,10.485,0,0,1-3.709-7.724Zm0,0"
                            transform="translate(0 -39.516)"
                            fill="#29313f"
                          />
                          <path
                            id="Path_3519"
                            data-name="Path 3519"
                            d="M271.769,7.879A7.879,7.879,0,1,1,263.89,0,7.878,7.878,0,0,1,271.769,7.879Zm0,0"
                            transform="translate(-237.103)"
                            fill="#29313f"
                          />
                        </g>
                        <text
                          id="_2"
                          data-name={2}
                          transform="translate(1579 72)"
                          fill="#fff"
                          fontSize={16}
                          fontFamily="Nunito-SemiBold, Nunito"
                          fontWeight={600}
                        >
                          <tspan x={0} y={0}>
                            2
                          </tspan>
                        </text>
                      </g>
                    </svg>
                  </a>
                  <ul className="dropdown-menu dropdown-menu-media dropdown-menu-right">
                    <li className="dropdown-menu-header d-flex align-items-center justify-content-between">
                      <h6 className="p-md bold m-0">
                        <span className="grey darken-2">Notifications</span>
                        {/* <span class="notification-tag badge badge-default badge-danger float-right m-0">5 New</span>  */}
                      </h6>
                      <a href="notifications.php" className="pur-text">
                        View All
                      </a>
                    </li>
                    <li
                      className="scrollable-container media-list ps-container ps-theme-dark ps-active-y"
                      data-ps-id="75e644f2-605d-3ecc-f100-72d86e4a64d8"
                    >
                      <hr />
                      <a href="notifications.php">
                        <div className="media">
                          <div className="media-left mr-2">
                            <i className="fas fa-bell pur-text notifications-bell" />
                          </div>
                          <div className="media-body">
                            {/* <h6 class="media-heading">You have new notification!</h6> */}
                            <p className="notification-text mb-0 p-sm font-small-3 text-muted">
                              Lorem ipsum dolor sit amet, consetetur sadipscing
                              elitr, sed diam nonumy eirmod tempor
                            </p>
                            <div className="text-right">
                              <small>
                                <time
                                  className="media-meta text-muted"
                                  dateTime="2015-06-11T18:29:20+08:00"
                                >
                                  5 min ago
                                </time>
                              </small>
                            </div>
                          </div>
                        </div>
                      </a>
                      <hr />
                      <a href="notifications.php">
                        <div className="media">
                          <div className="media-left mr-2">
                            <i className="fas fa-bell pur-text notifications-bell" />
                          </div>
                          <div className="media-body">
                            {/* <h6 class="media-heading">You have new notification!</h6> */}
                            <p className="notification-text mb-0 p-sm font-small-3 text-muted">
                              Lorem ipsum dolor sit amet, consetetur sadipscing
                              elitr, sed diam nonumy eirmod tempor
                            </p>
                            <div className="text-right">
                              <small>
                                <time
                                  className="media-meta text-muted"
                                  dateTime="2015-06-11T18:29:20+08:00"
                                >
                                  5 min ago
                                </time>
                              </small>
                            </div>
                          </div>
                        </div>
                      </a>
                      <hr />
                      <a href="notifications.php">
                        <div className="media">
                          <div className="media-left mr-2">
                            <i className="fas fa-bell pur-text notifications-bell" />
                          </div>
                          <div className="media-body">
                            {/* <h6 class="media-heading">You have new notification!</h6> */}
                            <p className="notification-text mb-0 p-sm font-small-3 text-muted">
                              Lorem ipsum dolor sit amet, consetetur sadipscing
                              elitr, sed diam nonumy eirmod tempor
                            </p>
                            <div className="text-right">
                              <small>
                                <time
                                  className="media-meta text-muted"
                                  dateTime="2015-06-11T18:29:20+08:00"
                                >
                                  5 min ago
                                </time>
                              </small>
                            </div>
                          </div>
                        </div>
                      </a>
                    </li>
                  </ul>
                </li>
                <li className="nav-item active">
                  <a
                    className="nav-link mb-3 mb-lg-0 mt-lg-0 d-inline-block"
                    href="m-profile.php"
                  >
                    <div className="media align-items-center">
                      <img
                        src="images/avatar.png"
                        alt=""
                        className="img-fluid"
                      />
                      <div className="media-body ml-2">
                        <p className="p-md mb-0 ubuntu bold">George Wilson</p>
                        <p className="mb-0">Branch Manager</p>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </div>
          </nav>
          <div className="container-fluid">
            <div className="row">
              <div className="col-12 px-0 px-lg-2">
                <div className="white-card mt-3 px-2 pb-5 py-3 px-sm-3">
                  <h4>Edit Leaves</h4>
                  <div className="orange-bg mt-4 py-xl-5">
                    <div className="row">
                      <div className="col-xl-7 mx-auto py-5 col-lg-8">
                        <form action="#_" className="my-lg-5">
                          <div className="password-card mt-2 py-4 px-3">
                            <label htmlFor className="mt-0 ubuntu p-lg">
                              Annual Leaves
                            </label>
                            <input
                              type="number"
                              defaultValue={20}
                              className="site-input transparent-bg px-3 py-2 w-100"
                            />
                            <label htmlFor className="mt-3 ubuntu p-lg">
                              Casual Leaves
                            </label>
                            <input
                              type="number"
                              defaultValue={12}
                              className="site-input transparent-bg px-3 py-2 w-100"
                            />
                            <label htmlFor className="mt-3 ubuntu p-lg">
                              Medical Leaves
                            </label>
                            <input
                              type="number"
                              defaultValue={10}
                              className="site-input transparent-bg px-3 py-2 w-100"
                            />
                            <label htmlFor className="mt-3 ubuntu p-lg">
                              Total: 42
                            </label>
                            <div className="d-sm-flex align-items-center justify-content-center">
                              <button
                                className="site-btn grey-btn px-3 mr-sm-3 py-2 mt-3"
                                type="button"
                                data-target=".editLeave"
                                data-toggle="modal"
                              >
                                Save Changes
                              </button>
                              <button
                                className="site-btn yellow-border px-5 py-2 mt-3"
                                type="button"
                              >
                                Cancel
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade editLeave"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content site-modal">
            <i
              className="close modal-close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={21}
                height={21}
                viewBox="0 0 21 21"
              >
                <path
                  id="Icon_material-close"
                  data-name="Icon material-close"
                  d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
                  transform="translate(-7.5 -7.5)"
                  fill="#545454"
                />
              </svg>
            </i>
            <div className="text-center">
              <img src="images/question.png" alt="" className="img-fluid" />
              <p className="d-grey-text mt-3 p-lg">
                Are you sure you want to edit the leave count?
              </p>
              <div className="d-sm-flex align-items-center justify-content-center">
                <button
                  className="site-btn grey-border mr-sm-3 py-2 px-5 mt-1"
                  data-dismiss="modal"
                  aria-label="Close"
                  data-toggle="modal"
                  data-target=".leaveEdited"
                >
                  Yes
                </button>
                <button
                  className="site-btn grey-btn py-2 px-5 mt-1"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  No
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade leaveEdited"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content site-modal">
            <i
              className="close modal-close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={21}
                height={21}
                viewBox="0 0 21 21"
              >
                <path
                  id="Icon_material-close"
                  data-name="Icon material-close"
                  d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
                  transform="translate(-7.5 -7.5)"
                  fill="#545454"
                />
              </svg>
            </i>
            <div className="text-center">
              <img src="images/check.png" alt="" className="img-fluid" />
              <p className="d-grey-text mt-3 p-lg">Leave Count Edited</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
