import React, { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { Link } from "react-router-dom";
import { queryClient } from "../../..";
import { getCertificateLogs } from "../../../Apis";
import Table from "../../../Components/Table";

export default function CertificateLogs({ match }) {
  const id = match.params.id;
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);

  const { isFetching, isLoading, data } = useQuery(
    ["certificate_logs", page, perPage, id],
    () => getCertificateLogs(page, perPage, id),
    { keepPreviousData: true }
  );

  // Prefetch the next page!
  useEffect(() => {
    if (data?.data?.logs?.hasNextPage) {
      queryClient.prefetchQuery(
        ["certificate_logs", page + 1, perPage, id],
        () => getCertificateLogs(page + 1, perPage, id)
      );
    }
  }, [data, page, queryClient, id]);

  console.log("debug", data);

  return (
    <div className="main-body">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 px-0 px-lg-2">
            <div className="white-card mt-3 px-2 py-3 px-sm-3">
              <div className="d-sm-flex align-items-center justify-content-between">
                <h5 className="mt-0 mb-0">
                  Employee's Certificates of Recognition
                </h5>
                <Link
                  className="site-btn grey-btn px-3 py-2 mt-2 mt-sm-0"
                  to={`/employee/certificate/new/${id}`}
                >
                  Give New Certificate
                </Link>
              </div>
            </div>
            <Table
              headings={["Year", "Month", "Awarded By", "Actions"]}
              perPage={perPage}
              setPerPage={setPerPage}
              totalPages={data?.data?.logs?.totalPages}
              setPage={setPage}
              data={data?.data?.logs?.docs}
              isFetching={isFetching}
              isLoading={isLoading}
            >
              <tbody className="orange-bg p-3">
                {data?.data?.logs?.docs?.map((log) => (
                  <tr>
                    <td>{log?.year}</td>
                    <td>{log?.month}</td>
                    <td>{log?.awarded_by}</td>
                    <td>
                      <Link
                        to={`/employee/${log?.employee}/certificate/view/${log?._id}`}
                        className="orange-text"
                      >
                        View
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </div>
  );
}
