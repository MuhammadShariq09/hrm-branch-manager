import React, { useEffect, useState } from "react";
import debounce from "debounce-promise";
import AsyncSelect from "react-select/async";
import { useMutation, useQuery } from "react-query";
import { Link } from "react-router-dom";
import { changeRoute } from "../../../Utils/helpers";
import {
  getMinBranches,
  getShiftDropdown,
  registerEmployee,
} from "../../../Apis";
import Calender from "../../../Components/Elements/Form/Calender";
import Input from "../../../Components/Elements/Form/Input";
import InputPassword from "../../../Components/Elements/Form/InputPassword";
import Loading from "../../../Components/Elements/Icons/Loading";
import InputNumber from "../../../Components/Elements/Form/InputNumber";
import Button from "../../../Components/Elements/Form/Button";
import Error from "../../../Components/Elements/Modals/Modal.Error";
import SuccessDialog from "../../../Components/Elements/Modals/Dialog.Success";
import ImageSelector from "../../../Components/ImageSelector";

export default function AddEmployee({ history, match }) {
  const [data, setData] = useState({
    first_name: "",
    last_name: "",
    date_of_birth: "",
    employement_status: "",
    branch: "",
    shift: "",
    salary: "",
    bank_details: {
      bank: "",
      account_title: "",
      account_number: "",
      iban: "",
    },
    email: "",
    password: "",
    confirm_password: "",
  });
  const [branches, setBranches] = useState([]);
  const [profilePhoto, setProfilePhoto] = useState();

  const { data: all_shift, isLoading: shiftLoading } = useQuery(
    ["shifts_dropdown"],
    () => getShiftDropdown()
  );

  const { mutate, isLoading } = useMutation((data) => registerEmployee(data), {
    onSuccess: () => {
      window.$(".employeeAdded").modal("show");
      changeRoute(history, "/employee/logs");
    },
    onError: (err) => Error(err?.response?.data?.message),
  });

  useEffect(() => {
    handleGetBranches();
  }, []);

  const handleGetBranches = async (inputValue, callback) => {
    const { data } = await getMinBranches(inputValue, true);
    const formatted = [];
    data?.branches?.forEach((branch) => {
      formatted.push({
        label: branch?.title,
        value: branch?._id,
      });
    });

    if (callback) await callback(formatted);
    else setBranches(formatted);
  };

  const debounceLoadBranches = (inputValue, callBack) =>
    debounce(handleGetBranches(inputValue, callBack), 500, {
      leading: true,
    });

  const handleAddEmployee = () => {
    const form_data = new FormData();
    form_data.append("first_name", data?.first_name);
    form_data.append("last_name", data?.last_name);
    form_data.append("user_image", profilePhoto);
    form_data.append("date_of_birth", data?.date_of_birth);
    form_data.append("employement_status", data?.employement_status);
    form_data.append("branch", data?.branch?.value);
    form_data.append("shift", data?.shift);
    form_data.append("salary", data?.salary);
    form_data.append("bank_details", JSON.stringify(data?.bank_details));
    form_data.append("email", data?.email);
    form_data.append("password", data?.password);
    form_data.append("confirm_password", data?.confirm_password);
    mutate(form_data);
  };

  return (
    <div className="main-body">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 px-0 px-lg-2">
            <div className="white-card mt-3 px-2 py-3 px-sm-3">
              <h4 className="mb-0">Add New Employee</h4>
              <div className="row">
                <div className="col-12">
                  <div className="profile-card mt-3 py-lg-5 p-sm-4 p-3">
                    <div className="profile-img mx-auto">
                      <ImageSelector
                        value={profilePhoto}
                        onChange={(image) => setProfilePhoto(image)}
                      />
                    </div>
                    <h5 className="mt-3">Personal Information:</h5>
                    <form action="profile.php">
                      <div className="row">
                        <div className="col-lg-6">
                          <label htmlFor className="medium ubuntu p-lg">
                            First Name:
                          </label>
                          <Input
                            type="text"
                            placeholder="Enter First Name"
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.first_name}
                            onChange={(first_name) =>
                              setData({ ...data, first_name })
                            }
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Last Name:
                          </label>
                          <Input
                            type="text"
                            placeholder="Enter Last Name"
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.last_name}
                            onChange={(last_name) =>
                              setData({ ...data, last_name })
                            }
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Date of Birth:
                          </label>
                          <Calender
                            type="date"
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.date_of_birth}
                            onChange={(date_of_birth) =>
                              setData({ ...data, date_of_birth })
                            }
                            placeholder="Enter Date Of Birth"
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Employee Status:
                          </label>
                          <select
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.employement_status}
                            onChange={(e) =>
                              setData({
                                ...data,
                                employement_status: e.target.value,
                              })
                            }
                          >
                            <option value>Select Employee Status</option>
                            <option value="Full-Time">Full-Time</option>
                            <option value="Part-Time">Part-Time</option>
                            <option value="Internship">Internship</option>
                            <option value="Probationary">Probationary</option>
                          </select>
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Shift:
                          </label>
                          {shiftLoading ? (
                            <Loading />
                          ) : (
                            <select
                              className="site-input w-100 grey-input px-3 py-2"
                              value={data?.shift}
                              onChange={(e) =>
                                setData({ ...data, shift: e.target.value })
                              }
                            >
                              <option value>Select Shift</option>
                              {all_shift?.data?.shifts?.map((shift) => (
                                <option value={shift?._id}>
                                  {shift?.title} || {shift.from} - {shift.to}
                                </option>
                              ))}
                            </select>
                          )}
                        </div>
                        <div className="col-lg-6">
                          <label htmlFor className="medium ubuntu p-lg">
                            Email Address:
                          </label>
                          <Input
                            type="email"
                            placeholder="Enter Email Address"
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.email}
                            onChange={(email) => setData({ ...data, email })}
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Password:
                          </label>
                          <InputPassword
                            type="password"
                            placeholder="Enter Password"
                            className="site-input login-input px-3 py-2 w-100"
                            value={data?.password}
                            onChange={(password) =>
                              setData({ ...data, password })
                            }
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Confirm Password:
                          </label>
                          <InputPassword
                            type="password"
                            placeholder="Enter Password"
                            className="site-input login-input px-3 py-2 w-100"
                            value={data?.confirm_password}
                            onChange={(confirm_password) =>
                              setData({ ...data, confirm_password })
                            }
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Branch:
                          </label>
                          <AsyncSelect
                            loadOptions={(inputValue, callBack) =>
                              debounceLoadBranches(inputValue, callBack)
                            }
                            onChange={(opt) => {
                              setData({ ...data, branch: opt });
                            }}
                            defaultOptions={branches}
                            value={data?.branch}
                            styles={{
                              control: (styles) => ({
                                ...styles,
                                cursor: "pointer",
                              }),
                            }}
                            placeholder=""
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Basic Salary:
                          </label>
                          <InputNumber
                            type="number"
                            placeholder="Enter Salary"
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.salary}
                            onChange={(salary) => setData({ ...data, salary })}
                          />
                        </div>
                        <div className="col-12">
                          <h5 className="mt-4">Bank Details:</h5>
                        </div>
                        <div className="col-lg-6">
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Bank:
                          </label>
                          <Input
                            type="text"
                            placeholder="Enter Bank"
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.bank_details?.bank}
                            onChange={(bank) =>
                              setData({
                                ...data,
                                bank_details: {
                                  ...data.bank_details,
                                  bank,
                                },
                              })
                            }
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Account Number:
                          </label>
                          <InputNumber
                            type="number"
                            placeholder="Enter Account Number"
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.bank_details?.account_number}
                            onChange={(account_number) =>
                              setData({
                                ...data,
                                bank_details: {
                                  ...data.bank_details,
                                  account_number,
                                },
                              })
                            }
                          />
                        </div>
                        <div className="col-lg-6">
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            Account Title:
                          </label>
                          <Input
                            type="text"
                            placeholder="Enter Account Title"
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.bank_details?.account_title}
                            onChange={(account_title) =>
                              setData({
                                ...data,
                                bank_details: {
                                  ...data.bank_details,
                                  account_title,
                                },
                              })
                            }
                          />
                          <label htmlFor className="medium ubuntu p-lg mt-3">
                            IBAN:
                          </label>
                          <Input
                            type="text"
                            placeholder="Enter IBAN"
                            className="site-input w-100 grey-input px-3 py-2"
                            value={data?.bank_details?.iban}
                            onChange={(iban) =>
                              setData({
                                ...data,
                                bank_details: {
                                  ...data.bank_details,
                                  iban,
                                },
                              })
                            }
                          />
                        </div>
                      </div>
                      <div className="text-center mt-lg-3">
                        <Button
                          loading={isLoading}
                          onClick={handleAddEmployee}
                          className="site-btn grey-btn mt-3 px-5 py-2"
                        >
                          Add
                        </Button>
                        <button
                          class="site-btn yellow-border mt-3 mr-sm-3 px-5 py-2"
                          style={{ marginLeft: 10 }}
                          onClick={(e) => {
                            e.preventDefault();
                            history.goBack();
                          }}
                        >
                          Cancel
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <SuccessDialog target="employeeAdded">
        Employee Added Successfully!
      </SuccessDialog>
    </div>
  );
}
