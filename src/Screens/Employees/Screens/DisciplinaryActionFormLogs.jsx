import React, { useState } from "react";
import { useQuery } from "react-query";
import { Link } from "react-router-dom";
import { getDisciplinaryActionLogs } from "../../../Apis";
import LoadingView from "../../../Components/LoadingView";

import { getMonths, getYears } from "../../../Utils/helpers";

export default function DisciplinaryActionFormLogs({ history, match }) {
  const id = match.params.id;
  const [month, setMonth] = useState();
  const [year, setYear] = useState();

  const { isFetching, isLoading, data } = useQuery(
    ["disciplinary_action_logs", year, month, id],
    () => getDisciplinaryActionLogs(year, month, id),
    { keepPreviousData: true }
  );

  if (isLoading || isFetching) return <LoadingView />;

  return (
    <div className="main">
      <div className="main-body">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 px-0 px-lg-2">
              <div className="white-card mt-3 px-2 py-3 px-sm-3">
                <div className="d-md-flex align-items-center justify-content-between">
                  <h5 className="mt-0 mb-0">
                    Employee's Disciplinary Action Form
                  </h5>
                  <button
                    className="site-btn grey-btn px-3 mt-3 mt-sm-0 py-2"
                    onClick={() =>
                      history.push(
                        `/employee/disciplinary-action-form/new/${id}`
                      )
                    }
                  >
                    Submit New Form
                  </button>
                </div>
                <div className="d-sm-flex align-items-center mt-3">
                  <div>
                    <label htmlFor className="d-grey-text ubuntu d-block p-sm">
                      Filter by Year
                    </label>
                    <select
                      value={year}
                      onChange={(e) => setYear(e.target.value)}
                      className="site-input w-190 px-3 py-2"
                    >
                      <option value>Select Year</option>
                      {getYears()?.map((month) => (
                        <option value={month}>{month}</option>
                      ))}
                    </select>
                  </div>
                  <div className="ml-md-3">
                    <label htmlFor className="d-grey-text ubuntu d-block p-sm">
                      Filter By Month
                    </label>
                    <select
                      className="site-input w-190 px-3 py-2"
                      value={month}
                      onChange={(e) => setMonth(e.target.value)}
                    >
                      <option value="">Select Month</option>
                      {getMonths()?.map((month) => (
                        <option value={month}>{month}</option>
                      ))}
                    </select>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-10 col-xl-9 mx-auto orange-bg mt-lg-5 mb-lg-5 mb-4 mt-4">
                    <div className="maain-tabble manager-table table-responsive">
                      <table className="table text-center table-striped table-bordered zero-configuration">
                        <thead>
                          <tr>
                            <th>Year</th>
                            <th>Month</th>
                            <th className="text-center">Actions</th>
                          </tr>
                        </thead>
                        <tbody className="orange-bg p-3">
                          {data?.data?.logs?.length > 0 ? (
                            data?.data?.logs?.map((el, i) => (
                              <tr key={i}>
                                <td>{el.violationYear}</td>
                                <td>{el.violationMonth}</td>
                                <td>
                                  <Link
                                    to={`/employee/${el?.employee}/disciplinary-action-form/${el?._id}`}
                                    href="m-disciplinary-details.php"
                                    className="orange-text"
                                  >
                                    View
                                  </Link>
                                </td>
                              </tr>
                            ))
                          ) : (
                            <tr>
                              <td colSpan={3} style={{ textAlign: "center" }}>
                                No Records Found
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
