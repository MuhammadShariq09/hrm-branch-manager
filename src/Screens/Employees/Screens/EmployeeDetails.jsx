import moment from "moment";
import React from "react";
import { useQuery } from "react-query";
import { Link } from "react-router-dom";
import { getEmployeeDetails } from "../../../Apis";
import LoadingView from "../../../Components/LoadingView";
import { image_url } from "../../../Utils/connection_strings";
import { format_currency } from "../../../Utils/helpers";

export default function Details({ match, history }) {
  const id = match.params.id;
  const { data, isLoading } = useQuery(["employee_details", id], () =>
    getEmployeeDetails(id, false)
  );

  if (isLoading) return <LoadingView />;

  return (
    <div className="main-body">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 px-0 px-lg-2">
            <div className="white-card mt-3 px-2 py-3 px-sm-3">
              <div className="d-md-flex align-items-center justify-content-between">
                <h4 className="mb-0">Employee Details</h4>
                <div className="d-sm-flex align-items-center">
                  <Link
                    className="site-btn mt-3 mt-md-0 yellow-border p-2 mr-sm-3"
                    to={`/employee/certificate/logs/${data?.data?.employee?._id}`}
                  >
                    View Cert. of Recognition
                  </Link>
                  <button
                    className="site-btn mt-3 mt-md-0 d-flex align-items-center grey-btn px-4 py-2"
                    onClick={() => history.push(`/employee/messages/${id}`)}
                  >
                    <svg
                      className="mr-1"
                      xmlns="http://www.w3.org/2000/svg"
                      width="25.781"
                      height="24.492"
                      viewBox="0 0 25.781 24.492"
                    >
                      <path
                        id="chat"
                        d="M12.89,1.5C5.783,1.5,0,6.7,0,13.1a10.725,10.725,0,0,0,2.049,6.275c-.254,2.809-.935,4.894-1.923,5.882a.43.43,0,0,0,.3.733.4.4,0,0,0,.06,0,22.954,22.954,0,0,0,7.14-2.294A13.986,13.986,0,0,0,12.89,24.7c7.108,0,12.89-5.2,12.89-11.6S20,1.5,12.89,1.5ZM6.875,14.82A1.719,1.719,0,1,1,8.594,13.1,1.72,1.72,0,0,1,6.875,14.82Zm6.016,0A1.719,1.719,0,1,1,14.609,13.1,1.72,1.72,0,0,1,12.89,14.82Zm6.016,0A1.719,1.719,0,1,1,20.625,13.1,1.72,1.72,0,0,1,18.906,14.82Z"
                        transform="translate(0 -1.5)"
                        fill="#fff"
                      />
                    </svg>
                    Message
                  </button>
                </div>
              </div>
              <div className="orange-bg mt-3 py-lg-5 p-sm-4 p-3">
                <div className="d-flex align-items-center justify-content-end">
                  <button
                    onClick={() => history.push(`/employee/edit/${id}`)}
                    className="site-btn yellow-border px-2 py-1"
                  >
                    <i className="fas fa-pencil-alt" />
                  </button>
                  <div className="btn-group ml-2 dropleft">
                    <button
                      type="button"
                      className="transparent-btn"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      {" "}
                      <i className="fa fa-ellipsis-v" />
                    </button>
                    <div className="dropdown-menu mt-5 custom-dropdown">
                      <Link
                        to={`/employee/disciplinary-action-forms/${id}`}
                        className="dropdown-item d-grey-text"
                      >
                        Disciplinary Action Form
                      </Link>
                      <Link
                        to={`/employee/monthly-report/${id}`}
                        className="dropdown-item d-grey-text"
                      >
                        View Monthly Report
                      </Link>
                      <Link
                        to={`/employee/attendance-report/${id}`}
                        className="dropdown-item d-grey-text"
                      >
                        View Attendance Report
                      </Link>
                      <Link
                        to={`/employee/training-details/${id}`}
                        className="dropdown-item d-grey-text"
                      >
                        View Training Details
                      </Link>
                      <Link
                        to={`/employee/forms/${id}`}
                        className="dropdown-item d-grey-text"
                      >
                        View Forms
                      </Link>
                    </div>
                  </div>
                </div>
                <div class="profile-img mx-auto">
                  <img
                    src={
                      data?.data?.employee?.user_image
                        ? `${image_url}${data?.data?.employee?.user_image}`
                        : "images/profile-img.png"
                    }
                    alt=""
                    class="img-fluid"
                  />
                </div>
                <div className="row">
                  <div className="col-xl-9 col-lg-11 mx-auto">
                    <ul
                      className="nav nav-tabs white-card mt-lg-4 mt-3 p-3 justify-content-between profile-tabs"
                      id="myTab"
                      role="tablist"
                    >
                      <li className="nav-item" role="presentation">
                        <a
                          className="nav-link active"
                          id="home-tab"
                          data-toggle="tab"
                          href="#home"
                          role="tab"
                          aria-controls="home"
                          aria-selected="true"
                        >
                          Personal Information
                        </a>
                      </li>
                      <li className="nav-item" role="presentation">
                        <a
                          className="nav-link"
                          id="profile-tab"
                          data-toggle="tab"
                          href="#profile"
                          role="tab"
                          aria-controls="profile"
                          aria-selected="false"
                        >
                          Bank Details
                        </a>
                      </li>
                      <li className="nav-item" role="presentation">
                        <a
                          className="nav-link"
                          id="contact-tab"
                          data-toggle="tab"
                          href="#contact"
                          role="tab"
                          aria-controls="contact"
                          aria-selected="false"
                        >
                          Documents
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="tab-content" id="myTabContent">
                  <div
                    className="tab-pane fade show active"
                    id="home"
                    role="tabpanel"
                    aria-labelledby="home-tab"
                  >
                    <div className="row mt-2">
                      <div className="col-xl-4 col-md-6">
                        <div className="row">
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">First Name:</p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {data?.data?.employee?.first_name}
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">Date of Birth:</p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {moment(
                                data?.data?.employee?.date_of_birth
                              ).format("DD-MM-YYYY")}
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">Branch:</p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {data?.data?.employee?.branch?.title}
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="col-xl-4 col-md-6">
                        <div className="row">
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">Last Name:</p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {data?.data?.employee?.last_name}
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">Employee ID:</p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {data?.data?.employee?._id}
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">Shift:</p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {data?.data?.employee?.shift?.title}
                              <br />
                              {data?.data?.employee?.shift?.from} -{" "}
                              {data?.data?.employee?.shift?.to}
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="col-xl-4 col-md-6">
                        <div className="row">
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">Email Address:</p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {data?.data?.employee?.auth?.email}
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">
                              Employment Status:
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {data?.data?.employee?.employement_status}
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">Basic Salary:</p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {format_currency(data?.data?.employee?.salary)}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade"
                    id="profile"
                    role="tabpanel"
                    aria-labelledby="profile-tab"
                  >
                    <div className="row mt-2">
                      <div className="col-xl-5 col-lg-7 mx-auto">
                        <div className="row">
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">Bank:</p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {data?.data?.employee?.bank_details?.bank}
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">Account Title:</p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {
                                data?.data?.employee?.bank_details
                                  ?.account_title
                              }
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">
                              Account Number:
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {
                                data?.data?.employee?.bank_details
                                  ?.account_number
                              }
                            </p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 ubuntu medium">IBAN:</p>
                          </div>
                          <div className="col-sm-6 mt-3">
                            <p className="mb-0 d-grey-text">
                              {data?.data?.employee?.bank_details?.iban}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade"
                    id="contact"
                    role="tabpanel"
                    aria-labelledby="contact-tab"
                  >
                    <div className="row mt-2">
                      <div className="col-12 text-right">
                        <button className="site-btn yellow-border px-3 py-2">
                          Upload Document
                        </button>
                      </div>
                      <div className="col-12 mt-3">
                        <div className="maain-tabble table-responsive">
                          <table className="table text-left table-striped table-bordered zero-configuration">
                            <thead>
                              <tr>
                                <th className="orange-bg">S.No.</th>
                                <th className="orange-bg">Document Title</th>
                                <th className="orange-bg">Uploaded On</th>
                                <th className="orange-bg text-center">
                                  Action
                                </th>
                              </tr>
                            </thead>
                            <tbody className="orange-bg p-3">
                              <tr>
                                <td>01</td>
                                <td>documentabc.pdf</td>
                                <td>mm/dd/yyyy</td>
                                <td className="text-center">
                                  <a
                                    href="#_"
                                    data-target=".deletePdf"
                                    data-toggle="modal"
                                  >
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width={15}
                                      height="18.469"
                                      viewBox="0 0 15 18.469"
                                    >
                                      <g
                                        id="delete_1_"
                                        data-name="delete (1)"
                                        transform="translate(0.003 0.002)"
                                      >
                                        <path
                                          id="Path_4025"
                                          data-name="Path 4025"
                                          d="M222.831,154.7a.432.432,0,0,0-.433.433v8.175a.433.433,0,1,0,.865,0v-8.175A.432.432,0,0,0,222.831,154.7Zm0,0"
                                          transform="translate(-212.782 -148.013)"
                                          fill="#545454"
                                        />
                                        <path
                                          id="Path_4026"
                                          data-name="Path 4026"
                                          d="M104.831,154.7a.432.432,0,0,0-.433.433v8.175a.433.433,0,1,0,.865,0v-8.175A.432.432,0,0,0,104.831,154.7Zm0,0"
                                          transform="translate(-99.886 -148.013)"
                                          fill="#545454"
                                        />
                                        <path
                                          id="Path_4027"
                                          data-name="Path 4027"
                                          d="M1.225,5.5V16.153A2.386,2.386,0,0,0,1.86,17.8a2.13,2.13,0,0,0,1.545.668h8.183a2.129,2.129,0,0,0,1.545-.668,2.386,2.386,0,0,0,.634-1.646V5.5a1.652,1.652,0,0,0-.424-3.249H11.13V1.707A1.7,1.7,0,0,0,9.417,0H5.576A1.7,1.7,0,0,0,3.864,1.707v.541H1.649A1.652,1.652,0,0,0,1.225,5.5ZM11.588,17.6H3.405A1.37,1.37,0,0,1,2.09,16.153V5.535H12.9V16.153A1.37,1.37,0,0,1,11.588,17.6Zm-6.86-15.9A.833.833,0,0,1,5.576.864H9.417a.833.833,0,0,1,.848.843v.541H4.729ZM1.649,3.113h11.7a.779.779,0,0,1,0,1.557H1.649a.779.779,0,0,1,0-1.557Zm0,0"
                                          transform="translate(0 0)"
                                          fill="#545454"
                                        />
                                        <path
                                          id="Path_4028"
                                          data-name="Path 4028"
                                          d="M163.831,154.7a.432.432,0,0,0-.433.433v8.175a.433.433,0,1,0,.865,0v-8.175A.432.432,0,0,0,163.831,154.7Zm0,0"
                                          transform="translate(-156.334 -148.013)"
                                          fill="#545454"
                                        />
                                      </g>
                                    </svg>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td>02</td>
                                <td>documentabc.pdf</td>
                                <td>mm/dd/yyyy</td>
                                <td className="text-center">
                                  <a
                                    href="#_"
                                    data-target=".deletePdf"
                                    data-toggle="modal"
                                  >
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width={15}
                                      height="18.469"
                                      viewBox="0 0 15 18.469"
                                    >
                                      <g
                                        id="delete_1_"
                                        data-name="delete (1)"
                                        transform="translate(0.003 0.002)"
                                      >
                                        <path
                                          id="Path_4025"
                                          data-name="Path 4025"
                                          d="M222.831,154.7a.432.432,0,0,0-.433.433v8.175a.433.433,0,1,0,.865,0v-8.175A.432.432,0,0,0,222.831,154.7Zm0,0"
                                          transform="translate(-212.782 -148.013)"
                                          fill="#545454"
                                        />
                                        <path
                                          id="Path_4026"
                                          data-name="Path 4026"
                                          d="M104.831,154.7a.432.432,0,0,0-.433.433v8.175a.433.433,0,1,0,.865,0v-8.175A.432.432,0,0,0,104.831,154.7Zm0,0"
                                          transform="translate(-99.886 -148.013)"
                                          fill="#545454"
                                        />
                                        <path
                                          id="Path_4027"
                                          data-name="Path 4027"
                                          d="M1.225,5.5V16.153A2.386,2.386,0,0,0,1.86,17.8a2.13,2.13,0,0,0,1.545.668h8.183a2.129,2.129,0,0,0,1.545-.668,2.386,2.386,0,0,0,.634-1.646V5.5a1.652,1.652,0,0,0-.424-3.249H11.13V1.707A1.7,1.7,0,0,0,9.417,0H5.576A1.7,1.7,0,0,0,3.864,1.707v.541H1.649A1.652,1.652,0,0,0,1.225,5.5ZM11.588,17.6H3.405A1.37,1.37,0,0,1,2.09,16.153V5.535H12.9V16.153A1.37,1.37,0,0,1,11.588,17.6Zm-6.86-15.9A.833.833,0,0,1,5.576.864H9.417a.833.833,0,0,1,.848.843v.541H4.729ZM1.649,3.113h11.7a.779.779,0,0,1,0,1.557H1.649a.779.779,0,0,1,0-1.557Zm0,0"
                                          transform="translate(0 0)"
                                          fill="#545454"
                                        />
                                        <path
                                          id="Path_4028"
                                          data-name="Path 4028"
                                          d="M163.831,154.7a.432.432,0,0,0-.433.433v8.175a.433.433,0,1,0,.865,0v-8.175A.432.432,0,0,0,163.831,154.7Zm0,0"
                                          transform="translate(-156.334 -148.013)"
                                          fill="#545454"
                                        />
                                      </g>
                                    </svg>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td>03</td>
                                <td>documentabc.pdf</td>
                                <td>mm/dd/yyyy</td>
                                <td className="text-center">
                                  <a
                                    href="#_"
                                    data-target=".deletePdf"
                                    data-toggle="modal"
                                  >
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width={15}
                                      height="18.469"
                                      viewBox="0 0 15 18.469"
                                    >
                                      <g
                                        id="delete_1_"
                                        data-name="delete (1)"
                                        transform="translate(0.003 0.002)"
                                      >
                                        <path
                                          id="Path_4025"
                                          data-name="Path 4025"
                                          d="M222.831,154.7a.432.432,0,0,0-.433.433v8.175a.433.433,0,1,0,.865,0v-8.175A.432.432,0,0,0,222.831,154.7Zm0,0"
                                          transform="translate(-212.782 -148.013)"
                                          fill="#545454"
                                        />
                                        <path
                                          id="Path_4026"
                                          data-name="Path 4026"
                                          d="M104.831,154.7a.432.432,0,0,0-.433.433v8.175a.433.433,0,1,0,.865,0v-8.175A.432.432,0,0,0,104.831,154.7Zm0,0"
                                          transform="translate(-99.886 -148.013)"
                                          fill="#545454"
                                        />
                                        <path
                                          id="Path_4027"
                                          data-name="Path 4027"
                                          d="M1.225,5.5V16.153A2.386,2.386,0,0,0,1.86,17.8a2.13,2.13,0,0,0,1.545.668h8.183a2.129,2.129,0,0,0,1.545-.668,2.386,2.386,0,0,0,.634-1.646V5.5a1.652,1.652,0,0,0-.424-3.249H11.13V1.707A1.7,1.7,0,0,0,9.417,0H5.576A1.7,1.7,0,0,0,3.864,1.707v.541H1.649A1.652,1.652,0,0,0,1.225,5.5ZM11.588,17.6H3.405A1.37,1.37,0,0,1,2.09,16.153V5.535H12.9V16.153A1.37,1.37,0,0,1,11.588,17.6Zm-6.86-15.9A.833.833,0,0,1,5.576.864H9.417a.833.833,0,0,1,.848.843v.541H4.729ZM1.649,3.113h11.7a.779.779,0,0,1,0,1.557H1.649a.779.779,0,0,1,0-1.557Zm0,0"
                                          transform="translate(0 0)"
                                          fill="#545454"
                                        />
                                        <path
                                          id="Path_4028"
                                          data-name="Path 4028"
                                          d="M163.831,154.7a.432.432,0,0,0-.433.433v8.175a.433.433,0,1,0,.865,0v-8.175A.432.432,0,0,0,163.831,154.7Zm0,0"
                                          transform="translate(-156.334 -148.013)"
                                          fill="#545454"
                                        />
                                      </g>
                                    </svg>
                                  </a>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
