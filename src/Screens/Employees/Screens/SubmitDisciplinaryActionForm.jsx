import moment from "moment";
import React, { useState } from "react";

import Calender from "../../../Components/Elements/Form/Calender";
import TimePicker from "../../../Components/Elements/Form/TimePicker";
import { changeRoute } from "../../../Utils/helpers";
import { submitDisciplinaryActionForm } from "../../../Apis";
import { useMutation } from "react-query";
import Button from "../../../Components/Elements/Form/Button";
import Input from "../../../Components/Elements/Form/Input";
import Warning from "../../../Components/Elements/Modals/Modal.Warning";
import SuccessDialog from "../../../Components/Elements/Modals/Dialog.Success";

export default function SubmitDisciplinaryActionForm({ history, match }) {
  const id = match.params.id;
  const [open1, setOpen1] = useState(false);
  const [open2, setOpen2] = useState(false);
  const [state, setState] = useState({
    dateOfWarning: "",
    violationType: "",
    other: "",
    violationDate: "",
    violationTime: "",
    violationNotes: "",
  });

  const { mutate, isLoading } = useMutation(
    (data) => submitDisciplinaryActionForm(data),
    {
      onSuccess: () => {
        window.$(".disciplinarySubmitted").modal("show");
        changeRoute(history, `/employee/disciplinary-action-forms/${id}`);
      },
      onError: (err) => Error(err?.response?.data?.message),
    }
  );

  const handleSubmit = () => {
    if (state?.violationType === "Other" && state?.other === "")
      Warning("Please specify violation type!");
    else mutate({ ...state, employeeId: id });
  };

  return (
    <div className="main">
      <div className="main-body">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 px-0 px-lg-2">
              <div className="white-card mt-3 px-2 py-3 px-sm-3">
                <h4 className="mb-0">Disciplinary Action Form</h4>
                <div className="profile-card mt-3 p-sm-4 pb-lg-5 p-3">
                  <form action="m-candidates.php">
                    <div className="row">
                      <div className="col-lg-6">
                        <h5 className="bold">Date of Warning:</h5>
                        <Calender
                          className="site-input w-100 grey-input px-3 py-2"
                          value={state?.dateOfWarning}
                          open={open1}
                          onChange={(date) => {
                            setState({ ...state, dateOfWarning: date });
                          }}
                          onInputClick={() => {
                            setOpen1(!open1);
                          }}
                          onClickOutside={() => setOpen1(false)}
                        />
                      </div>
                      <div className="col-12 mt-4">
                        <h5 className="bold mb-0">Type of Violation:</h5>
                      </div>
                      <div className="col-lg-6">
                        <label htmlFor className="medium ubuntu p-lg mt-3">
                          Select an option:
                        </label>
                        <select
                          value={state?.violationType}
                          onChange={(e) => {
                            setState({
                              ...state,
                              violationType: e.target.value,
                            });
                          }}
                          className="site-input w-100 grey-input px-3 py-2"
                        >
                          <option value>Select an option</option>
                          {[
                            "Attendance",
                            "Carelessness",
                            "Disobedience",
                            "Safety",
                            "Tardiness",
                            "Work Quality",
                            "Other",
                          ].map((el, i) => (
                            <option key={i} value={el}>
                              {el}
                            </option>
                          ))}
                        </select>
                      </div>
                      <div className="col-lg-6">
                        <label htmlFor className="medium ubuntu p-lg mt-3">
                          Other:
                        </label>
                        <Input
                          className="site-input w-100 grey-input px-3 py-2"
                          placeholder="Add other option details"
                          value={state?.other}
                          onChange={(other) =>
                            setState({
                              ...state,
                              other,
                            })
                          }
                        />
                      </div>
                      <div className="col-12 mt-4">
                        <h5 className="bold mb-0">Warning Details:</h5>
                      </div>
                      <div className="col-lg-6">
                        <label htmlFor className="medium ubuntu p-lg mt-3">
                          Violation Date:
                        </label>
                        <Calender
                          className="site-input w-100 grey-input px-3 py-2"
                          value={state?.violationDate}
                          open={open2}
                          onChange={(violationDate) => {
                            setState({ ...state, violationDate });
                          }}
                          onInputClick={() => {
                            setOpen2(!open2);
                          }}
                          onClickOutside={() => setOpen2(false)}
                        />
                      </div>
                      <div className="col-lg-6">
                        <label htmlFor className="medium ubuntu p-lg mt-3">
                          Violation Time:
                        </label>
                        <TimePicker
                          className="site-input w-100 grey-input px-3 py-2"
                          value={state?.violationTime}
                          onChange={(violationTime) =>
                            setState({ ...state, violationTime })
                          }
                          format="HH:mm a"
                        />
                      </div>
                      <div className="col-12">
                        <label htmlFor className="medium ubuntu p-lg mt-3">
                          Violation Location:
                        </label>
                        <textarea
                          className="site-input w-100 grey-input p-3"
                          rows={8}
                          placeholder="Add other option details"
                          defaultValue={""}
                          onChange={(e) =>
                            setState({
                              ...state,
                              violationNotes: e.target.value,
                            })
                          }
                        />
                      </div>
                    </div>
                    <div className="d-sm-flex align-items-center justify-content-center mt-lg-3">
                      <Button
                        className="site-btn grey-btn mt-3 px-5 py-2"
                        loading={isLoading}
                        onClick={handleSubmit}
                      >
                        Submit
                      </Button>
                      <button
                        className="site-btn yellow-border mt-3 px-5 py-2 ml-sm-3"
                        type="button"
                        onClick={() => history.goBack()}
                      >
                        Cancel
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <SuccessDialog target="disciplinarySubmitted">
        Form Submitted Successfully
      </SuccessDialog>
    </div>
  );
}
