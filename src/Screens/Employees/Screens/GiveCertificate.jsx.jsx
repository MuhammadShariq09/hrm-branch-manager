import React from "react";
import { useState } from "react";
import { useMutation, useQuery } from "react-query";
import { giveCertificate, getEmployeeName } from "../../../Apis";
import Button from "../../../Components/Elements/Form/Button";
import Confirmation from "../../../Components/Elements/Modals/Modal.Confirmation";
import Error from "../../../Components/Elements/Modals/Modal.Error";
import Success from "../../../Components/Elements/Modals/Modal.Success";
import LoadingView from "../../../Components/LoadingView";
import { getMonths } from "../../../Utils/helpers";
import Certificate from "../Components/Certificate";

export default function GiveCertificate({ history, match }) {
  const id = match.params.id;
  const [month, setMonth] = useState();
  const [awarded_by] = useState("Branch Manager");

  const { data, isLoading: fetchingEmployee } = useQuery(
    ["employee_full_name", id],
    () => getEmployeeName(id)
  );

  const { mutate, isLoading } = useMutation((data) => giveCertificate(data), {
    onSuccess: (res) => {
      Success(res?.data?.message);
    },
    onError: (err) => Error(err?.response?.data?.message),
  });

  if (fetchingEmployee) return <LoadingView />;

  return (
    <div className="main-body">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 px-0 px-lg-2">
            <div className="white-card mt-3 px-2 py-3 px-sm-3">
              <h4>Give Certificate</h4>
              <label
                htmlFor
                className="mt-3 p-sm grey-text d-block ubuntu medium"
              >
                Select Month
              </label>
              <select
                value={month}
                onChange={(e) => setMonth(e.target.value)}
                className="w-190 py-2 grey-text site-btn yellow-input"
              >
                <option value="">Select Month</option>
                {getMonths()?.map((month) => (
                  <option value={month}>{month}</option>
                ))}
              </select>
              <div className="row">
                <div className="col-xl-9 col-lg-10 mx-auto">
                  <Certificate
                    employeeName={data?.data?.employee}
                    month={month}
                  />
                  {/* <img
                    src="images/certificate.png"
                    alt=""
                    className="img-fluid mt-3 w-100"
                  /> */}
                </div>
              </div>
              <div className="d-sm-flex pb-lg-5 pb-3 align-items-center justify-content-center">
                <Button
                  onClick={() =>
                    !month
                      ? Error("Please Select a Month")
                      : Confirmation(
                          `Are You Sure You Want To Award This Employee This Certificate For The Month Of ${month}?`,
                          "Yes",
                          () =>
                            mutate({
                              month,
                              awarded_by,
                              employee: id,
                              employeeId: id,
                            })
                        )
                  }
                  loading={isLoading}
                  className="site-btn grey-btn px-3 py-2"
                >
                  Send Certificate
                </Button>
                <button className="site-btn yellow-border px-3 mt-sm-0 mt-3 py-2 ml-sm-3">
                  Download PDF
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
