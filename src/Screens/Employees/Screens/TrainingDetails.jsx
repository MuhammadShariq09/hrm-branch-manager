import React, { useState, useEffect } from "react";
import { useQuery } from "react-query";

import { queryClient } from "../../../";
import LoadingView from "../../../Components/LoadingView";
import Pagination from "../../../Components//Elements/Table/Pagination";
import CourseCard from "../Components/CourseCard";
import { getEmployeeTrainingCourses } from "../../../Apis";
import { useParams } from "react-router-dom";

export default function TrainingCourses() {
  const [toggle, setToggle] = useState("ongoing");
  const [page, setPage] = useState(1);
  const { id } = useParams();

  const { isFetching, isLoading, data } = useQuery(
    ["employee_training_courses", page, toggle],
    () => getEmployeeTrainingCourses(page, id, toggle),
    {
      keepPreviousData: true,
    }
  );

  // Prefetch the next page!
  useEffect(() => {
    if (data?.data?.logs?.hasNextPage) {
      queryClient.prefetchQuery(["employee_training_courses", page + 1, id, toggle], () =>
        getEmployeeTrainingCourses(page + 1, id, toggle)
      );
    }
  }, [data, page, queryClient, toggle]);

  if (isLoading || isFetching) return <LoadingView />;

  return (
    <div className="main">
      <div className="main-body">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 px-0 px-lg-2">
              <div className="white-card mt-3 px-2 py-3 px-sm-3">
                <h4 className="mt-2 mb-0">Training Courses</h4>
                <ul className="nav nav-pills mt-3 training-tabs justify-content-center mb-3" id="pills-tab" role="tablist">
                  <li className="nav-item" role="presentation">
                    <a className={`nav-link anchorLink ${toggle === "ongoing" && "active"}`} onClick={() => setToggle("ongoing")}>
                      Ongoing
                    </a>
                  </li>
                  <li className="nav-item" role="presentation">
                    <a
                      className={`nav-link anchorLink ${toggle === "completed" && "active"}`}
                      onClick={() => setToggle("completed")}
                    >
                      Completed
                    </a>
                  </li>
                </ul>
                <div className="tab-content">
                  <div className="row">
                    {data?.data?.logs?.docs?.map((course, index) => (
                      <div className="col-xl-4 col-md-6 mt-3" key={index}>
                        <CourseCard toggle={toggle} details={{ ...course, ...course?.course_id }} />
                      </div>
                    ))}
                  </div>
                </div>
                <div className="justify-content-between">
                  <p className="p-sm mt-3 mb-0"></p>
                  <Pagination totalPages={data?.data?.logs?.totalPages} setPage={setPage} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
