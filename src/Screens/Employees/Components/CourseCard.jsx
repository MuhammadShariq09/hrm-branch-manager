import moment from "moment";
import React, { useEffect, useState } from "react";

export default function CourseCard({ details, toggle }) {
  //   const history = useHistory();

  return (
    <div className="training-card">
      <div className="training-overlay">
        <img src="images/training-6.png" alt="" className="img-fluid training-img w-100" />
      </div>
      <div className="py-3 px-2">
        <h6 className="mb-0 p-lg ubuntu">{details?.course_name}</h6>
        <p className="mb-2">Instructor Name: {details?.instructor_name}</p>
        <div className="row">
          <div className="col-6">
            <div className="d-flex align-items-center">
              <svg className="mr-2" xmlns="http://www.w3.org/2000/svg" width="15.334" height="15.334" viewBox="0 0 15.334 15.334">
                <path
                  id="play-button"
                  d="M7.667,0a7.667,7.667,0,1,0,7.667,7.667A7.667,7.667,0,0,0,7.667,0Zm2.65,8.073-3.833,2.4a.479.479,0,0,1-.733-.406V5.271a.479.479,0,0,1,.733-.406l3.833,2.4a.479.479,0,0,1,0,.813Z"
                  fill="#aaabad"
                />
              </svg>
              <p className="p-sm mb-0">Total Videos: {details?.total_videos}</p>
            </div>
          </div>
          <div className="col-6">
            <div className="d-flex align-items-center">
              <svg className="mr-2" xmlns="http://www.w3.org/2000/svg" width="15.334" height="15.334" viewBox="0 0 15.334 15.334">
                <path
                  id="clock"
                  d="M7.667,0a7.667,7.667,0,1,0,7.667,7.667A7.675,7.675,0,0,0,7.667,0Zm3.646,11.633a.638.638,0,0,1-.9,0L7.215,8.438a.636.636,0,0,1-.187-.452V3.833a.639.639,0,0,1,1.278,0V7.722l3.007,3.007a.638.638,0,0,1,0,.9Zm0,0"
                  fill="#aaabad"
                />
              </svg>
              <p className="p-sm mb-0">Total Hours: {(details?.total_hours / 360).toFixed(2)}</p>
            </div>
          </div>
          {toggle === "ongoing" ? (
            <div className="progress">
              <div
                className="progress-bar"
                role="progressbar"
                style={{ width: details?.progress }}
                aria-valuenow={details?.progress}
                aria-valuemin="0"
                aria-valuemax="100"
              ></div>
            </div>
          ) : (
            <div className="col-12 mt-3">
              <p className="p-sm mb-0 orange-text">Completed On: {moment.utc(details?.completion_date).format("DD/MM/YYYY")}</p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
