import React, { useState, useEffect } from "react";
import { useMutation, useQuery } from "react-query";
import { Link } from "react-router-dom";

import { queryClient } from "../../..";
import { changeEmployeeStatus, getEmployeeLogs } from "../../../Apis";
import Table from "../../../Components//Table";
import Filters from "../../../Components/Filters";
import SuccessDialog from "../../../Components/Elements/Modals/Dialog.Success";
import Error from "../../../Components/Elements/Modals/Modal.Error";
import { format_date } from "../../../Utils/helpers";
import ConfirmationDialog from "../../../Components/Elements/Modals/Dialog.Confirm";

export default function Logs({ history, origin }) {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [search_string, setSearchString] = useState("");
  const [status, setStatus] = useState("");
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [employeeId, setEmployeeId] = useState();
  const statusOptions = [
    { label: "Active", value: true },
    { label: "Terminated", value: false },
  ];

  const { isFetching, isLoading, data, refetch } = useQuery(
    ["employee_logs", page, perPage, search_string, status, from, to],
    () => getEmployeeLogs(page, perPage, search_string, status, from, to),
    { keepPreviousData: true }
  );

  // Prefetch the next page!
  useEffect(() => {
    if (data?.data?.logs?.hasNextPage) {
      queryClient.prefetchQuery(
        ["employee_logs", page + 1, perPage, search_string, status, from, to],
        () =>
          getEmployeeLogs(page + 1, perPage, search_string, from, to, status)
      );
    }
  }, [data, page, queryClient]);

  const { mutate: _changeEmployeeStatus, isLoading: loadingEmployeeStatus } =
    useMutation(() => changeEmployeeStatus(employeeId), {
      onSuccess: (res) => {
        console.log(res?.data);
        refetch();
        if (res?.data?.status === "Activated") {
          window.$(".reactiveEmployee").modal("hide");
          window.$(".employeeActivated").modal("show");
        } else {
          window.$(".terminateEmployee").modal("hide");
          window.$(".employeeTerminated").modal("show");
        }
      },
      onError: (err) => Error(err?.response?.data?.message),
    });

  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 px-0 px-lg-2">
            <div className="white-card mt-3 px-2 py-3 px-sm-3">
              <div className="d-md-flex align-items-center justify-content-between">
                <h5 className="mt-0 mb-0">
                  {origin === "Dashboard" ? "Recent Employees" : "Employee"}
                </h5>
                {origin === "Employee" && (
                  <div className="d-sm-flex">
                    <button
                      className="site-btn grey-btn px-3 mt-3 mt-sm-0 py-2 mr-sm-3"
                      onClick={() => history.push("/candidate/logs")}
                    >
                      View Candidates
                    </button>
                    <button
                      className="site-btn grey-btn px-2 mt-3 mt-sm-0 py-2"
                      onClick={() => history.push("/employee/add")}
                    >
                      Add New Employee
                    </button>
                  </div>
                )}
              </div>
              <div className="orange-bg d-md-flex flex-wrap align-items-center mt-3 p-3">
                <Filters
                  perPage={perPage}
                  setPerPage={setPerPage}
                  statusOptions={statusOptions}
                  status={status}
                  setStatus={setStatus}
                  dateFilters={true}
                  dateTo={to}
                  setDateTo={setTo}
                  dateFrom={from}
                  setDateFrom={setFrom}
                  setSearchString={setSearchString}
                  isFetching={isFetching}
                />
              </div>
            </div>
            <div className="maain-tabble manager-table table-responsive">
              <Table
                headings={[
                  "S. No",
                  "First Name",
                  "Last Name",
                  "Date",
                  "Email Address",
                  "Emp. Status",
                  "Status",
                  "Action",
                ]}
                perPage={perPage}
                setPerPage={setPerPage}
                totalPages={data?.data?.logs?.totalPages}
                setPage={setPage}
                data={data?.data?.logs?.docs}
                isFetching={isFetching}
                isLoading={isLoading || loadingEmployeeStatus}
              >
                <tbody className="orange-bg p-3">
                  {data?.data?.logs?.docs?.map((el, i) => (
                    <tr>
                      <td>{i + 1}</td>
                      <td>{el?.first_name}</td>
                      <td>{el?.last_name}</td>
                      <td>{format_date(el?.createdAt)}</td>
                      <td>{el?.auth?.email}</td>
                      <td>{el?.employement_status}</td>
                      <td>{Boolean(el?.status) ? "Active" : "Terminated"}</td>
                      <td class="text-center">
                        <div class="btn-group ml-2 mb-1">
                          <button
                            type="button"
                            class="transparent-btn"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            {" "}
                            <i class="fa fa-ellipsis-v"></i>
                          </button>
                          <div class="dropdown-menu custom-dropdown">
                            <Link
                              to={`/employee/details/${el?._id}`}
                              href="m-employee-details.php"
                              class="dropdown-item"
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="11.678"
                                height="7.962"
                                viewBox="0 0 11.678 7.962"
                              >
                                <path
                                  id="Icon_material-remove-red-eye"
                                  data-name="Icon material-remove-red-eye"
                                  d="M7.339,6.75A6.278,6.278,0,0,0,1.5,10.731a6.272,6.272,0,0,0,11.678,0A6.278,6.278,0,0,0,7.339,6.75Zm0,6.635a2.654,2.654,0,1,1,2.654-2.654A2.655,2.655,0,0,1,7.339,13.385Zm0-4.246a1.592,1.592,0,1,0,1.592,1.592A1.59,1.59,0,0,0,7.339,9.139Z"
                                  transform="translate(-1.5 -6.75)"
                                  fill="#666"
                                />
                              </svg>
                              View
                            </Link>
                            <Link
                              class="dropdown-item"
                              to={`/employee/edit/${el?._id}`}
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="10.215"
                                height="10.215"
                                viewBox="0 0 10.215 10.215"
                              >
                                <path
                                  id="Icon_material-edit"
                                  data-name="Icon material-edit"
                                  d="M4.5,12.583v2.128H6.628L12.9,8.435,10.775,6.308ZM14.549,6.79a.565.565,0,0,0,0-.8L13.221,4.662a.565.565,0,0,0-.8,0L11.383,5.7,13.51,7.828,14.549,6.79Z"
                                  transform="translate(-4.5 -4.496)"
                                  fill="#545454"
                                />
                              </svg>
                              Edit
                            </Link>
                            <a
                              class="dropdown-item"
                              data-toggle="modal"
                              data-target={
                                Boolean(el?.status)
                                  ? ".terminateEmployee"
                                  : ".reactiveEmployee"
                              }
                              onClick={() => setEmployeeId(el?._id)}
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="14.42"
                                height="14.134"
                                viewBox="0 0 14.42 14.134"
                              >
                                <g
                                  id="Group_1098"
                                  data-name="Group 1098"
                                  transform="translate(-1460.145 -1471.475)"
                                >
                                  <path
                                    id="Path_3560"
                                    data-name="Path 3560"
                                    d="M8.346,236.459a5.034,5.034,0,0,0-3.238-1.16H5.1a5.019,5.019,0,0,0-3.61,1.512A5.38,5.38,0,0,0,0,240.4l8.255-.017a3.568,3.568,0,0,1,.09-3.922Zm0,0"
                                    transform="translate(1460.145 1244.005)"
                                    fill="#666"
                                  />
                                  <path
                                    id="Path_3561"
                                    data-name="Path 3561"
                                    d="M247.316,237.4a3.091,3.091,0,1,0,2.212.905A3.117,3.117,0,0,0,247.316,237.4Z"
                                    transform="translate(1224.131 1241.976)"
                                    fill="#d50c0c"
                                  />
                                  <path
                                    id="Path_3559"
                                    data-name="Path 3559"
                                    d="M69.446,3.075A2.973,2.973,0,1,1,66.473.1,2.973,2.973,0,0,1,69.446,3.075Zm0,0"
                                    transform="translate(1398.773 1471.374)"
                                    fill="#666"
                                  />
                                </g>
                              </svg>
                              {Boolean(el?.status) ? "Terminate" : "Reactive"}
                            </a>
                          </div>
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </div>
        </div>
      </div>
      <ConfirmationDialog
        target="terminateEmployee"
        onSuccess={() => _changeEmployeeStatus()}
      >
        Are you sure you want to terminate this employee?
      </ConfirmationDialog>
      <SuccessDialog target="employeeTerminated">
        Employee Terminated Successfully!
      </SuccessDialog>

      <ConfirmationDialog
        target="reactiveEmployee"
        onSuccess={() => _changeEmployeeStatus()}
      >
        Are you sure you want to reactivate this employee?
      </ConfirmationDialog>
      <SuccessDialog target="employeeActivated">
        Employee Activated Successfully!
      </SuccessDialog>
    </>
  );
}
