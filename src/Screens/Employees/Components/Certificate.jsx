import moment from "moment";
import React from "react";

export default function Certificate({ employeeName, date, month }) {
  const employee_name = `${employeeName?.first_name} ${employeeName?.last_name}`;
  let _date = moment().format("Do of MMMM YYYY");
  if (date) {
    _date = moment(date).format("Do of MMMM YYYY");
  } else if (month) {
    _date = moment(month, "MMMM").format("MMMM");
    _date = `${moment().format("Do")} of ${_date} ${moment().format("YYYY")}`;
  }

  return (
    <div>
      <table
        width={600}
        border={0}
        cellSpacing={0}
        cellPadding={0}
        className="email-body"
        style={{ border: "2px solid #b78b40" }}
      >
        <tbody>
          <tr>
            <td width={80} />
            <td align="center">
              <table width="100%" border={0}>
                <tbody>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center">
                      <img src="images/heading.jpg" width={250} />
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center">
                      <img src="images/heading-award.jpg" width={180} />
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td
                      align="center"
                      style={{
                        fontFamily:
                          'Arial, "Helvetica Neue", Helvetica, sans-serif',
                        fontSize: "40px",
                        color: "#b78b40",
                        fontStyle: "italic",
                      }}
                    >
                      <em>{employee_name}</em>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td
                      align="center"
                      style={{
                        fontFamily:
                          'Arial, "Helvetica Neue", Helvetica, sans-serif',
                        fontSize: "14px",
                        color: "#000",
                      }}
                    >
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s,
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td
                      align="center"
                      style={{
                        fontFamily:
                          'Arial, "Helvetica Neue", Helvetica, sans-serif',
                        fontSize: "14px",
                        color: "#000",
                      }}
                    >
                      Given on the {_date}, at the San Diego Headquarters
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center">
                      <table width="80%" border={0}>
                        <tbody>
                          <tr>
                            <td align="center">
                              <table width="100%" border={0}>
                                <tbody>
                                  <tr>
                                    <td>
                                      <input
                                        type="text"
                                        style={{
                                          background: "#fff",
                                          border: 0,
                                          borderBottom: "2px solid #000",
                                        }}
                                      />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td
                                      align="center"
                                      style={{
                                        fontFamily:
                                          'Arial, "Helvetica Neue", Helvetica, sans-serif',
                                        fontSize: "18px",
                                        color: "#000",
                                      }}
                                    >
                                      <strong>Ruby D. Huffman</strong>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td
                                      align="center"
                                      style={{
                                        fontFamily:
                                          'Arial, "Helvetica Neue", Helvetica, sans-serif',
                                        fontSize: "14px",
                                        color: "#000",
                                      }}
                                    >
                                      Chief Executive Officer
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                            <td width={40}>&nbsp;</td>
                            <td>
                              <table width="100%" border={0}>
                                <tbody>
                                  <tr>
                                    <td>
                                      <input
                                        type="text"
                                        style={{
                                          background: "#fff",
                                          border: 0,
                                          borderBottom: "2px solid #000",
                                        }}
                                      />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td
                                      align="center"
                                      style={{
                                        fontFamily:
                                          'Arial, "Helvetica Neue", Helvetica, sans-serif',
                                        fontSize: "18px",
                                        color: "#000",
                                      }}
                                    >
                                      <strong>David P. Liriano</strong>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td
                                      align="center"
                                      style={{
                                        fontFamily:
                                          'Arial, "Helvetica Neue", Helvetica, sans-serif',
                                        fontSize: "14px",
                                        color: "#000",
                                      }}
                                    >
                                      Department Head
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                </tbody>
              </table>
            </td>
            <td width={80} />
          </tr>
        </tbody>
      </table>
    </div>
  );
}
