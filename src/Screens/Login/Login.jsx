import React, { useEffect, useState } from "react";
import { useMutation } from "react-query";
import { Link } from "react-router-dom";
import { useRecoilState } from "recoil";

import { login, me } from "../../Apis";
import Button from "../../Components/Elements/Form/Button";
import Input from "../../Components/Elements/Form/Input";
import InputPassword from "../../Components/Elements/Form/InputPassword";
import Error from "../../Components/Elements/Modals/Modal.Error";
import { userState } from "../../Recoil";

export default function Login({ history }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { mutate, isLoading } = useMutation((data) => login(data), {
    retry: false,
    onSuccess: (res) => {
      localStorage.setItem("TokenBranchManagerHRM", res.data.token);
      window.location.reload();
    },
    onError: (err) => Error(err?.response?.data?.message),
  });

  // AFTER LOGIN FLOW
  const [user, setUser] = useRecoilState(userState);
  const [loading, setLoading] = useState(true);

  const { mutate: meMutate } = useMutation(() => me(), {
    retry: false,
    onSuccess: (res) => {
      setUser(res?.data?.branch_manager);
      history.replace("/dashboard");
    },
    onError: (err) => {
      localStorage.clear();
      history.push("/");
      setLoading(false);
    },
  });

  useEffect(() => {
    const Token = localStorage.getItem("TokenBranchManagerHRM");
    if (Token) {
      setLoading(true);
      meMutate();
    } else {
      setLoading(false);
    }
  }, []);

  if (loading)
    return (
      <div
        style={{
          height: "100vh",
          width: "100vw",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          background: "#000",
        }}
      >
        <img src="images/logo.png" />
      </div>
    );

  return (
    <div className="main">
      <section className="login">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-6 login-left px-md-0">
              <img src="images/logo-bg.png" alt="" className="img-fluid" />
              <p className="mt-3">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum.{" "}
              </p>
              <a href="index.php" className="d-blue-text ubuntu">
                Back to website
              </a>
            </div>
            <div className="col-md-6 login-right px-md-0">
              <h1>Log in</h1>
              <form className="w-100">
                <label htmlFor className="mt-3 ubuntu p-lg">
                  Email <span className="red-text">*</span>
                </label>
                <Input
                  type="email"
                  placeholder="Enter Email"
                  className="site-input login-input px-3 py-2 w-100"
                  value={email}
                  onChange={(email) => setEmail(email)}
                />
                <label htmlFor className="mt-3 ubuntu p-lg">
                  Password <span className="red-text">*</span>
                </label>
                <div className="position-relative">
                  <InputPassword
                    type="password"
                    placeholder="Enter Password"
                    className="site-input login-input px-3 py-2 w-100"
                    value={password}
                    onChange={(password) => setPassword(password)}
                    onKeyPress={() => mutate({ email, password })}
                  />
                </div>
                <div className="d-flex align-items-center justify-content-between">
                  <p className="mb-0 mt-3"></p>
                  <Link
                    to="/forgot-password"
                    className="mt-2 d-inline-block d-blue-text"
                  >
                    Forget Password?
                  </Link>
                </div>
                <Button
                  loading={isLoading}
                  onClick={() => mutate({ email, password })}
                  className="site-btn grey-btn py-2 w-100 mt-lg-5 mt-4"
                >
                  Login
                </Button>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
