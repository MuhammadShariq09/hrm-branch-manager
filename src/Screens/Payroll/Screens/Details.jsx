import React, { useState } from "react";

import InputNumber from "../../../Components/Elements/Form/InputNumber";

export default function Details() {
  const [is_edit, setIsEdit] = useState(false);
  const [state, setState] = useState({
    allownce: 123,
    overTime: 123,
    absents: 123,
    underTime: 123,
  });

  const handleUpdate = () => {
    console.log("debug", state);
  };

  return (
    <div className="main">
      <div className="main-body">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 px-0 px-lg-2">
              <div className="white-card mt-3 px-2 py-3 px-sm-3">
                <div className="d-sm-flex align-items-center justify-content-between">
                  <h4 className="mt-2 mb-0">Payroll Details</h4>
                  <button className="site-btn grey-btn px-3 py-2 mt-sm-2 mt-1">
                    Download PDF
                  </button>
                </div>
                <div className="orange-bg mt-4 p-4">
                  <div className="row">
                    <div className="col-xl-10 col-lg-11 col-12 mx-auto mt-lg-2">
                      <div className="row">
                        <div className="col-lg-6 mt-3">
                          <div className="row">
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Employee ID:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">001</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                First Name:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">abc</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Last Name:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">xyz</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Employment Status
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">Full-time</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">Branch:</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">Branch A</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Branch Salary:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">$123</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">Shift:</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">Morning</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Pay Date:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">mm/dd/yyyy</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Date Of Joining:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">mm/dd/yyyy</p>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-6 mt-3">
                          <div className="row">
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Total Days:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">22</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Days Worked:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">22</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Absent Days:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">0</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">Leaves:</p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">0</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Total Required Working Hours:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">198</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Hours Worked:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">198</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Hours Remaining:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">0</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Average Working Hours:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">9</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">Bank:</p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">Bank A</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Bank Account Number:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">01234xxxxxxxx</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="orange-bg mt-4 p-3">
                  <div className="row">
                    <div className="col-xl-10 col-lg-11 col-12 mx-auto">
                      <div className="row">
                        <div className="col-lg-6 mt-3">
                          <h5>Earnings</h5>
                          <div className="row">
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Basic Salary:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">$123</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Allowance:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              {is_edit ? (
                                <InputNumber
                                  style={{ maxWidth: 150, marginBottom: 5 }}
                                  value={state?.allownce}
                                  onChange={(allownce) =>
                                    setState({ ...state, allownce })
                                  }
                                  className="site-input yellow-input px-3 py-2 w-100"
                                />
                              ) : (
                                <p className="d-grey-text mb-2">$123</p>
                              )}
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Overtime
                              </p>
                            </div>
                            <div className="col-sm-6">
                              {is_edit ? (
                                <InputNumber
                                  style={{ maxWidth: 150, marginBottom: 5 }}
                                  value={state?.overTime}
                                  onChange={(overTime) =>
                                    setState({ ...state, overTime })
                                  }
                                  className="site-input yellow-input px-3 py-2 w-100"
                                />
                              ) : (
                                <p className="d-grey-text mb-2">$123</p>
                              )}
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Total Earings:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">$123</p>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-6 mt-3">
                          <h5>Deductions</h5>
                          <div className="row">
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">Tax:</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">$123</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Absents:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              {is_edit ? (
                                <InputNumber
                                  style={{ maxWidth: 150, marginBottom: 5 }}
                                  value={state?.absents}
                                  onChange={(absents) =>
                                    setState({ ...state, absents })
                                  }
                                  className="site-input yellow-input px-3 py-2 w-100"
                                />
                              ) : (
                                <p className="d-grey-text mb-2">$123</p>
                              )}
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Undertime:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              {is_edit ? (
                                <InputNumber
                                  style={{ maxWidth: 150, marginBottom: 5 }}
                                  value={state?.underTime}
                                  onChange={(underTime) =>
                                    setState({ ...state, underTime })
                                  }
                                  className="site-input yellow-input px-3 py-2 w-100"
                                />
                              ) : (
                                <p className="d-grey-text mb-2">$123</p>
                              )}
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Total Deductions:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">$123</p>
                            </div>
                          </div>
                        </div>
                        <div className="col-12 mt-3">
                          <h5>Net Pay: $123</h5>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 mt-4 text-center">
                    <button
                      className="site-btn grey-btn px-5 py-2"
                      onClick={!is_edit ? () => setIsEdit(true) : handleUpdate}
                    >
                      {!is_edit ? "Edit" : "Update"}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
