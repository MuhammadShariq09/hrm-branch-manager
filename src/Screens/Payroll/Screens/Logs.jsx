import React, { useState } from "react";
import { Link } from "react-router-dom";
import Filters from "../../../Components/Filters";
import Table from "../../../Components/Table";

export default function Logs() {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [search_string, setSearchString] = useState("");
  const [status, setStatus] = useState("");
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const data = [
    {
      first_name: "Mark",
      last_name: "Carson",
      shift: "Morning",
      brnach: {
        title: "Apple",
      },
      basic_salary: 123,
    },
    {
      first_name: "Mark",
      last_name: "Carson",
      shift: "Morning",
      brnach: {
        title: "Apple",
      },
      basic_salary: 123,
    },
    {
      first_name: "Mark",
      last_name: "Carson",
      shift: "Morning",
      brnach: {
        title: "Apple",
      },
      basic_salary: 123,
    },
  ];

  return (
    <div className="main">
      <div className="main-body">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 px-0 px-lg-2">
              <div className="white-card mt-3 px-2 py-3 px-sm-3">
                <h5 className="mt-0 mb-0">Payroll</h5>
                <div className="orange-bg d-md-flex flex-wrap align-items-center mt-3 p-3">
                  <Filters
                    perPage={perPage}
                    setPerPage={setPerPage}
                    dateFilters={true}
                    dateTo={to}
                    setDateTo={setTo}
                    dateFrom={from}
                    setDateFrom={setFrom}
                    setSearchString={setSearchString}
                    //   isFetching={isFetching}
                  />
                </div>
              </div>
              <div className="maain-tabble manager-table table-responsive">
                <Table
                  headings={[
                    "S. No",
                    "Emp.First Name",
                    "Emp.Last Name",
                    "Shift",
                    "Branch",
                    "Basic Salary",
                    "Action",
                  ]}
                  perPage={perPage}
                  setPerPage={setPerPage}
                  //   totalPages={data?.data?.logs?.totalPages}
                  totalPages={2}
                  setPage={setPage}
                  data={data}
                  data={data?.data?.logs?.docs}
                  //   isFetching={isFetching}
                  //   isLoading={isLoading || loadingEmployeeStatus}
                >
                  <tbody className="orange-bg p-3">
                    {data.map((el, i) => (
                      <tr>
                        <td>{i + 1}</td>
                        <td>{el?.first_name}</td>
                        <td>{el?.last_name}</td>
                        <td>{el?.shift}</td>
                        <td>{el?.brnach?.title}</td>
                        <td>{el?.basic_salary}</td>
                        <td className="text-center">
                          <div className="btn-group ml-2 mb-1">
                            <button
                              type="button"
                              className="transparent-btn"
                              data-toggle="dropdown"
                              aria-haspopup="true"
                              aria-expanded="false"
                            >
                              <i className="fa fa-ellipsis-v" />
                            </button>
                            <div className="dropdown-menu custom-dropdown">
                              <Link
                                to={`/payroll/:employeeId/details/:paryrollId`}
                                className="dropdown-item"
                              >
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="13.465"
                                  height="10.324"
                                  viewBox="0 0 13.465 10.324"
                                >
                                  <g
                                    id="stack-of-square-papers"
                                    transform="translate(0 -35.441)"
                                  >
                                    <g
                                      id="Group_1057"
                                      data-name="Group 1057"
                                      transform="translate(0 35.441)"
                                    >
                                      <path
                                        id="Path_4860"
                                        data-name="Path 4860"
                                        d="M.218,38.554l6.373,2.728a.352.352,0,0,0,.141.029.361.361,0,0,0,.141-.029l6.373-2.728a.359.359,0,0,0-.014-.665L6.86,35.465a.357.357,0,0,0-.255,0L.232,37.889a.359.359,0,0,0-.013.665Zm6.514-2.37,5.415,2.06L6.733,40.562,1.317,38.244Z"
                                        transform="translate(0 -35.441)"
                                        fill="#545454"
                                      />
                                      <path
                                        id="Path_4861"
                                        data-name="Path 4861"
                                        d="M.219,125.879l6.514,2.788,6.514-2.788a.359.359,0,0,0-.282-.66l-6.232,2.668L.5,125.22a.359.359,0,1,0-.282.66Z"
                                        transform="translate(-0.001 -121.214)"
                                        fill="#545454"
                                      />
                                      <path
                                        id="Path_4862"
                                        data-name="Path 4862"
                                        d="M.219,158.278l6.514,2.788,6.514-2.788a.359.359,0,1,0-.282-.66l-6.232,2.668L.5,157.618a.359.359,0,1,0-.282.66Z"
                                        transform="translate(-0.001 -152.178)"
                                        fill="#545454"
                                      />
                                      <path
                                        id="Path_4863"
                                        data-name="Path 4863"
                                        d="M.219,190.682l6.514,2.788,6.514-2.788a.359.359,0,0,0-.282-.66L6.733,192.69.5,190.022a.359.359,0,1,0-.282.66Z"
                                        transform="translate(-0.001 -183.146)"
                                        fill="#545454"
                                      />
                                    </g>
                                  </g>
                                </svg>
                                Details
                              </Link>
                            </div>
                          </div>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
