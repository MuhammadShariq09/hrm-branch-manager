import moment from "moment";

export const createDateAsUTC = (date) => {
  return new Date(
    Date.UTC(date?.getFullYear(), date?.getMonth(), date?.getDate(), date?.getHours(), date?.getMinutes(), date?.getSeconds())
  );
};
export const format_date = (date) => moment.utc(date).format("LL");
export const format_date_time = (date) => moment.utc(date).format("MMM DD, YYYY hh:mm:ss A");
export const format_time = (date) => moment.utc(date).format("hh:mm a");
export const format_number = (number) => (number ? number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0);

export const closeModals = () => {
  window?.$(".modal").modal("hide");
  window?.$(".modal-backdrop").remove();
};

export const convertMinsToHrsMins = (mins) => {
  let h = Math.floor(mins / 60);
  let m = mins % 60;
  h = h < 10 ? "0" + h : h; // (or alternatively) h = String(h).padStart(2, '0')
  m = m < 10 ? "0" + m : m; // (or alternatively) m = String(m).padStart(2, '0')
  return `${h}:${m}`;
};

export const format_currency = (number) => {
  const formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  return formatter.format(number);
};

export const getMonths = () => [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

export const getYears = () => {
  const years = Array.from({ length: 100 }, (v, i) => new Date().getFullYear() - 100 + i + 1);
  return years.reverse();
};

export const changeRoute = (history, path) => {
  setTimeout(() => {
    history.push(path);
    closeModals();
  }, 1000);
};
