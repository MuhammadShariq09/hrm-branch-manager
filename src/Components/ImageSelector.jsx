import React, { useState, useEffect } from "react";
import { image_url } from "../Utils/connection_strings";

export default function ImageSelector({ accept, onChange, value }) {
  const [defaultImage, setDefaultImage] = useState(null);

  useEffect(() => {
    setDefaultImage(`${image_url}${value}`);
    if (value && typeof value !== "string") {
      const objectURL = URL.createObjectURL(value);
      setDefaultImage(objectURL);
      return () => URL.revokeObjectURL(objectURL);
    }
  }, [value]);

  return (
    <div class="profile-img mx-auto mx-md-0">
      <img
        className="img-fluid"
        src={value ? defaultImage : `images/profile-img.png`}
      />
      <label for="upload-img" class="camera-btn-2">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="15.517"
          height="13.578"
          viewBox="0 0 15.517 13.578"
        >
          <path
            id="Icon_awesome-camera"
            data-name="Icon awesome-camera"
            d="M15.517,5.644v8.729a1.455,1.455,0,0,1-1.455,1.455H1.455A1.455,1.455,0,0,1,0,14.373V5.644A1.455,1.455,0,0,1,1.455,4.19H4.122l.373-1A1.453,1.453,0,0,1,5.855,2.25h3.8a1.453,1.453,0,0,1,1.361.943l.376,1h2.667A1.455,1.455,0,0,1,15.517,5.644ZM11.4,10.009a3.637,3.637,0,1,0-3.637,3.637A3.64,3.64,0,0,0,11.4,10.009Zm-.97,0A2.667,2.667,0,1,1,7.759,7.342,2.671,2.671,0,0,1,10.426,10.009Z"
            transform="translate(0 -2.25)"
            fill="#666"
          />
        </svg>
      </label>
      <div class="d-none">
        <input
          id="upload-img"
          type="file"
          name=""
          onChange={(event) => onChange(event.target.files[0])}
        />
      </div>
    </div>
  );
}
