import React from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";

export default function Footer() {
  return (
    <div>
      <footer className="py-md-5 py-4 text-center text-sm-left">
        <div className="container">
          <div className="row">
            <div className="col-lg-4 col-sm-6 mt-3">
              <img src="images/footer-logo.png" alt="" className="img-fluid" />
              <p className="mt-3 pr-xl-4">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore
              </p>
              <ul className="d-flex pl-0 align-items-center justify-content-sm-start justify-content-center mt-lg-4">
                <li className="mr-3">
                  <Link to="/">
                    <i className="fab fa-facebook-f" />
                  </Link>
                </li>
                <li className="mr-3">
                  <Link to="/">
                    <i className="fab fa-twitter" />
                  </Link>
                </li>
                <li className="mr-3">
                  <Link to="/">
                    <i className="fab fa-instagram" />
                  </Link>
                </li>
                <li className="mr-3">
                  <Link to="/">
                    <i className="fab fa-linkedin-in" />
                  </Link>
                </li>
              </ul>
            </div>
            <div className="col-lg-2 col-sm-6 mt-3">
              <p className="p-lg ubuntu">For Employers</p>
              <ul className="pl-0">
                <li className="mt-3">
                  <Link to="/dashboard">Employer Dashboard</Link>
                </li>
                <li className="mt-3">
                  <Link to="/">Apply leave</Link>
                </li>
                <li className="mt-3">
                  <Link to="/apply-leave">Training Courses</Link>
                </li>
                <li className="mt-3">
                  <Link to="/profile">Manage Profile</Link>
                </li>
                <li className="mt-3">
                  <Link to="/profile/monthly-reports">Monthly Report</Link>
                </li>
              </ul>
            </div>
            <div className="col-lg-2 col-sm-6 mt-3">
              <p className="p-lg ubuntu">For Managers</p>
              <ul className="pl-0">
                <li className="mt-3">
                  <Link to="/dashboard">Employer Dashboard</Link>
                </li>
                <li className="mt-3">
                  <Link to="/apply-leave">Apply leave</Link>
                </li>
                <li className="mt-3">
                  <Link to="/employee/training-courses">Training Courses</Link>
                </li>
                <li className="mt-3">
                  <Link to="/profile">Manage Profile</Link>
                </li>
                <li className="mt-3">
                  <Link to="/profile/monthly-reports">Monthly Report</Link>
                </li>
              </ul>
            </div>
            <div className="col-lg-4 col-sm-6 mt-3">
              <p className="p-lg ubuntu">Contact Us</p>
              <div className="media mt-3">
                <i className="fas mt-1 fa-phone-alt" />
                <div className="media-body ml-3">
                  <p className="mb-0">(+1) 674 857 86540</p>
                </div>
              </div>
              <div className="media mt-3">
                <i className="fas mt-1 fa-envelope" />
                <div className="media-body ml-3">
                  <p className="mb-0">support@example.com</p>
                </div>
              </div>
              <div className="media mt-3">
                <i className="fas mt-1 fa-map-marker-alt" />
                <div className="media-body ml-3">
                  <p className="mb-0">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy eirmod tempor
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <section className="bottom py-3">
        <div className="container">
          <p className="mb-0">© 2021 HRM. All Rights Reserved.</p>
        </div>
      </section>
    </div>
  );
}
