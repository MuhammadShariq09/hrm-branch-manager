// import React from "react";
import Swal from "sweetalert2";

const Success = (message) =>
  Swal.fire({
    icon: "success",
    title: "SUCCESS",
    text: `${message ? message : "Completed"}`,
    showConfirmButton: false,
    timer: 3000,
  });

export default Success;

// export default function Success({ target, message }) {
//   return (
//     <div
//       className={`modal fade ${target}`}
//       tabIndex={-1}
//       role="dialog"
//       aria-labelledby="exampleModalCenterTitle"
//       aria-hidden="true"
//     >
//       <div className="modal-dialog modal-dialog-centered" role="document">
//         <div className="modal-content site-modal">
//           <i
//             className="close modal-close"
//             data-dismiss="modal"
//             aria-label="Close"
//           >
//             <svg
//               xmlns="http://www.w3.org/2000/svg"
//               width={21}
//               height={21}
//               viewBox="0 0 21 21"
//             >
//               <path
//                 id="Icon_material-close"
//                 data-name="Icon material-close"
//                 d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
//                 transform="translate(-7.5 -7.5)"
//                 fill="#545454"
//               />
//             </svg>
//           </i>
//           <div className="text-center">
//             <img src="images/check.png" alt="" className="img-fluid" />
//             <p className="d-grey-text mt-3 p-lg">{message}</p>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// }
