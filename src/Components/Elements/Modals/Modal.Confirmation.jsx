// import React from "react";
import Swal from "sweetalert2";

export default function Confirmation(
  title,
  confirmButtonText,
  successFunction
) {
  return Swal.fire({
    title,
    showCancelButton: true,
    confirmButtonText,
  }).then((result) => {
    if (result.isConfirmed) {
      successFunction();
    }
  });
}

// export default function Confirmation({
//   text,
//   onSuccess,
//   target,
//   actionAfterSuccess,
// }) {
//   return (
//     <div
//       className={`modal fade ${target}`}
//       tabIndex={-1}
//       role="dialog"
//       aria-labelledby="exampleModalCenterTitle"
//       aria-hidden="true"
//     >
//       <div className="modal-dialog modal-dialog-centered" role="document">
//         <div className="modal-content site-modal">
//           <i
//             className="close modal-close"
//             data-dismiss="modal"
//             aria-label="Close"
//           >
//             <svg
//               xmlns="http://www.w3.org/2000/svg"
//               width={21}
//               height={21}
//               viewBox="0 0 21 21"
//             >
//               <path
//                 id="Icon_material-close"
//                 data-name="Icon material-close"
//                 d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
//                 transform="translate(-7.5 -7.5)"
//                 fill="#545454"
//               />
//             </svg>
//           </i>
//           <div className="text-center">
//             <img src="images/question.png" alt="" className="img-fluid" />
//             <p className="d-grey-text mt-3 p-lg">{text}</p>
//             <div className="d-sm-flex align-items-center justify-content-center">
//               <button
//                 className="site-btn grey-border mr-sm-3 py-2 px-5 mt-1"
//                 data-dismiss="modal"
//                 aria-label="Close"
//                 data-target={`.${actionAfterSuccess}`}
//                 data-toggle="modal"
//                 onClick={onSuccess}
//               >
//                 Yes
//               </button>
//               <button
//                 className="site-btn grey-btn py-2 px-5 mt-1"
//                 data-dismiss="modal"
//                 aria-label="Close"
//               >
//                 No
//               </button>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// }
