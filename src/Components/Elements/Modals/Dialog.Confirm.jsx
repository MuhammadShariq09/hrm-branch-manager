import React from "react";
import Button from "../Form/Button";

export default function ConfirmationDialog({
  target,
  children,
  isClose,
  onSuccess,
  onSuccessTarget,
  loading,
}) {
  return (
    <div>
      <div
        className={`modal fade ${target}`}
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content site-modal">
            {isClose && (
              <i
                className="close modal-close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={21}
                  height={21}
                  viewBox="0 0 21 21"
                >
                  <path
                    id="Icon_material-close"
                    data-name="Icon material-close"
                    d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
                    transform="translate(-7.5 -7.5)"
                    fill="#545454"
                  />
                </svg>
              </i>
            )}
            <div className="text-center">
              <img src="images/question.png" alt="" className="img-fluid" />
              <p className="d-grey-text mt-3 p-lg">{children}</p>
              <div className="d-sm-flex align-items-center justify-content-center">
                {onSuccessTarget ? (
                  <Button
                    className="site-btn grey-border mr-sm-3 py-2 px-5 mt-1"
                    data-toggle="modal"
                    data-target={`.${onSuccessTarget}`}
                    data-dismiss="modal"
                    aria-label="Close"
                    loading={loading}
                    onClick={onSuccess}
                  >
                    Yes
                  </Button>
                ) : (
                  <Button
                    className="site-btn grey-border mr-sm-3 py-2 px-5 mt-1"
                    data-dismiss="modal"
                    aria-label="Close"
                    loading={loading}
                    onClick={onSuccess}
                  >
                    Yes
                  </Button>
                )}
                <button
                  className="site-btn grey-btn py-2 px-5 mt-1"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  No
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
