import React, { useState } from "react";
import { useDebouncedEffect } from "../../../Hooks/useDebouncedEffect";
import Button from "../Form/Button";
import Input from "../Form/Input";

import Loading from "../Icons/Loading";
import Search from "../Icons/Search";

export default function TableFilters({
  type_filter,
  status_filter,
  setSearchString,
  isFetching,
  setType,
  type,
  status,
  setStatus,
}) {
  const [internal_search, setInternalSearch] = useState("");

  useDebouncedEffect(
    () => setSearchString && setSearchString(internal_search),
    [internal_search],
    500
  );

  return (
    <div className="card rounded-10 shadow-none">
      <div className="card-body dataTables_wrapper">
        <div className="page-title">
          <div className="row user-listing-top ">
            <div className="col-12 d-lg-flex d-md-block justify-content-center mt-3 mt-sm-0">
              {type_filter?.length > 0 && (
                <div className="filter-wrap custom-filter mr-1 d-flex align-items-center">
                  <div className="select-wrapper d-block w-100">
                    <select
                      value={type}
                      onChange={(e) => setType(e.target.value)}
                      className="form-control"
                      id
                    >
                      {type_filter?.map((type) => (
                        <option value={type?.value}>{type?.label}</option>
                      ))}
                    </select>
                  </div>
                </div>
              )}
              {status_filter?.length > 0 && (
                <div className="filter-wrap custom-filter mr-1 d-flex align-items-center">
                  <div className="select-wrapper d-block w-100">
                    <select
                      value={status}
                      onChange={(e) => setStatus(e.target.value)}
                      className="form-control"
                      id
                    >
                      {status_filter?.map((type) => (
                        <option value={type?.value}>{type?.label}</option>
                      ))}
                    </select>
                  </div>
                </div>
              )}
              {setSearchString && (
                <div className="filter-wrap custom-filter mr-1 d-flex align-items-center">
                  <div className="input-wrapper d-block w-100">
                    <div className="dataTables_filter">
                      <div className="position-relative">
                        <Input
                          value={internal_search}
                          onChange={(internal_search) =>
                            setInternalSearch(internal_search)
                          }
                          startIcon={isFetching ? <Loading /> : <Search />}
                          placeholder="Search"
                          className="site-input w-100 py-2 px-3"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
