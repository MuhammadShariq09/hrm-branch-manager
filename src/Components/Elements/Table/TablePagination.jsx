import React from "react";

export default function TablePagination({ perPage, setPerPage }) {
  return (
    <div className="user-listing-top">
      <div className="row align-items-end justify-content-end">
        <div className="col-12 col-md-6 col-lg-6 col-xl-6 d-flex d-flex justify-content-end p-0">
          <div className=" filter-wrap custom-filter mr-1 d-flex align-items-center">
            <div className="select-wrapper d-block w-100">
              <select
                value={perPage}
                onChange={(e) => setPerPage(e.target.value)}
                className="form-control"
                id
              >
                <option value={10}>10</option>
                <option value={25}>25</option>
                <option value={50}>50</option>
                <option value={100}>100</option>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
