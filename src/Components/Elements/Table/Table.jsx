import React from "react";
import Pagination from "./Pagination";
import TableFilters from "./TableFilters";
import TableHeader from "./TableHeader";
import TablePagination from "./TablePagination";
import TableBodyHandler from "./TableBodyHandler";

export default function Table({
  headings,
  type_filter,
  status_filter,
  setSearchString,
  isFetching,
  setPerPage,
  perPage,
  setType,
  type,
  status,
  setStatus,
  totalPages,
  setPage,
  data,
  children,
  isLoading,
}) {
  return (
    <section id="configuration" className="user-page">
      <div className="row">
        <div className="col-12">
          <TableFilters
            type_filter={type_filter}
            status_filter={status_filter}
            setSearchString={setSearchString}
            isFetching={isFetching}
            setType={setType}
            type={type}
            status={status}
            setStatus={setStatus}
          />
          <div className="shadow-none">
            <div className="card-body">
              <div className>
                {/* <TablePagination setPerPage={setPerPage} perPage={perPage} /> */}
                <div className="row mt-1">
                  <div className="main-tabble table-responsive">
                    <div className="dataTables_wrapper container-fluid">
                      <div className="row">
                        <div className="col-sm-12">
                          <table className="table table-borderless dataTable">
                            <TableHeader headings={headings} />
                            <TableBodyHandler
                              data={data}
                              length={headings?.length}
                              isLoading={isLoading}
                            />
                            {children}
                          </table>
                        </div>
                      </div>
                      <Pagination totalPages={totalPages} setPage={setPage} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
