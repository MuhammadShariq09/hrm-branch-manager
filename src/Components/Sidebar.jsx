import React from "react";
import { Link } from "react-router-dom";

export default function Sidebar() {
  return (
    <nav class="sidebar" ss-container id="sidebar">
      <div class="text-center">
        <Link to="/dashboard" class="p-0 mt-0">
          <img
            src="images/logo-black.png"
            alt=""
            class="img-fluid d-none d-lg-block"
          />
        </Link>
      </div>
      <ul class="pl-0">
        <li>
          <Link to="/dashboard" class="d-flex align-items-center">
            <svg
              class="mr-3"
              id="dashboard"
              xmlns="http://www.w3.org/2000/svg"
              width="15.667"
              height="15.666"
              viewBox="0 0 15.667 15.666"
            >
              <path
                id="Path_138"
                data-name="Path 138"
                d="M6.038,0h-4.9A1.144,1.144,0,0,0,0,1.142V4.08A1.144,1.144,0,0,0,1.142,5.222h4.9A1.144,1.144,0,0,0,7.181,4.08V1.142A1.144,1.144,0,0,0,6.038,0Zm0,0"
                fill="#545454"
              ></path>
              <path
                id="Path_139"
                data-name="Path 139"
                d="M6.038,213.332h-4.9A1.144,1.144,0,0,0,0,214.474v6.854a1.144,1.144,0,0,0,1.142,1.142h4.9a1.144,1.144,0,0,0,1.142-1.142v-6.854A1.144,1.144,0,0,0,6.038,213.332Zm0,0"
                transform="translate(0 -206.804)"
                fill="#545454"
              ></path>
              <path
                id="Path_140"
                data-name="Path 140"
                d="M283.37,341.332h-4.9a1.144,1.144,0,0,0-1.142,1.142v2.938a1.144,1.144,0,0,0,1.142,1.142h4.9a1.143,1.143,0,0,0,1.142-1.142v-2.937A1.144,1.144,0,0,0,283.37,341.332Zm0,0"
                transform="translate(-268.846 -330.888)"
                fill="#545454"
              ></path>
              <path
                id="Path_141"
                data-name="Path 141"
                d="M283.37,0h-4.9a1.144,1.144,0,0,0-1.142,1.142V8a1.144,1.144,0,0,0,1.142,1.142h4.9A1.144,1.144,0,0,0,284.513,8V1.142A1.144,1.144,0,0,0,283.37,0Zm0,0"
                transform="translate(-268.846)"
                fill="#545454"
              ></path>
            </svg>
            Dashboard
          </Link>
        </li>
        <li>
          <Link to="/employee/logs" class="d-flex align-items-center">
            <svg
              class="mr-3"
              xmlns="http://www.w3.org/2000/svg"
              width="15.666"
              height="10.217"
              viewBox="0 0 15.666 10.217"
            >
              <g
                id="user_2_"
                data-name="user (2)"
                transform="translate(0 -85.333)"
              >
                <g
                  id="Group_1022"
                  data-name="Group 1022"
                  transform="translate(5.449 85.333)"
                >
                  <g
                    id="Group_1021"
                    data-name="Group 1021"
                    transform="translate(0 0)"
                  >
                    <circle
                      id="Ellipse_92"
                      data-name="Ellipse 92"
                      cx="2.384"
                      cy="2.384"
                      r="2.384"
                      fill="#545454"
                    ></circle>
                  </g>
                </g>
                <g
                  id="Group_1024"
                  data-name="Group 1024"
                  transform="translate(10.898 88.058)"
                >
                  <g
                    id="Group_1023"
                    data-name="Group 1023"
                    transform="translate(0 0)"
                  >
                    <circle
                      id="Ellipse_93"
                      data-name="Ellipse 93"
                      cx="1.703"
                      cy="1.703"
                      r="1.703"
                      fill="#545454"
                    ></circle>
                  </g>
                </g>
                <g
                  id="Group_1026"
                  data-name="Group 1026"
                  transform="translate(1.411 88.058)"
                >
                  <g
                    id="Group_1025"
                    data-name="Group 1025"
                    transform="translate(0 0)"
                  >
                    <circle
                      id="Ellipse_94"
                      data-name="Ellipse 94"
                      cx="1.703"
                      cy="1.703"
                      r="1.703"
                      fill="#545454"
                    ></circle>
                  </g>
                </g>
                <g
                  id="Group_1028"
                  data-name="Group 1028"
                  transform="translate(3.406 90.782)"
                >
                  <g
                    id="Group_1027"
                    data-name="Group 1027"
                    transform="translate(0 0)"
                  >
                    <path
                      id="Path_4843"
                      data-name="Path 4843"
                      d="M111.093,256a4.432,4.432,0,0,0-4.427,4.427.341.341,0,0,0,.341.341h8.174a.341.341,0,0,0,.341-.341A4.432,4.432,0,0,0,111.093,256Z"
                      transform="translate(-106.666 -256)"
                      fill="#545454"
                    ></path>
                  </g>
                </g>
                <g
                  id="Group_1030"
                  data-name="Group 1030"
                  transform="translate(0 92.144)"
                >
                  <g
                    id="Group_1029"
                    data-name="Group 1029"
                    transform="translate(0 0)"
                  >
                    <path
                      id="Path_4844"
                      data-name="Path 4844"
                      d="M3.7,298.741a3.054,3.054,0,0,0-.635-.074A3.069,3.069,0,0,0,0,301.732a.341.341,0,0,0,.341.341H2.787a1.01,1.01,0,0,1-.063-.341A5.076,5.076,0,0,1,3.7,298.741Z"
                      transform="translate(0 -298.667)"
                      fill="#545454"
                    ></path>
                  </g>
                </g>
                <g
                  id="Group_1032"
                  data-name="Group 1032"
                  transform="translate(11.965 92.144)"
                >
                  <g
                    id="Group_1031"
                    data-name="Group 1031"
                    transform="translate(0 0)"
                  >
                    <path
                      id="Path_4845"
                      data-name="Path 4845"
                      d="M375.4,298.667a3.055,3.055,0,0,0-.635.074,5.077,5.077,0,0,1,.976,2.992,1.01,1.01,0,0,1-.063.341h2.447a.341.341,0,0,0,.341-.341A3.069,3.069,0,0,0,375.4,298.667Z"
                      transform="translate(-374.763 -298.667)"
                      fill="#545454"
                    ></path>
                  </g>
                </g>
              </g>
            </svg>
            Employees
          </Link>
        </li>
        <li>
          <Link to="/leave/logs" class="d-flex align-items-center">
            <svg
              class="mr-3"
              xmlns="http://www.w3.org/2000/svg"
              width="12.402"
              height="15.666"
              viewBox="0 0 12.402 15.666"
            >
              <g id="notes" transform="translate(-2)">
                <path
                  id="Path_4846"
                  data-name="Path 4846"
                  d="M6.4,3.59a.653.653,0,0,1-.653-.653V.653a.653.653,0,1,1,1.306,0V2.937A.653.653,0,0,1,6.4,3.59Z"
                  transform="translate(-1.302)"
                  fill="#545454"
                ></path>
                <path
                  id="Path_4847"
                  data-name="Path 4847"
                  d="M11.153,3.59a.653.653,0,0,1-.653-.653V.653a.653.653,0,1,1,1.306,0V2.937A.653.653,0,0,1,11.153,3.59Z"
                  transform="translate(-2.952)"
                  fill="#545454"
                ></path>
                <path
                  id="Path_4848"
                  data-name="Path 4848"
                  d="M15.9,3.59a.653.653,0,0,1-.653-.653V.653a.653.653,0,1,1,1.306,0V2.937A.653.653,0,0,1,15.9,3.59Z"
                  transform="translate(-4.601)"
                  fill="#545454"
                ></path>
                <path
                  id="Path_4849"
                  data-name="Path 4849"
                  d="M12.607,2.25H3.8A1.8,1.8,0,0,0,2,4.045V14.652a1.8,1.8,0,0,0,1.8,1.8h8.812a1.8,1.8,0,0,0,1.8-1.8V4.045A1.8,1.8,0,0,0,12.607,2.25ZM5.264,6H8.528a.653.653,0,0,1,0,1.306H5.264A.653.653,0,0,1,5.264,6Zm6.528,6.528H5.264a.653.653,0,0,1,0-1.306h6.528a.653.653,0,1,1,0,1.306Zm0-2.611H5.264a.653.653,0,0,1,0-1.306h6.528a.653.653,0,1,1,0,1.306Z"
                  transform="translate(0 -0.781)"
                  fill="#545454"
                ></path>
              </g>
            </svg>
            Leaves
          </Link>
        </li>
        <li>
          <Link to="/payroll/logs" class="d-flex align-items-center">
            <svg
              class="mr-3"
              xmlns="http://www.w3.org/2000/svg"
              width="15.667"
              height="16.171"
              viewBox="0 0 15.667 16.171"
            >
              <g id="bill" transform="translate(-7.957 0)">
                <path
                  id="Path_4015"
                  data-name="Path 4015"
                  d="M83.339,9.095V2a2,2,0,0,0-2-2H71.727a2,2,0,0,1,1.022,1.744V13.7a.952.952,0,0,0,.951.951h4.773a4.1,4.1,0,0,1,4.867-5.554ZM77.1,6.9a.476.476,0,0,1,.653-.161,1.461,1.461,0,0,0,.989.24.786.786,0,0,0,.818-.633c.026-.168,0-.471-.448-.619a5.256,5.256,0,0,1-1.46-.649,1.376,1.376,0,0,1,.617-2.384V2.51a.476.476,0,1,1,.951,0v.152a1.831,1.831,0,0,1,.787.354.475.475,0,1,1-.608.73h0l0,0c-.337-.264-1.177-.286-1.276.26a.3.3,0,0,0,.084.3,4.9,4.9,0,0,0,1.2.515,1.571,1.571,0,0,1-.187,3.047v.218a.476.476,0,1,1-.951,0V7.91a2.234,2.234,0,0,1-1.009-.36A.476.476,0,0,1,77.1,6.9ZM74.208,3.044h1.015a.476.476,0,0,1,0,.951H74.208a.476.476,0,0,1,0-.951Zm0,1.776h1.015a.476.476,0,0,1,0,.951H74.208a.476.476,0,0,1,0-.951Zm0,2.029h1.015a.476.476,0,0,1,0,.951H74.208a.476.476,0,0,1,0-.951ZM77,11.859h-2.79a.476.476,0,0,1,0-.951H77a.476.476,0,1,1,0,.951Zm0-2.029h-2.79a.476.476,0,0,1,0-.951H77a.476.476,0,1,1,0,.951Z"
                  transform="translate(-61.748)"
                  fill="#545454"
                ></path>
                <path
                  id="Path_4016"
                  data-name="Path 4016"
                  d="M309.913,313.756l-2.354,2.354a.476.476,0,0,1-.673,0l-.761-.761a.476.476,0,0,1,.507-.781c.125.046.142.084.591.533l2.136-2.135a3.137,3.137,0,1,0,.555.79Z"
                  transform="translate(-286.614 -302.107)"
                  fill="#545454"
                ></path>
                <path
                  id="Path_4017"
                  data-name="Path 4017"
                  d="M9,22.16a1.046,1.046,0,0,0-1.046,1.046v2.056a.951.951,0,0,0,.951.951H10.05V23.206A1.046,1.046,0,0,0,9,22.16Z"
                  transform="translate(0 -21.457)"
                  fill="#545454"
                ></path>
              </g>
            </svg>
            Payroll
          </Link>
        </li>
        <li>
          <Link to="/profile" class="d-flex align-items-center">
            <svg
              class="mr-3"
              xmlns="http://www.w3.org/2000/svg"
              width="14.667"
              height="18.578"
              viewBox="0 0 14.667 18.578"
            >
              <g id="avatar" transform="translate(-53.895)">
                <g
                  id="Group_894"
                  data-name="Group 894"
                  transform="translate(57.073 0)"
                >
                  <g id="Group_893" data-name="Group 893">
                    <circle
                      id="Ellipse_80"
                      data-name="Ellipse 80"
                      cx="4.155"
                      cy="4.155"
                      r="4.155"
                      fill="#545454"
                    ></circle>
                  </g>
                </g>
                <g
                  id="Group_896"
                  data-name="Group 896"
                  transform="translate(53.895 9.289)"
                >
                  <g id="Group_895" data-name="Group 895">
                    <path
                      id="Path_4022"
                      data-name="Path 4022"
                      d="M61.228,256a7.333,7.333,0,0,0-7.333,7.333,1.956,1.956,0,0,0,1.956,1.956H66.606a1.956,1.956,0,0,0,1.956-1.956A7.333,7.333,0,0,0,61.228,256Z"
                      transform="translate(-53.895 -256)"
                      fill="#545454"
                    ></path>
                  </g>
                </g>
              </g>
            </svg>
            Profile
          </Link>
        </li>
        <li>
          <Link
            to=""
            onClick={() => {
              localStorage.clear();
              window.location.reload();
            }}
            class="d-flex align-items-center"
          >
            <svg
              class="mr-3"
              id="log-out"
              xmlns="http://www.w3.org/2000/svg"
              width="15.159"
              height="15.159"
              viewBox="0 0 15.159 15.159"
            >
              <path
                id="Path_4829"
                data-name="Path 4829"
                d="M9.474,8.211a.631.631,0,0,0-.632.632v2.527A.632.632,0,0,1,8.211,12H6.316V2.527a1.273,1.273,0,0,0-.86-1.2l-.187-.063H8.211a.632.632,0,0,1,.632.632V3.79a.632.632,0,1,0,1.263,0V1.895A1.9,1.9,0,0,0,8.211,0H1.421a.493.493,0,0,0-.068.014C1.323.011,1.294,0,1.263,0A1.265,1.265,0,0,0,0,1.263V12.633a1.273,1.273,0,0,0,.86,1.2l3.8,1.267a1.308,1.308,0,0,0,.392.059A1.265,1.265,0,0,0,6.316,13.9v-.632H8.211a1.9,1.9,0,0,0,1.895-1.895V8.843a.631.631,0,0,0-.632-.632Z"
                transform="translate(0)"
                fill="#545454"
              ></path>
              <path
                id="Path_4830"
                data-name="Path 4830"
                d="M19.763,7.712,17.236,5.185a.631.631,0,0,0-1.078.447V7.526H13.632a.632.632,0,1,0,0,1.263h2.527v1.895a.631.631,0,0,0,1.078.447L19.763,8.6a.631.631,0,0,0,0-.893Z"
                transform="translate(-4.789 -1.842)"
                fill="#545454"
              ></path>
            </svg>
            Logout
          </Link>
        </li>
      </ul>
    </nav>
  );
}
