import React, { useState } from "react";

import Calender from "./Elements/Form/Calender";

export default function MonthlyReport() {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [search_string, setSearchString] = useState("");
  const [filter, setFilter] = useState("");
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [open1, setOpen1] = useState(false);
  const [open2, setOpen2] = useState(false);

  return (
    <div className="main">
      <div className="main-body">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 px-0 px-lg-2">
              <div className="white-card mt-3 px-2 py-3 px-sm-3">
                <div className="d-sm-flex align-items-center justify-content-between">
                  <h4 className="mt-2 mb-0">Monthly Report</h4>
                  <button className="site-btn grey-btn px-3 py-2 mt-sm-2 mt-1">
                    Download PDF
                  </button>
                </div>
                <div className="row">
                  <div className="col-xl-4 col-lg-6 col-md-8 col-12 mt-3">
                    <label htmlFor>Select Month</label>
                    <Calender
                      className="site-input border-0 py-2 px-3 w-100"
                      open={open1}
                      value={from}
                      maxDate={to || new Date()}
                      onChange={(date) => {
                        console.log(date);
                      }}
                      onInputClick={() => {
                        setOpen1(!open1);
                      }}
                      onClickOutside={() => setOpen1(false)}
                    />
                  </div>
                </div>
                <div className="orange-bg mt-3 p-4">
                  <div className="text-center">
                    <img
                      src="images/monthly-report.png"
                      alt=""
                      className="img-fluid mx-auto report-img"
                    />
                    <h5 className="mt-2">Company abc</h5>
                    <p className="mb-2 d-grey-text">Tel: +01234567890</p>
                    <p className="mb-2 d-grey-text">email@example.com</p>
                    <p className="mb-2 d-grey-text">
                      Address abc, Address Line 1
                    </p>
                    <h5 className="mt-2">
                      Monthly Report for the period of February 2021
                    </h5>
                  </div>
                  <div className="row">
                    <div className="col-xl-10 col-lg-11 col-12 mx-auto mt-lg-2">
                      <div className="row">
                        <div className="col-lg-6 mt-3">
                          <div className="row">
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Employee ID:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">001</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                First Name:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">abc</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Last Name:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">xyz</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">Branch:</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">Branch A</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">Shift:</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">
                                Morning (9 to 5 PM)
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Leaves Remaining:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">22</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Total Days:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">28</p>
                            </div>
                            <div className="col-sm-6">
                              <p className="p-md mb-2 ubuntu medium">
                                Employee Status:
                              </p>
                            </div>
                            <div className="col-sm-6">
                              <p className="d-grey-text mb-2">Full-Time</p>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-6 mt-3">
                          <div className="row">
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Days Worked:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">22</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Absent Days:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">22</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">Leaves:</p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">0</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Total Required Working Hours:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">198</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Hours Worked:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">198</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Hours Remaining:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">0</p>
                            </div>
                            <div className="col-sm-9">
                              <p className="p-md mb-2 ubuntu medium">
                                Average Working Hours:
                              </p>
                            </div>
                            <div className="col-sm-3">
                              <p className="d-grey-text mb-2">9</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
