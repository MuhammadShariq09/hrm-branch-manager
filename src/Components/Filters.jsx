import React, { useState } from "react";

import Loading from "./Elements/Icons/Loading";
import Calender from "./Elements/Form/Calender";
import { useDebouncedEffect } from "../Hooks/useDebouncedEffect";

export default function Filters({
  perPage,
  setPerPage,
  statusOptions,
  status,
  setStatus,
  dateFilters,
  dateTo,
  setDateTo,
  dateFrom,
  setDateFrom,
  setSearchString,
  isFetching,
}) {
  const [internal_search, setInternalSearch] = useState("");
  const [open1, setOpen1] = useState(false);
  const [open2, setOpen2] = useState(false);

  useDebouncedEffect(
    () => setSearchString && setSearchString(internal_search),
    [internal_search],
    500
  );

  return (
    <>
      <div>
        <label htmlFor className="d-grey-text ubuntu d-block p-sm">
          Show Entries:
        </label>
        <select
          name
          id
          className="site-input w-100 border-0 px-3 py-2"
          value={perPage}
          onChange={(e) => setPerPage(e.target.value)}
        >
          <option value={10}>10</option>
          <option value={25}>25</option>
          <option value={50}>50</option>
          <option value={100}>100</option>
        </select>
      </div>
      {setStatus && (
        <div className="ml-md-3">
          <label htmlFor className="d-grey-text ubuntu d-block p-sm">
            Filter By Status:
          </label>
          <select
            name
            id
            className="site-input w-150 border-0 px-3 py-2"
            value={status}
            onChange={(e) => setStatus(e.target.value)}
          >
            <option value="">Select</option>
            {statusOptions.map((el, i) => (
              <option key={i} value={el.value}>
                {el.label}
              </option>
            ))}
          </select>
        </div>
      )}
      {dateFilters && (
        <div className="ml-md-3">
          <label htmlFor className="d-grey-text ubuntu p-sm">
            Sort By Date:
          </label>
          <div className="d-sm-flex">
            {setDateFrom && (
              <Calender
                className="site-input border-0 py-2 px-3 w-100"
                value={dateFrom}
                maxDate={dateTo}
                open={open1}
                onChange={(date) => {
                  setDateFrom(date);
                }}
                onInputClick={() => {
                  setOpen1(!open1);
                }}
                onClickOutside={() => setOpen1(false)}
              />
            )}
            <span style={{ width: 20 }} />
            {setDateTo && (
              <Calender
                className="site-input border-0 py-2 px-3 w-100"
                value={dateTo}
                minDate={dateFrom}
                open={open1}
                onChange={(date) => {
                  setDateTo(date);
                }}
                onInputClick={() => {
                  setOpen1(!open1);
                }}
                onClickOutside={() => setOpen1(false)}
              />
            )}
          </div>
        </div>
      )}
      <div className="ml-md-3">
        <label htmlFor className="d-grey-text ubuntu p-sm">
          Search Here
        </label>
        <div className="search-bar">
          <input
            type="text"
            placeholder="Search...."
            className="site-input border-0 w-100 py-2 px-3"
            value={internal_search}
            onChange={(e) => setInternalSearch(e.target.value)}
          />
          <button className="transparent-btn">
            {isFetching ? (
              <Loading />
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20.02"
                height="20.02"
                viewBox="0 0 20.02 20.02"
              >
                <g
                  id="Icon_feather-search"
                  data-name="Icon feather-search"
                  transform="translate(1 1)"
                >
                  <path
                    id="Path_3557"
                    data-name="Path 3557"
                    d="M20.149,12.325A7.825,7.825,0,1,1,12.325,4.5,7.825,7.825,0,0,1,20.149,12.325Z"
                    transform="translate(-4.5 -4.5)"
                    fill="none"
                    stroke="#666"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                  />
                  <path
                    id="Path_3558"
                    data-name="Path 3558"
                    d="M29.23,29.23l-4.255-4.255"
                    transform="translate(-11.624 -11.624)"
                    fill="none"
                    stroke="#666"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                  />
                </g>
              </svg>
            )}
          </button>
        </div>
      </div>
    </>
  );
}
