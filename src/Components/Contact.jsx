import React, { useState } from "react";
import { useMutation } from "react-query";

import Dialog from "./Elements/Modals/Dialog";
import Input from "./Elements/Form/Input";
import Button from "./Elements/Form/Button";

import { submitQuery } from "../Apis";

export default function Contact({ hideModal }) {
  const [state, setState] = useState({});
  const { mutate, isLoading } = useMutation((data) => submitQuery(data), {
    onSuccess: (res) => {
      console.log("debug", "success");
    },
    onError: (err) => {
      console.log("debug", "error", err?.response?.data?.message);
    },
  });

  const handleSubmit = () => {
    mutate(state);
  };

  return (
    <Dialog target="contactUs">
      <h3 className="medium text-center">Contact Us Form</h3>
      <div className="text-left">
        <form action>
          <label htmlFor className="p-lg ubuntu">
            Enter Name
          </label>
          <Input
            placeholder="Enter Name"
            className="site-input px-3 py-2 w-100"
            value={state?.name}
            onChange={(name) => setState({ ...state, name })}
          />
          <label htmlFor className="p-lg mt-3 ubuntu">
            Enter Email
          </label>
          <Input
            placeholder="Enter Email Address"
            className="site-input px-3 py-2 w-100"
            value={state?.email}
            onChange={(email) => setState({ ...state, email })}
          />
          <label htmlFor className="p-lg mt-3 ubuntu">
            Enter Message
          </label>
          <textarea
            name
            placeholder="Enter Message"
            id
            cols={30}
            rows={5}
            className="site-input py-2 px-3 w-100"
            defaultValue={""}
            onChange={(e) => setState({ ...state, message: e.target.value })}
          />
          <div className="text-center">
            <Button
              data-target=".formSubmitted"
              data-toggle="modal"
              data-dismiss="modal"
              aria-label="Close"
              loading={isLoading}
              onClick={handleSubmit}
              className="site-btn grey-btn py-2 w-100 mt-lg-5 mt-4"
            >
              Contact Us
            </Button>
          </div>
        </form>
      </div>
    </Dialog>
  );
}
