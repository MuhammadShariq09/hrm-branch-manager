import React, { useState, Fragment } from "react";
import { Link } from "react-router-dom";
import { useQuery, useMutation } from "react-query";
import { useRecoilValue } from "recoil";
import { userState } from "../../Recoil";
import { getNotificationsCount, getNotificationsMenu, postNotificationRead } from "../../Apis";
import moment from "moment";

export default function NotificationsMenu() {
  const user = useRecoilValue(userState);

  const { data } = useQuery(["notifications_menu", user?.branch], () => getNotificationsMenu(user?.branch), {
    keepPreviousData: true,
  });

  const { data: _data } = useQuery(["notifications_count", user?.branch], () => getNotificationsCount(), {});

  const { mutate, isLoading: postNotificationReadLoading } = useMutation((id, branchId) => postNotificationRead(id, branchId), {
    onSuccess: (res) => {},
    onError: (err) => Error(err?.response?.data?.message),
  });

  return (
    <div className="dropdown dropdown-notification two-bell-icons">
      <a className="nav-link nav-link-label" href="#" data-toggle="dropdown">
        <svg xmlns="http://www.w3.org/2000/svg" width="34.667" height="40.909" viewBox="0 0 34.667 40.909">
          <g id="Group_218" data-name="Group 218" transform="translate(-1556.583 -56)">
            <g id="notification" transform="translate(1556.584 59.091)">
              <path
                id="Path_3517"
                data-name="Path 3517"
                d="M140.754,452.727A5.918,5.918,0,0,0,146.544,448H134.965a5.919,5.919,0,0,0,5.789,4.727Zm0,0"
                transform="translate(-124.997 -414.909)"
                fill="#29313f"
              />
              <path
                id="Path_3518"
                data-name="Path 3518"
                d="M26.806,58.423h-.019a11.043,11.043,0,0,1-11.03-11.03,10.932,10.932,0,0,1,1.054-4.674c-.347-.033-.7-.053-1.054-.053A11.03,11.03,0,0,0,4.727,53.7v4.393A10.566,10.566,0,0,1,.964,66.179,2.757,2.757,0,0,0,.1,69.009a2.891,2.891,0,0,0,2.833,2.022H28.576a2.889,2.889,0,0,0,2.88-2.186,2.762,2.762,0,0,0-.941-2.7,10.485,10.485,0,0,1-3.709-7.724Zm0,0"
                transform="translate(0 -39.516)"
                fill="#29313f"
              />
              <path
                id="Path_3519"
                data-name="Path 3519"
                d="M271.769,7.879A7.879,7.879,0,1,1,263.89,0,7.878,7.878,0,0,1,271.769,7.879Zm0,0"
                transform="translate(-237.103)"
                fill="#29313f"
              />
            </g>
            {_data?.data?.count > 0 && (
              <text
                id="_2"
                data-name={2}
                transform="translate(1579 72)"
                fill="#fff"
                fontSize={16}
                fontFamily="Nunito-SemiBold, Nunito"
                fontWeight={600}
              >
                <tspan x={0} y={0}>
                  {_data?.data.count}
                </tspan>
              </text>
            )}
          </g>
        </svg>
      </a>
      <ul className="dropdown-menu dropdown-menu-media dropdown-menu-right">
        <li className="dropdown-menu-header d-flex align-items-center justify-content-between">
          <h6 className="p-md bold m-0">
            <span className="grey darken-2">Notifications</span>
          </h6>
          <Link to="/notifications" className="orange-text">
            View All
          </Link>
        </li>
        <li
          className="scrollable-container media-list ps-container ps-theme-dark ps-active-y"
          data-ps-id="75e644f2-605d-3ecc-f100-72d86e4a64d8"
        >
          <hr />
          {data?.data?.notifications?.length > 0 ? (
            data?.data?.notifications?.map((el, i) => (
              <Fragment key={i}>
                <div
                  className="media"
                  onClick={!postNotificationReadLoading && !Boolean(el?.read) ? () => mutate(el?._id, user?.branch) : null}
                >
                  <div className="media-left mr-2">
                    {Boolean(el?.read) ? (
                      <i className="d-blue-text fas fa-bell notifications-bell" />
                    ) : (
                      <i className="fas fa-bell orange-text notifications-bell" />
                    )}
                  </div>
                  <div className="media-body">
                    {/* <h6 class="media-heading">You have new notification!</h6> */}
                    <p className="notification-text mb-0 p-sm font-small-3 text-muted">{el?.message}</p>
                    <div className="text-right">
                      <small>
                        <time className="media-meta text-muted" dateTime="2015-06-11T18:29:20+08:00">
                          {moment(el?.createdAt).startOf("minutes").fromNow()}
                          {/* {moment(el?.createdAt).format(
                            "DD/MM/YYYY hh:mm:ss A"
                          )} */}
                        </time>
                      </small>
                    </div>
                  </div>
                </div>
                <hr />
              </Fragment>
            ))
          ) : (
            <p>No Notifications</p>
          )}
        </li>
      </ul>
    </div>
  );
}
