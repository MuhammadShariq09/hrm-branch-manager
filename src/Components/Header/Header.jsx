import React from "react";
import { Link } from "react-router-dom";
import AccountMenu from "./AccountMenu";
import NotificationsMenu from "./NotificationsMenu";

export default function Header() {

  return (
    <div className="main-body">
      <nav className="navbar navbar-expand-lg navbar-light navbar-lg  bg-me1">
        <Link to="/dashboard" className="sidebar-toggle">
          <i className="fas fa-bars" />
        </Link>
        <Link to="" className="p-0 mt-0">
          <img
            src="images/logo-black.png"
            alt=""
            className="img-fluid d-lg-none"
          />
        </Link>
        <button
          className="navbar-toggler border-0 sidebar-toggle"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav1"
          aria-controls="navbarNav1"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <i className="fas fa-ellipsis-v" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav1">
          <ul className="navbar-nav align-items-lg-center ml-auto">
            <li className="nav-item">
              <NotificationsMenu />
            </li>
            <li className="nav-item">
             <AccountMenu />
            </li>
            <li className="nav-item active">
              <a
                className="nav-link mb-3 mb-lg-0 mt-lg-0 d-inline-block"
                href="a-profile.php"
              ></a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
}
