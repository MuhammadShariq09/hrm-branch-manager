import React from "react";
import { Link } from "react-router-dom";

import { useRecoilValue } from "recoil";
import { userState } from "../../Recoil";
import { image_url } from "../../Utils/connection_strings";

export default function AccountMenu() {
  const user = useRecoilValue(userState);

  return (
    <div className="dropdown">
      <a className="nav-link nav-link-label" href="#" data-toggle="dropdown">
        <div className="media align-items-center">
          <img
            src={
              user?.user_image
                ? `${image_url}${user?.user_image}`
                : `images/avatar.png`
            }
            alt=""
            className="img-fluid"
            style={{
              height: 59,
              width: 59,
            }}
          />
          <div className="media-body ml-2">
            <p className="p-md black-text mb-0 ubuntu bold">{user?.first_name}</p>
            <p className="mb-0 black-text">Branch Manager</p>
          </div>
        </div>
      </a>
      <ul className="dropdown-menu px-3 dropdown-menu-media dropdown-menu-right">
        <Link to="/profile" className="d-block">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="10.8"
            height="13.68"
            viewBox="0 0 10.8 13.68"
          >
            <g id="avatar" transform="translate(-53.895)">
              <g
                id="Group_173"
                data-name="Group 173"
                transform="translate(56.235 0)"
              >
                <g id="Group_172" data-name="Group 172">
                  <circle
                    id="Ellipse_26"
                    data-name="Ellipse 26"
                    cx="3.06"
                    cy="3.06"
                    r="3.06"
                    fill="#666"
                  />
                </g>
              </g>
              <g
                id="Group_175"
                data-name="Group 175"
                transform="translate(53.895 6.84)"
              >
                <g id="Group_174" data-name="Group 174">
                  <path
                    id="Path_3544"
                    data-name="Path 3544"
                    d="M59.3,256a5.4,5.4,0,0,0-5.4,5.4,1.44,1.44,0,0,0,1.44,1.44h7.92a1.44,1.44,0,0,0,1.44-1.44A5.4,5.4,0,0,0,59.3,256Z"
                    transform="translate(-53.895 -256)"
                    fill="#666"
                  />
                </g>
              </g>
            </g>
          </svg>
          Profile
        </Link>
        <hr className="my-2" />
        <Link
          to="#"
          className="d-block"
          onClick={() => {
            localStorage.clear();
            window.location.reload();
          }}
        >
          <svg
            id="log-out"
            xmlns="http://www.w3.org/2000/svg"
            width="10.188"
            height="10.188"
            viewBox="0 0 10.188 10.188"
          >
            <path
              id="Path_4829"
              data-name="Path 4829"
              d="M6.367,5.518a.424.424,0,0,0-.424.424v1.7a.425.425,0,0,1-.424.424H4.245V1.7A.856.856,0,0,0,3.667.891L3.541.849H5.518a.425.425,0,0,1,.424.424V2.547a.424.424,0,1,0,.849,0V1.273A1.275,1.275,0,0,0,5.518,0H.955A.332.332,0,0,0,.91.009C.889.008.87,0,.849,0A.85.85,0,0,0,0,.849V8.49A.856.856,0,0,0,.578,9.3l2.555.852a.879.879,0,0,0,.263.039.85.85,0,0,0,.849-.849V8.914H5.518A1.275,1.275,0,0,0,6.792,7.641v-1.7a.424.424,0,0,0-.424-.424Z"
              transform="translate(0)"
              fill="#545454"
            />
            <path
              id="Path_4830"
              data-name="Path 4830"
              d="M17.545,6.822l-1.7-1.7a.424.424,0,0,0-.725.3V6.7h-1.7a.424.424,0,1,0,0,.849h1.7V8.82a.424.424,0,0,0,.725.3l1.7-1.7a.424.424,0,0,0,0-.6Z"
              transform="translate(-7.482 -2.877)"
              fill="#545454"
            />
          </svg>
          Logout
        </Link>
      </ul>
    </div>
  );
}
