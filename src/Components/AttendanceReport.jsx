import React, { useState } from "react";

export default function AttendanceReport({ history }) {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [search_string, setSearchString] = useState("");

  //   const { isFetching, isLoading, data, refetch } = useQuery(
  //     ["leave_logs", page, perPage, search_string],
  //     () => getLeaveLogs(page, perPage, search_string),
  //     { keepPreviousData: true }
  //   );

  //   // Prefetch the next page!
  //   useEffect(() => {
  //     if (data?.data?.logs?.hasNextPage) {
  //       queryClient.prefetchQuery(
  //         ["leave_logs", page + 1, perPage, search_string],
  //         () => getLeaveLogs(page + 1, perPage, search_string)
  //       );
  //     }
  //   }, [data, page, queryClient]);

  //   const { mutate, isLoading: loadingLeaveStatus } = useMutation(
  //     (data) => changeLeaveStatus(data),
  //     {
  //       onSuccess: (res) => {
  //         refetch();
  //         Success(res?.data?.message);
  //       },
  //       onError: (err) => Error(err?.response?.data?.message),
  //     }
  //   );

  return (
    <div>
      <div className="main">
        <div className="main-body">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12 px-0 px-lg-2">
                <div className="white-card mt-3 px-2 py-3 px-sm-3">
                  <div className="d-sm-flex align-items-center justify-content-between">
                    <div>
                      <h4 className="mt-3 mb-0">Attendance Reports</h4>
                      <p className="p-md d-grey-text mb-0">
                        Shift: Morning (9 AM to 5 PM)
                      </p>
                    </div>
                    <button className="site-btn grey-btn px-3 py-2 mt-sm-3 mt-1">
                      Download Excel
                    </button>
                  </div>
                  <div className="orange-bg d-md-flex align-items-center mt-3 p-3">
                    <div className="w-190">
                      <label htmlFor className="d-grey-text ubuntu p-sm">
                        Select month
                      </label>
                      <input
                        type="date"
                        name
                        id
                        className="site-input border-0 py-2 px-3 w-100"
                      />
                    </div>
                    <div className="ml-md-3">
                      <label
                        htmlFor
                        className="d-grey-text mt-md-0 mt-3 ubuntu p-sm"
                      >
                        Filter by leave
                      </label>
                      <div className="white-card d-sm-flex align-items-center py-2 px-3">
                        <p className="my-0">
                          <input type="checkbox" id="all" name="cb" />
                          <label
                            htmlFor="all"
                            className="d-flex align-items-center"
                          >
                            All
                          </label>
                        </p>
                        <p className="my-0 ml-sm-3">
                          <input type="checkbox" id="no-leave" name="cb" />
                          <label
                            htmlFor="no-leave"
                            className="d-flex align-items-center"
                          >
                            No Leave
                          </label>
                        </p>
                        <p className="my-0 ml-sm-3">
                          <input type="checkbox" id="apply-leave" name="cb" />
                          <label
                            htmlFor="apply-leave"
                            className="d-flex align-items-center"
                          >
                            Apply Leave
                          </label>
                        </p>
                        <p className="my-0 ml-sm-3">
                          <input type="checkbox" id="leave-applied" name="cb" />
                          <label
                            htmlFor="leave-applied"
                            className="d-flex align-items-center"
                          >
                            Leave Applied
                          </label>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="d-md-flex align-items-center justify-content-between mt-3">
                    <h5 className="mb-md-0">
                      Attendance Report - (April 2021)
                    </h5>
                    <div className="search-bar">
                      <input
                        type="text"
                        placeholder="Search...."
                        className="site-input w-100 py-2 px-3"
                      />
                      <button className="transparent-btn">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="20.02"
                          height="20.02"
                          viewBox="0 0 20.02 20.02"
                        >
                          <g
                            id="Icon_feather-search"
                            data-name="Icon feather-search"
                            transform="translate(1 1)"
                          >
                            <path
                              id="Path_3557"
                              data-name="Path 3557"
                              d="M20.149,12.325A7.825,7.825,0,1,1,12.325,4.5,7.825,7.825,0,0,1,20.149,12.325Z"
                              transform="translate(-4.5 -4.5)"
                              fill="none"
                              stroke="#666"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth={2}
                            />
                            <path
                              id="Path_3558"
                              data-name="Path 3558"
                              d="M29.23,29.23l-4.255-4.255"
                              transform="translate(-11.624 -11.624)"
                              fill="none"
                              stroke="#666"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth={2}
                            />
                          </g>
                        </svg>
                      </button>
                    </div>
                  </div>
                </div>
                <div className="maain-tabble table-responsive">
                  <table className="table text-left table-striped table-bordered zero-configuration">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Date</th>
                        <th>Day</th>
                        <th>Time In</th>
                        <th>Time Out</th>
                        <th>Hours</th>
                        <th>Leave</th>
                        <th>Reason</th>
                        <th className="text-center">Status</th>
                      </tr>
                    </thead>
                    <tbody className="orange-bg p-3">
                      <tr>
                        <td>01</td>
                        <td>mm/dd/yyyy</td>
                        <td>Monday</td>
                        <td>0:00PM</td>
                        <td>0:00PM</td>
                        <td>9</td>
                        <td>
                          <a
                            href="#_"
                            className="d-blue-text"
                            data-target=".leaveForm"
                            data-toggle="modal"
                          >
                            Apply Leave
                          </a>
                        </td>
                        <td>-</td>
                        <td className="text-center">
                          <p className="pending mb-0 d-inline-block py-2 px-5">
                            Pending
                          </p>
                        </td>
                      </tr>
                      <tr>
                        <td>02</td>
                        <td>mm/dd/yyyy</td>
                        <td>Tuesday</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>Leave Applied</td>
                        <td>Lorem Ipsum</td>
                        <td className="text-center">
                          <p className="approved mb-0 d-inline-block py-2 px-5">
                            Approved
                          </p>
                        </td>
                      </tr>
                      <tr>
                        <td>03</td>
                        <td>mm/dd/yyyy</td>
                        <td>Wednesday</td>
                        <td>0:00PM</td>
                        <td>0:00PM</td>
                        <td>9</td>
                        <td>Leave Applied</td>
                        <td>Lorem Ipsum</td>
                        <td className="text-center">
                          <p className="rejected mb-0 d-inline-block py-2 px-5">
                            Rejected
                          </p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="white-card px-2 py-3 px-sm-3">
                  <div className="d-md-flex align-items-center justify-content-between">
                    <p className="p-sm mb-md-0 mt-0">
                      Showing 1 to 20 of 52 entries
                    </p>
                    <nav aria-label="Page navigation example">
                      <ul className="pagination mb-0">
                        <li className="page-item">
                          <a className="page-link" href="#">
                            Previous
                          </a>
                        </li>
                        <li className="page-item">
                          <a className="page-link" href="#">
                            1
                          </a>
                        </li>
                        <li className="page-item">
                          <a className="page-link" href="#">
                            2
                          </a>
                        </li>
                        <li className="page-item">
                          <a className="page-link" href="#">
                            3
                          </a>
                        </li>
                        <li className="page-item">
                          <a className="page-link" href="#">
                            Next
                          </a>
                        </li>
                      </ul>
                    </nav>
                  </div>
                  <div className="row">
                    <div className="col-lg-7 mt-3">
                      <div className="orange-bg p-2">
                        <div className="grey-bg p-3 text-center">
                          <p className="p-lg my-0 ubuntu">Working Hours</p>
                        </div>
                        <div className="py-lg-4 p-2">
                          <div className="d-flex align-items-center white-card p-2 px-lg-4 justify-content-between">
                            <p className="p-md mb-0 ubuntu">
                              Required Working Hours:
                            </p>
                            <p className="p-md mb-0">198</p>
                          </div>
                          <div className="d-flex mt-3 align-items-center white-card p-2 px-lg-4 justify-content-between">
                            <p className="p-md mb-0 ubuntu">Hours Worked:</p>
                            <p className="p-md mb-0">198</p>
                          </div>
                          <div className="d-flex mt-3 align-items-center white-card p-2 px-lg-4 justify-content-between">
                            <p className="p-md mb-0 ubuntu">Days Worked:</p>
                            <p className="p-md mb-0">22</p>
                          </div>
                          <div className="d-flex mt-3 align-items-center white-card p-2 px-lg-4 justify-content-between">
                            <p className="p-md mb-0 ubuntu">Hours Remaining:</p>
                            <p className="p-md mb-0">0</p>
                          </div>
                          <div className="d-flex mt-3 align-items-center white-card p-2 px-lg-4 justify-content-between">
                            <p className="p-md mb-0 ubuntu">
                              Average Working Hours:
                            </p>
                            <p className="p-md mb-0">9</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-5 mt-3 orange-bg p-2">
                      <div className="grey-bg p-3 text-center">
                        <p className="p-lg mb-0 ubuntu">Leaves</p>
                      </div>
                      <div className="py-lg-4 p-2">
                        <div className="d-flex align-items-center white-card p-2 px-lg-4 justify-content-between">
                          <p className="p-md mb-0 ubuntu">Total:</p>
                          <p className="p-md mb-0">28</p>
                        </div>
                        <div className="d-flex mt-3 align-items-center white-card p-2 px-lg-4 justify-content-between">
                          <p className="p-md mb-0 ubuntu">Availed:</p>
                          <p className="p-md mb-0">14</p>
                        </div>
                        <div className="d-flex mb-lg-5 mt-3 align-items-center white-card p-2 px-lg-4 justify-content-between">
                          <p className="p-md mb-0 ubuntu">Remaining:</p>
                          <p className="p-md mb-0">14</p>
                        </div>
                        <div className="text-center">
                          <button
                            className="mt-lg-4 site-btn px-4 py-2 yellow-border"
                            onClick={() =>
                              history.push(`/employee/leave/eidt/${212896161}`)
                            }
                          >
                            Edit Leaves
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade leaveForm"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content site-modal">
            <i
              className="close modal-close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={21}
                height={21}
                viewBox="0 0 21 21"
              >
                <path
                  id="Icon_material-close"
                  data-name="Icon material-close"
                  d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
                  transform="translate(-7.5 -7.5)"
                  fill="#545454"
                />
              </svg>
            </i>
            <h3 className="medium text-center">Leave Form</h3>
            <div className="text-left">
              <form action>
                <label htmlFor className="p-lg ubuntu">
                  From
                </label>
                <input type="date" className="site-input py-2 px-3 w-100" />
                <label htmlFor className="p-lg mt-3 ubuntu">
                  To
                </label>
                <input type="date" className="site-input py-2 px-3 w-100" />
                <label htmlFor className="p-lg mt-3 ubuntu">
                  Select Leave Type:
                </label>
                <select name id className="site-input px-3 py-2 w-100">
                  <option value>Select Leave Type</option>
                </select>
                <label htmlFor className="p-lg mt-3 ubuntu">
                  Reason
                </label>
                <textarea
                  name
                  placeholder="Enter Reason"
                  id
                  cols={30}
                  rows={5}
                  className="site-input py-2 px-3 w-100"
                  defaultValue={""}
                />
                <button
                  className="site-btn grey-btn w-100 py-2 px-4 mt-4"
                  data-target=".leaveSubmitted"
                  data-toggle="modal"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  Apply
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade leaveSubmitted"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content site-modal">
            <i
              className="close modal-close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={21}
                height={21}
                viewBox="0 0 21 21"
              >
                <path
                  id="Icon_material-close"
                  data-name="Icon material-close"
                  d="M28.5,9.615,26.385,7.5,18,15.885,9.615,7.5,7.5,9.615,15.885,18,7.5,26.385,9.615,28.5,18,20.115,26.385,28.5,28.5,26.385,20.115,18Z"
                  transform="translate(-7.5 -7.5)"
                  fill="#545454"
                />
              </svg>
            </i>
            <div className="text-center">
              <img src="images/check.png" alt="" className="img-fluid" />
              <p className="d-grey-text mt-3 p-lg">
                You have applied for the leave <br /> successfully!
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
