import React from "react";

import TableHeader from "./Elements/Table/TableHeader";
import Pagination from "./Elements/Table/Pagination";
import TableBodyHandler from "./Elements/Table/TableBodyHandler";

export default function Table({
  headings,
  totalPages,
  setPage,
  data,
  children,
  isLoading,
}) {
  return (
    <>
      <table className="table table-borderless dataTable">
        <TableHeader headings={headings} />
        <TableBodyHandler
          data={data}
          length={headings?.length}
          isLoading={isLoading}
        />
        {children}
      </table>
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-12">
            <Pagination totalPages={totalPages} setPage={setPage} />
          </div>
        </div>
      </div>
    </>
    // <section id="configuration" className="user-page">
    //   {/* <div className="row">
    //     <div className="col-12"> */}
    //   {/* <div className="shadow-none"> */}
    //   {/* <div className="card-body"> */}
    //   {/* <div className> */}
    //   {/* <TablePagination setPerPage={setPerPage} perPage={perPage} /> */}
    //   <div className="row mt-1">
    //     <div className="main-tabble table-responsive">
    //       <div className="dataTables_wrapper container-fluid">
    //
    //       </div>
    //     </div>
    //   </div>
    //   {/* </div> */}
    //   {/* </div> */}
    //   {/* </div> */}
    //   {/* </div> */}
    //   {/* </div> */}
    // </section>
  );
}
