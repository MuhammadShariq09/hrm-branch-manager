import React from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";

export default function PublicHeader({ showContactModal }) {
  return (
    <div className="container">
      <div className="row">
        <div className="col-12">
          <nav className="navbar navbar-expand-lg navbar-dark navbar-lg top-bar">
            <Link to="/">
              <img src="images/logo.png" alt="" className="img-fluid" />
            </Link>
            <button
              className="navbar-toggler border-0 sidebar-toggle"
              type="button"
              data-toggle="collapse"
              data-target="#navbarNav1"
              aria-controls="navbarNav1"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <i className="fas fa-bars" />
            </button>
            <div className="collapse navbar-collapse" id="navbarNav1">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link
                    to="/login"
                    className="nav-link mr-3 py-2 transparent-btn"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="14.2"
                      height="15.725"
                      viewBox="0 0 14.2 15.725"
                    >
                      <g
                        id="Icon_feather-user"
                        data-name="Icon feather-user"
                        transform="translate(-5 -3.5)"
                      >
                        <path
                          id="Path_1"
                          data-name="Path 1"
                          d="M18.2,27.075V25.55a3.05,3.05,0,0,0-3.05-3.05H9.05A3.05,3.05,0,0,0,6,25.55v1.525"
                          transform="translate(0 -8.85)"
                          fill="none"
                          stroke="#fff"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={2}
                        />
                        <path
                          id="Path_2"
                          data-name="Path 2"
                          d="M18.1,7.55A3.05,3.05,0,1,1,15.05,4.5,3.05,3.05,0,0,1,18.1,7.55Z"
                          transform="translate(-2.95)"
                          fill="none"
                          stroke="#fff"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={2}
                        />
                      </g>
                    </svg>
                    Login
                  </Link>
                </li>
                <li className="nav-item">
                  <button
                    onClick={showContactModal}
                    className="nav-link site-btn px-4 py-2"
                    data-toggle="modal"
                    data-target=".contactUs"
                  >
                    Contact Us
                  </button>
                  {/* <Link
                    to="/contact-us"
                    className="nav-link site-btn px-4 py-2"
                    
                  >
                    Contact Us
                  </Link> */}
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </div>
  );
}
