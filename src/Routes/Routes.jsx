import { Switch, Route, withRouter } from "react-router-dom";

// PRIVATE GUARD
import PrivateRoutes from "./PrivateRoutes";

// SCREENS
import LandingPage from "../Screens/Landing Page/LandingPage";
import Login from "../Screens/Login/Login";
import ForgotPassword from "../Screens/Forgot Password/ForgotPassword";
import Dashboard from "../Screens/Dashboard/Dashboard";
import Profile from "../Screens/Profile/Screens/Profile";
import ChangePassword from "../Screens/Profile/Screens/ChangePassword";
import MyMonthlyReports from "../Screens/Profile/Screens/MonthlyReports";
import MyAttendanceReport from "../Screens/Profile/Screens/AttendanceReport";
import EmployeesLogs from "../Screens/Employees/Screens/Logs";
import AddEmployee from "../Screens/Employees/Screens/AddEmployee";
import EditEmployee from "../Screens/Employees/Screens/EditEmployee";
import EmployeesDetails from "../Screens/Employees/Screens/EmployeeDetails";
import CertificatesOfRecognitionLogs from "../Screens/Employees/Screens/CertificateLogs";
import CertificatesOfRecognitionDetails from "../Screens/Employees/Screens/CertificateDetails";
import GiveCertificatesOfRecognition from "../Screens/Employees/Screens/GiveCertificate.jsx";
import ViewEmployeeMessages from "../Screens/Employees/Screens/ViewEmployeeMessages";
import DisciplinaryActionFormLogs from "../Screens/Employees/Screens/DisciplinaryActionFormLogs";
import SubmitDisciplinaryActionForm from "../Screens/Employees/Screens/SubmitDisciplinaryActionForm";
import DisciplinaryActionForm from "../Screens/Employees/Screens/DisciplinaryActionForm";
import EmployeeMonthlyReport from "../Screens/Employees/Screens/MonthlyReport";
import EmployeeAttendanceReport from "../Screens/Employees/Screens/AttendanceReport";
import EmployeeEditLeaves from "../Screens/Employees/Screens/EditLeaves";
import EmployeeTrainingDetails from "../Screens/Employees/Screens/TrainingDetails";
import EmployeeForms from "../Screens/Employees/Screens/Forms";
import EmployeeViewForm from "../Screens/Employees/Screens/ViewFrom";
import EmployeeViewId from "../Screens/Employees/Screens/ViewId";
import LeaveLogs from "../Screens/Leaves/Screens/Logs";
import LeaveDetails from "../Screens/Leaves/Screens/Details";
import AddCandidate from "../Screens/Candidates/AddCandidate";
import CandidateLogs from "../Screens/Candidates/CandidateLogs";
import CandidateDetails from "../Screens/Candidates/CandidateDetails";
import PayrollLogs from "../Screens/Payroll/Screens/Logs";
import PayrollDetails from "../Screens/Payroll/Screens/Details";
import NotificationsScreen from "../Screens/Notifications/Notifications";
import Messages from "../Screens/Messages/Messages";
import ViewAllEmployeesMessages from "../Screens/Messages/ViewEMployeesMessages";
import ViewAdminMessages from "../Screens/Messages/ViewAdminMessages";
import SendMessages from "../Screens/Messages/SendMessage";

function InternalRoutes() {
  return (
    <>
      <Switch>
        <Route path="/" exact render={(props) => <LandingPage {...props} />} />
        <Route path="/login" exact render={(props) => <Login {...props} />} />
        <Route
          path="/forgot-password"
          exact
          render={(props) => <ForgotPassword {...props} />}
        />

        <PrivateRoutes path="/dashboard" exact component={Dashboard} />
        <PrivateRoutes path="/profile" exact component={Profile} />
        <PrivateRoutes path="/profile/edit" exact component={Profile} />
        <PrivateRoutes
          path="/profile/change-password"
          exact
          component={ChangePassword}
        />
        <PrivateRoutes
          path="/profile/monthly-reports"
          exact
          component={MyMonthlyReports}
        />
        <PrivateRoutes
          path="/profile/attendance-report"
          exact
          component={MyAttendanceReport}
        />
        <PrivateRoutes path="/employee/logs" exact component={EmployeesLogs} />
        <PrivateRoutes path="/employee/add" exact component={AddEmployee} />
        <PrivateRoutes
          path="/employee/edit/:id"
          exact
          component={EditEmployee}
        />
        <PrivateRoutes
          path="/employee/details/:id"
          exact
          component={EmployeesDetails}
        />
        <PrivateRoutes
          path="/employee/certificate/logs/:id"
          exact
          component={CertificatesOfRecognitionLogs}
        />
        <PrivateRoutes
          path="/employee/:employeeId/certificate/view/:id"
          exact
          component={CertificatesOfRecognitionDetails}
        />
        <PrivateRoutes
          path="/employee/certificate/new/:id"
          exact
          component={GiveCertificatesOfRecognition}
        />
        <PrivateRoutes
          path="/employee/messages/:id"
          exact
          component={ViewEmployeeMessages}
        />
        <PrivateRoutes
          path="/employee/disciplinary-action-forms/:id"
          exact
          component={DisciplinaryActionFormLogs}
        />
        <PrivateRoutes
          path="/employee/:employeeId/disciplinary-action-form/:id"
          exact
          component={DisciplinaryActionForm}
        />
        <PrivateRoutes
          path="/employee/disciplinary-action-form/new/:id"
          exact
          component={SubmitDisciplinaryActionForm}
        />
        <PrivateRoutes
          path="/employee/monthly-report/:id"
          exact
          component={EmployeeMonthlyReport}
        />
        <PrivateRoutes
          path="/employee/attendance-report/:id"
          exact
          component={EmployeeAttendanceReport}
        />
        <PrivateRoutes
          path="/employee/leave/eidt/:id"
          exact
          component={EmployeeEditLeaves}
        />
        <PrivateRoutes
          path="/employee/training-details/:id"
          exact
          component={EmployeeTrainingDetails}
        />
        <PrivateRoutes
          path="/employee/forms/:id"
          exact
          component={EmployeeForms}
        />
        <PrivateRoutes
          path="/employee/form/:id"
          exact
          component={EmployeeViewForm}
        />
        <PrivateRoutes
          path="/employee/form/id/:id"
          exact
          component={EmployeeViewId}
        />
        <PrivateRoutes path="/leave/logs" exact component={LeaveLogs} />
        <PrivateRoutes
          path="/leave/details/:id"
          exact
          component={LeaveDetails}
        />
        <PrivateRoutes path="/candidate/add" exact component={AddCandidate} />
        <PrivateRoutes path="/candidate/logs" exact component={CandidateLogs} />
        <PrivateRoutes
          path="/candidate/details/:id"
          exact
          component={CandidateDetails}
        />
        <PrivateRoutes path="/payroll/logs" exact component={PayrollLogs} />
        <PrivateRoutes
          path="/payroll/:employeeId/details/:id"
          exact
          component={PayrollDetails}
        />
        <PrivateRoutes
          path="/notifications"
          exact
          component={NotificationsScreen}
        />
        <PrivateRoutes path="/messages" exact component={Messages} />
        <PrivateRoutes
          path="/messages/employees/view"
          exact
          component={ViewAllEmployeesMessages}
        />
        <PrivateRoutes
          path="/messages/admin/view"
          exact
          component={ViewAdminMessages}
        />
        <PrivateRoutes path="/messages/send" exact component={SendMessages} />
      </Switch>
    </>
  );
}

export default withRouter(InternalRoutes);
