import React, { Fragment, useEffect, useState } from "react";
import { useMutation } from "react-query";
import { Route, withRouter } from "react-router-dom";
import { useRecoilState } from "recoil";

import Header from "../Components/Header/Header";
import Sidebar from "../Components/Sidebar";
import { me } from "../Apis";
import { userState } from "../Recoil";

const PrivateRoute = ({ component: Component, history, match, location, ...rest }) => {
  const [user, setUser] = useRecoilState(userState);
  const [loading, setLoading] = useState(false);

  const { mutate, isLoading } = useMutation(() => me(), {
    retry: false,
    onSuccess: (res) => {
      setUser(res?.data?.branch_manager);
    },
    onError: (err) => {
      history.push("/login");
      localStorage.clear();
      setLoading(false);
    },
  });

  useEffect(() => {
    const Token = localStorage.getItem("TokenBranchManagerHRM");
    if (Token) {
      setLoading(true);
      // console.log("user debug", user);
      if (!user?._id) {
        mutate();
      } else {
        setLoading(false);
        if (location.pathname === "/") history.replace("/dashboard");
      }
    } else {
      history.push("/login");
      localStorage.clear();
      setLoading(false);
    }
  }, [user?._id]);

  if (isLoading || loading)
    return (
      <div
        style={{
          height: "100vh",
          width: "100vw",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          background: "#000",
        }}
      >
        <img src="images/logo.png" />
      </div>
    );
  else
    return (
      <Route
        {...rest}
        render={(props) => (
          <Fragment>
            <Header />
            <Sidebar />
            <Component {...props} />
          </Fragment>
        )}
      />
    );
};

export default withRouter(PrivateRoute);
