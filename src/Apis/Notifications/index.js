import axios from "axios";
import { connection_string } from "../../Utils/connection_strings";

export const getNotifications = (page, branch) =>
  axios({
    url: `${connection_string}/notifications/branch-manager/`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    params: {
      page,
      role: "Branch Manager",
      branch,
    },
  });

export const getNotificationsMenu = (branch) =>
  axios({
    url: `${connection_string}/notifications/branch-manager/menu`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    params: {
      role: "Branch Manager",
      branch,
    },
  });

export const postNotificationRead = (id, branchId) =>
  axios({
    url: `${connection_string}/notifications/branch-manager/read/${id}`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    data: { branchId },
  });

export const getNotificationsCount = () =>
  axios({
    url: `${connection_string}/notifications/branch-manager/count`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });
