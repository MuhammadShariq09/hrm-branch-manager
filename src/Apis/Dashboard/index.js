import axios from "axios";
import { connection_string } from "../../Utils/connection_strings";

export const getDashboardData = (year) =>
  axios({
    url: `${connection_string}/dashboard/branch-manager/`,
    method: "GET",
    params: {
      year,
    },
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });
