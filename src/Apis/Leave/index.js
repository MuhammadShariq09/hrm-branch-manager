import axios from "axios";
import { connection_string } from "../../Utils/connection_strings";

export const applyForLeave = (data) =>
  axios({
    url: `${connection_string}/leave/branch-manager/apply`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    data,
  });

export const getLeaveLogs = (page, perPage, search_string, status, from, to) =>
  axios({
    url: `${connection_string}/leave/branch-manager/logs`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    params: {
      page,
      perPage,
      search_string,
      status,
      from,
      to,
    },
  });

export const changeLeaveStatus = (data) =>
  axios({
    url: `${connection_string}/leave/branch-manager/status`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    data,
  });

export const getLeaveDetails = (id) =>
  axios({
    url: `${connection_string}/leave/branch-manager/details/${id}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });
