import axios from "axios";
import { connection_string } from "../../Utils/connection_strings";

export const submitQuery = (data) =>
  axios({
    url: `${connection_string}/query`,
    method: "POST",
    data,
  });
