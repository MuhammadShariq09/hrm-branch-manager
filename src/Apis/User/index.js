import axios from "axios";
import { connection_string } from "../../Utils/connection_strings";

export const updateProfile = (data) =>
  axios({
    url: `${connection_string}/branch-manager/update-profile`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    data,
  });

export const changePassword = (data) =>
  axios({
    url: `${connection_string}/branch-manager/update-password`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    data,
  });
