import axios from "axios";
import { connection_string } from "../../Utils/connection_strings";

export const giveCertificate = (data) =>
  axios({
    url: `${connection_string}/certificate/branch-manager/give`,
    method: "POST",
    data,
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });

export const getCertificateLogs = (page, perPage, employee) =>
  axios({
    url: `${connection_string}/certificate/branch-manager/logs`,
    method: "GET",
    params: {
      page,
      perPage,
      employee,
      employeeId: employee,
    },
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });

export const getCertificate = (id, employeeId) =>
  axios({
    url: `${connection_string}/certificate/branch-manager/get/${id}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    params: {
      employeeId,
    },
  });
