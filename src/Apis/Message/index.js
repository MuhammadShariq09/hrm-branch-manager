import axios from "axios";
import { connection_string } from "../../Utils/connection_strings";

export const sendMessage = (data) =>
  axios({
    url: `${connection_string}/message/branch-manager/send`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    data,
  });

export const sendMessageToAll = (data) =>
  axios({
    url: `${connection_string}/message/branch-manager/send-to-all`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    data,
  });

export const getMessages = (page, perPage, room_id) =>
  axios({
    url: `${connection_string}/message/branch-manager/`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    params: {
      page,
      perPage,
      room_id,
    },
  });

export const getGroupMessages = (page, perPage) =>
  axios({
    url: `${connection_string}/message/branch-manager/group`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    params: {
      page,
      perPage,
    },
  });
