import axios from "axios";
import { connection_string } from "../../Utils/connection_strings";

export const getEmployeeLogs = (page, perPage, search_string, status, from, to) =>
  axios({
    url: `${connection_string}/employee/branch-manager/logs`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    params: {
      page,
      perPage,
      searchString: search_string,
      filter: status,
      from,
      to,
    },
  });

export const changeEmployeeStatus = (id) =>
  axios({
    url: `${connection_string}/employee/branch-manager/status`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    data: { id, employeeId: id },
  });

export const getEmployeeDetails = (id, bank_details) =>
  axios({
    url: `${connection_string}/employee/branch-manager/details/${id}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    params: {
      bank_details,
      employeeId: id,
    },
  });

export const registerEmployee = (data) =>
  axios({
    url: `${connection_string}/auth/employee/register`,
    method: "POST",
    data,
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });

export const updateEmployee = (data) =>
  axios({
    url: `${connection_string}/employee/branch-manager/update`,
    method: "POST",
    data,
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });

export const getEmployeeName = (id) =>
  axios({
    url: `${connection_string}/employee/branch-manager/get-name`,
    method: "Get",
    params: {
      employeeId: id,
    },
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });

export const getMinEmployees = async (searchString) =>
  await axios({
    url: `${connection_string}/employee/branch-manager/min`,
    method: "GET",
    params: { searchString },
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });

export const getEmployeeForms = async (employeeId) =>
  await axios({
    url: `${connection_string}/form/branch-manager/get`,
    method: "GET",
    params: { employeeId },
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });

export const getEmployeeTrainingCourses = async (page, employeeId, status) =>
  await axios({
    url: `${connection_string}/joined-course/branch-manager/get`,
    method: "GET",
    params: { page, employeeId, status, perPage: 20 },
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });
