import axios from "axios";
import { connection_string } from "../../Utils/connection_strings";

export const registerCandidate = (data) =>
  axios({
    url: `${connection_string}/auth/potential-candidate/register`,
    method: "POST",
    data,
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });

export const getCandidatesLogs = (
  page,
  perPage,
  search_string,
  status,
  from,
  to
) => {
  const Token = localStorage.getItem("TokenBranchManagerHRM");
  return axios({
    url: `${connection_string}/potential-candidate/branch-manager/logs`,
    method: "GET",
    params: {
      page,
      perPage,
      search_string,
      status,
      from,
      to,
    },
    headers: {
      Authorization: `Bearer ${Token}`,
    },
  });
};

export const candidateDetails = (id) =>
  axios({
    url: `${connection_string}/potential-candidate/branch-manager/details/${id}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    params: {
      candidateId: id,
    },
  });

export const rejectCandidate = (id, rejectionReason) =>
  axios({
    url: `${connection_string}/potential-candidate/branch-manager/reject/${id}`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    data: { rejectionReason, candidateId: id },
  });

export const acceptCandidate = (id) =>
  axios({
    url: `${connection_string}/potential-candidate/branch-manager/accept/${id}`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    data: {
      candidateId: id,
    },
  });

export const underReviewCandidate = (id) =>
  axios({
    url: `${connection_string}/potential-candidate/branch-manager/under-review/${id}`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    data: {
      candidateId: id,
    },
  });
