import axios from "axios";
import { connection_string } from "../../Utils/connection_strings";

export const submitDisciplinaryActionForm = (data) =>
  axios({
    url: `${connection_string}/disciplinay/branch-manager/submit`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    data,
  });

export const getDisciplinaryActionLogs = (year, month, employeeId) =>
  axios({
    url: `${connection_string}/disciplinay/branch-manager/logs`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    params: {
      year,
      month,
      employeeId,
    },
  });

export const getDisciplinaryActionForm = (id, employeeId) =>
  axios({
    url: `${connection_string}/disciplinay/branch-manager/get/${id}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
    params: {
      employeeId,
    },
  });
