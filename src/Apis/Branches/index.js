import axios from "axios";
import { connection_string } from "../../Utils/connection_strings";

export const getMinBranches = async (searchString, getAll) =>
  await axios({
    url: `${connection_string}/branch/branch-manager/min`,
    method: "GET",
    params: {
      searchString,
      getAll,
    },
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });
