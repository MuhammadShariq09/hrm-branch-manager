import axios from "axios";
import { connection_string } from "../../Utils/connection_strings";

export const getShiftDropdown = () =>
  axios({
    url: `${connection_string}/shift/branch-manager/dropdown`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("TokenBranchManagerHRM")}`,
    },
  });
