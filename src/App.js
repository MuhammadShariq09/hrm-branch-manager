import React from "react";

import Routes from './Routes/Routes';

export default function App() {
  // const user = useRecoilValue(userState);
  // const [user, setUser] = useRecoilState(userState);

  return (
    <>
      <Routes />
    </>
  );
}
